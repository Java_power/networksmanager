package javapower.netman.message;

import io.netty.buffer.ByteBuf;
import javapower.netman.util.BlockPosDim;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class NetworkTerminalProject implements IMessage
{
	public BlockPosDim env;
	public NBTTagCompound nbt;
	
	public NetworkTerminalProject()
	{
		env = new BlockPosDim();
		nbt = new NBTTagCompound();
	}
	
	public NetworkTerminalProject(BlockPosDim _env, NBTTagCompound _nbt)
	{
		env = _env;
		nbt = _nbt;
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		NBTTagCompound in = ByteBufUtils.readTag(buf);
		if(in == null)
			return;
		
		env = new BlockPosDim(in, "e");
		nbt = in.getCompoundTag("d");
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		NBTTagCompound out = new NBTTagCompound();
		if(env != null)
			env.WriteToNBT(out, "e");
		out.setTag("d", nbt);
		
		ByteBufUtils.writeTag(buf, out);
	}
	
}
