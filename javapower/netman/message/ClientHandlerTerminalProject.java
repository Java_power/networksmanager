package javapower.netman.message;

import javapower.netman.proxy.ClientProxy;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ClientHandlerTerminalProject  implements IMessageHandler<NetworkTerminalProject, NetworkTerminalProject>
{

	@Override
	public NetworkTerminalProject onMessage(NetworkTerminalProject message, MessageContext ctx)
	{
		if(message == null || message.env == null || message.nbt == null) return null;
		
		int tag = message.nbt.getByte("tag");
		message.nbt.getKeySet().remove("tag");
		
		if(tag == 1 && ClientProxy.project_temp != null)
		{
			int page = message.nbt.getInteger("p");
			
			if(page < ClientProxy.project_temp.pages.size())
			{
				NBTTagCompound nbt_page = ClientProxy.project_temp.getPacketMap(page);
				nbt_page.setByte("tag", (byte) 2);
				nbt_page.setInteger("p", page);
				return new NetworkTerminalProject(message.env, nbt_page);
			}
			else
			{
				ClientProxy.project_temp = null;
			}
		}
		else if(tag == 2)
		{
			ClientProxy.project_temp = null;
		}
		else if(tag == 3 && ClientProxy.project_temp != null)
		{
			ClientProxy.project_temp.setPacketInfo(message.nbt);
			
			NBTTagCompound nbt_return = new NBTTagCompound();
			nbt_return.setByte("tag", (byte) 4);
			nbt_return.setInteger("p", 0);
			
			return new NetworkTerminalProject(message.env, nbt_return);
		}
		else if(tag == 4)
		{
			int page_id = message.nbt.getInteger("p");
			ClientProxy.project_temp.setPacketPage(page_id, message.nbt);
			
			NBTTagCompound nbt_return = new NBTTagCompound();
			nbt_return.setByte("tag", (byte) 4);
			nbt_return.setInteger("p", page_id +1);
			
			return new NetworkTerminalProject(message.env, nbt_return);
		}
		else if(tag == 5)
		{
			ClientProxy.project_temp.endLoadPackets();
		}
		
		return null;
	}

}