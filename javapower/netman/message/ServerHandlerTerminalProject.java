package javapower.netman.message;

import java.io.File;
import java.io.IOException;

import javapower.netman.core.NetworksManager;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerHandlerTerminalProject implements IMessageHandler<NetworkTerminalProject, NetworkTerminalProject>
{

	@Override
	public NetworkTerminalProject onMessage(NetworkTerminalProject message, MessageContext ctx)
	{
		if(message == null || message.env == null || message.nbt == null) return null;
		if(message.nbt.hasKey("tag"))
		{
			int tag = message.nbt.getByte("tag");
			message.nbt.getKeySet().remove("tag");
			
			if(tag == 1)
			{
				int pages = message.nbt.getInteger("s");
				
				for(File file: message.env.BlockPathFolder().listFiles())
				    if (!file.isDirectory()) 
				        file.delete();
				
				File save = new File(message.env.BlockPathFolder() + "\\project_info.dat");
				
				try
				{
					CompressedStreamTools.write(message.nbt, save);
				}
				catch (IOException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)e.printStackTrace();
				}
				
				if(pages > 0)
				{
					NBTTagCompound nbt_return = new NBTTagCompound();
					nbt_return.setByte("tag", (byte) 1);
					nbt_return.setInteger("p", 0);
					
					return new NetworkTerminalProject(message.env, nbt_return);
				}
			}
			else if(tag == 2)
			{
				int page = message.nbt.getInteger("p");
				message.nbt.getKeySet().remove("p");
				
				File save = new File(message.env.BlockPathFolder() + "\\project_page"+page+".dat");
				try
				{
					CompressedStreamTools.write(message.nbt, save);
				}
				catch (IOException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)e.printStackTrace();
				}
				
				NBTTagCompound nbt_return = new NBTTagCompound();
				nbt_return.setByte("tag", (byte) 1);
				nbt_return.setInteger("p", page+1);
				
				return new NetworkTerminalProject(message.env, nbt_return);
			}
			else if(tag == 3)
			{
				File save = new File(message.env.BlockPathFolder() + "\\project_info.dat");
				if(save.exists())
				{
					try
					{
						NBTTagCompound nbt_return = CompressedStreamTools.read(save);
						if(nbt_return == null)
							return null;
						
						nbt_return.setByte("tag", (byte) 3);
						return new NetworkTerminalProject(message.env, nbt_return);
					}
					catch (IOException e)
					{
						if(NetworksManager.PrintStackTrace_TileEntity)e.printStackTrace();
					}
					
				}
				else
				{
					NBTTagCompound nbt = new NBTTagCompound();
					nbt.setByte("tag", (byte) 2);
					return new NetworkTerminalProject(message.env, nbt);
				}
			}
			else if(tag == 4)
			{
				int page = message.nbt.getInteger("p");
				
				File save = new File(message.env.BlockPathFolder() + "\\project_page"+page+".dat");
				if(save.exists())
				{
					try
					{
						NBTTagCompound nbt_return = CompressedStreamTools.read(save);
						
						if(nbt_return == null)
							return null;
						
						nbt_return.setByte("tag", (byte) 4);
						nbt_return.setInteger("p", page);
						
						return new NetworkTerminalProject(message.env, nbt_return);
					}
					catch (IOException e)
					{
						if(NetworksManager.PrintStackTrace_TileEntity)e.printStackTrace();
					}
				}
				else
				{
					NBTTagCompound nbt_return = new NBTTagCompound();
					nbt_return.setByte("tag", (byte) 5);
					return new NetworkTerminalProject(message.env, nbt_return);
				}
			}
		}
		return null;
	}

}
