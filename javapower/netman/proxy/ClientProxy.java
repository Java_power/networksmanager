package javapower.netman.proxy;

import java.lang.reflect.Field;

import javapower.netman.block.NMBlocks;
import javapower.netman.gui.terminal.project.Project;
import javapower.netman.item.NMItems;
import javapower.netman.util.RegisterUtils;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ClientProxy extends CommonProxy
{
	public static Minecraft minecraft = Minecraft.getMinecraft();
	
	public static Project project_temp = null;
	
	@Override
	public void preInit(FMLPreInitializationEvent e)
    {
		super.preInit(e);
		
		//Field[] fields1 = NMBlocks.class.getFields();
		//RegisterUtils.RegisterTilesEntitySR(fields1);
    }
	
	@Override
    public void init(FMLInitializationEvent e)
    {
		ResourceLocationRegister.register();
		/*if(Loader.isModLoaded("jei"))
		{
			JEIIntegration.load();
		}
		if(Loader.isModLoaded("nei"))
		{
			JEIIntegration.load();
		}*/
    	super.init(e);
    }
    
	@Override
    public void postInit(FMLPostInitializationEvent e)
    {
    	super.postInit(e);
    }
	
	@SubscribeEvent
    public void registerModels(ModelRegistryEvent e)
	{
		//Items
    	Field[] fields0 = NMItems.class.getFields();
    	RegisterUtils.RegisterIRender(fields0);
    	
    	//Blocks
    	Field[] fields1 = NMBlocks.class.getFields();
    	RegisterUtils.RegisterIRender(fields1);
    	//ClientRegistry.bindTileEntitySpecialRenderer(tileEntityClass, specialRenderer);
	}
}
