package javapower.netman.proxy;

import javapower.netman.core.NetworksManager;
import net.minecraft.util.ResourceLocation;

public class ResourceLocationRegister
{
	public static ResourceLocation
	texture_rf_guis,
	texture_redstone_guis,
	texture_fluid_guis,
	texture_terminal_icons,
	texture_terminal_items,
	texture_pictogram;
	
	public static void register()
	{
		texture_rf_guis = resource("textures/gui/rf_guis.png");
		texture_redstone_guis = resource("textures/gui/rsswitch_gui.png");
		texture_fluid_guis = resource("textures/gui/fluid_guis.png");
		texture_terminal_icons = resource("textures/gui/terminal_icons.png");
		texture_terminal_items = resource("textures/gui/terminal_items.png");
		texture_pictogram = resource("textures/gui/picto.png");
	}
	
	private static ResourceLocation resource(String target)
	{
		return new ResourceLocation(NetworksManager.MODID, target);
	}
}
