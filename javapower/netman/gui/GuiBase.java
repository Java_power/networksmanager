package javapower.netman.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javapower.netman.gui.util.IGuiElement;
import javapower.netman.util.IGUITileSync;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import javapower.netman.util.Vector2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public abstract class GuiBase<T extends TileEntity> extends GuiContainer implements IGUITileSync
{
	T tileEntity;
	
	Vector2 mouspos = new Vector2();
	List<IGuiElement> elements = new ArrayList<IGuiElement>();
	int xSprite, ySprite;
	
	public GuiBase(Container inventorySlotsIn, T _tileEntity, Vector2 spritePos, Vector2 guiSize)
	{
		super(inventorySlotsIn);
		tileEntity = _tileEntity;
		
		xSprite = spritePos.x;
		ySprite = spritePos.y;
		
		xSize = guiSize.x;
		ySize = guiSize.y;
		
		NetworkUtils.sendToServerPlayerAsOpenGUI(tileEntity, this);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.drawDefaultBackground();
		mc.renderEngine.bindTexture(getBindTexture());
		int x = (width - xSize) /2;
        int y = (height - ySize) /2;
        this.drawTexturedModalRect(x, y, xSprite, ySprite, xSize, ySize);
        
        for(IGuiElement e : elements)
		{
        	e.draw(mc, this, x, y);
		}
        
        drawelements(x, y);
	}
	
	protected abstract ResourceLocation getBindTexture();
	protected abstract void drawelements(int x, int y);

	@Override
	public Class<? extends TileEntity> tileEntityLink()
	{
		return tileEntity.getClass();
	}
	
	@Override
	public void onResize(Minecraft mcIn, int w, int h)
	{
		for(IGuiElement e : elements)
		{
			e.resize(w, h);
		}
		super.onResize(mcIn, w, h);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		int x = (width - xSize) /2;
        int y = (height - ySize) /2;
        
		for(IGuiElement e : elements)
		{
			if(e.eventMouse(mouseX - x, mouseY - y, mouseButton))
				break;
		}
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		for(IGuiElement e : elements)
		{
			if(e.eventKeyboard(typedChar, keyCode))
				break;
		}
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	public void updateScreen()
	{
		Tools.GetMouseLocalisation(mouspos, width, height, xSize, ySize);
		
		update();
		
		for(IGuiElement e : elements)
		{
			e.update();
		}
		super.updateScreen();
	}
	
	protected abstract void update();
	
	public void sendInfo(NBTTagCompound nbt)
	{
		NetworkUtils.sendToServerTheData(tileEntity, this, nbt);
	}

}
