package javapower.netman.gui.util;

import java.util.ArrayList;
import java.util.List;

import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.VectorDynamic2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GEMenu implements IGuiElement
{
	public boolean enable = false;
	public boolean ison;
	
	VectorDynamic2 vectorDyn;
	Zone2D aera;
	String title;
	int size = 0;
	int isonId = -1;
	public List<IGuiMenuElement> elements = new ArrayList<IGuiMenuElement>();
	
	/**
	 * @param _size default 60
	 */
	public GEMenu(VectorDynamic2 _vecDyn, String _title, int _size)
	{
		vectorDyn = _vecDyn;
		title = _title;
		size = _size;
		aera = new Zone2D(vectorDyn.pos, vectorDyn.pos.copyAndAdd(size, 20));
	}
	
	@Override
	public void resize(int w, int h)
	{
		vectorDyn.reCalculate();
		aera.start = vectorDyn.pos;
		aera.end = vectorDyn.pos.copyAndAdd(size, 20);
	}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		
		gui.drawTexturedModalRect(x + vectorDyn.pos.x, y + vectorDyn.pos.y, 0, 228, 20, 20);
		gui.drawRect(x + vectorDyn.pos.x + 20, y + vectorDyn.pos.y + 18, x + vectorDyn.pos.x + size, y + vectorDyn.pos.y + 20, 0xffb200ff);
		
		if(ison)
		{
			gui.drawRect(x + aera.start.x, y + aera.start.y, aera.start.x + size, aera.start.y + 20, 0x40ffffff);
		}
		
		if(enable)
		{
			gui.drawRect(x + vectorDyn.pos.x, y + vectorDyn.pos.y + 20, x + vectorDyn.pos.x + size, y + vectorDyn.pos.y + 20*(elements.size()+1), 0xff404040);
			
			
			int decal = 0;
			for(IGuiMenuElement gme : elements)
			{
				int icon = gme.iconId();
				boolean e_enable = gme.isEnable();
				
				if(e_enable && isonId == decal)
				{
					gui.drawRect(x + vectorDyn.pos.x + 20, y + vectorDyn.pos.y + 20 + (20*decal), x + vectorDyn.pos.x + size, y + vectorDyn.pos.y + 40 + (20*decal), 0xff2d2d2d);
				}
				
				if(icon >= 0)
				{
					mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
					if(e_enable)
						GlStateManager.color(1, 1, 1);
					else
						GlStateManager.color(0.25f, 0.25f, 0.25f);
					gui.drawTexturedModalRect(x + vectorDyn.pos.x + 2, y + vectorDyn.pos.y + 22 + (20*decal), 240, 16*icon, 16, 16);
				}
				gme.draw(mc, gui, x + vectorDyn.pos.x, y + vectorDyn.pos.y + 20 + (20*decal));
				
				gui.drawString(mc.fontRenderer, gme.text(), x + vectorDyn.pos.x + 22, y + vectorDyn.pos.y + 27 + (20*decal), e_enable ? 0xffffff : 0x404040);
				++decal;
			}
		}
		
		gui.drawString(mc.fontRenderer, title, x + vectorDyn.pos.x + 22, y + vectorDyn.pos.y + 7, 0xffffff);
	}

	@Override
	public void update()
	{
		ison = aera.MouseIsOnArea(vectorDyn.screen.mouse);
		
		if(aera.MouseIsOnArea_X(vectorDyn.screen.mouse.x) && vectorDyn.screen.mouse.y - vectorDyn.pos.y - 20 >= 0)
		{
			isonId = (vectorDyn.screen.mouse.y - vectorDyn.pos.y - 20)/20;
		}
		else
			isonId = -1;
	}

	@Override
	public boolean eventMouse(int x, int y, int button)
	{
		if(aera.MouseIsOnArea(vectorDyn.screen.mouse) && button == 0)
		{
			enable = !enable;
			return true;
		}
		else
		{
			if(enable)
			if(aera.MouseIsOnArea_X(vectorDyn.screen.mouse.x) && isonId >= 0 && isonId < elements.size())
			{
				IGuiMenuElement gme = elements.get(isonId);
				if(gme.isEnable())
					gme.event(vectorDyn.pos.copyAndAdd(size, 20 + isonId*20));
				return true;
			}
			else
			{
				boolean ison = false;
				
				for(IGuiMenuElement gme : elements)
					if(gme.isOn(vectorDyn))
					{
						ison = true;
						break;
					}
				
				if(!ison)
				{
					enable = false;
					for(IGuiMenuElement gme : elements)
						gme.disable();
				}
				else
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode)
	{
		return false;
	}
	
}
