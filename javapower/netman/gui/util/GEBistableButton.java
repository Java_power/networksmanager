package javapower.netman.gui.util;

import javapower.netman.eventio.IEventOut;
import javapower.netman.util.Vector2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GEBistableButton implements IGuiElement
{
	Vector2 posbuttonA, posbuttonB;
	public Boolean sw_pos = false;
	Zone2D zA, zB;
	Vector2 mousepos;
	IEventOut<Boolean> event;
	
	public GEBistableButton(Vector2 _posbuttonA, Vector2 _posbuttonB, Vector2 _mousepos, IEventOut<Boolean> _event)
	{
		posbuttonA = _posbuttonA;
		posbuttonB = _posbuttonB;
		mousepos = _mousepos;
		
		event = _event;
		
		zA = new Zone2D(posbuttonA, posbuttonA.copyAndAdd(36, 18));
		zB = new Zone2D(posbuttonB, posbuttonB.copyAndAdd(36, 18));
	}
	
	@Override
	public void resize(int w, int h) {}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		gui.drawTexturedModalRect(x+ posbuttonA.x, y+ posbuttonA.y, 80+(sw_pos ? 0 : (36 + (zA.MouseIsOnArea(mousepos)? 36 : 0))), 10, 36, 18);
		gui.drawTexturedModalRect(x+ posbuttonB.x, y+ posbuttonB.y, 80+(!sw_pos ? 0 : (36 + (zB.MouseIsOnArea(mousepos)? 36 : 0))), 28, 36, 18);
	}

	@Override
	public void update() {}

	@Override
	public boolean eventMouse(int x, int y, int button)
	{
		if(button == 0)
		if(sw_pos)
		{
			if(zB.MouseIsOnArea(x, y))
			{
				sw_pos = false;
				event.event(sw_pos);
				return true;
			}
		}
		else
		{
			if(zA.MouseIsOnArea(x, y))
			{
				sw_pos = true;
				event.event(sw_pos);
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode) {return false;}

}
