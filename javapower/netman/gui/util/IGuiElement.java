package javapower.netman.gui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public interface IGuiElement
{
	public void resize(int w, int h);
	public void draw(Minecraft mc, GuiScreen gui, int x, int y);
	public void update();
	public boolean eventMouse(int x, int y, int button);
	public boolean eventKeyboard(char typedChar, int keyCode);
}
