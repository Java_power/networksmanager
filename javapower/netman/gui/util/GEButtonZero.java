package javapower.netman.gui.util;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.util.Vector2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GEButtonZero implements IGuiElement
{
	Zone2D aera;
	Vector2 mouse;
	IEventVoid event;
	
	public GEButtonZero(Vector2 _pos, Vector2 _mouse, IEventVoid _event)
	{
		aera = new Zone2D(_pos, _pos.copyAndAdd(12, 12));
		mouse = _mouse;
		event = _event;
	}
	
	@Override
	public void resize(int w, int h){}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		if(aera.MouseIsOnArea(mouse.x - x, mouse.y - y))
		{
			gui.drawTexturedModalRect(x + aera.start.x, y + aera.start.y, 92, 56, 12, 12);
		}
		else
		{
			gui.drawTexturedModalRect(x + aera.start.x, y + aera.start.y, 80, 56, 12, 12);
		}
	}

	@Override
	public void update()
	{
		
	}

	@Override
	public boolean eventMouse(int x, int y, int button)
	{
		if(aera.MouseIsOnArea(mouse) && button == 0)
		{
			event.event();
			return true;
		}
		return false;
	}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode)
	{
		return false;
	}

}
