package javapower.netman.gui.util;

import javapower.netman.eventio.IEventOut;
import javapower.netman.eventio.IEventVoid;
import javapower.netman.util.VectorDynamic2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GEAera implements IGuiElement
{
	Zone2D aera;
	VectorDynamic2 vectorDyn;
	IEventVoid event;
	IEventOut<Boolean> state;
	boolean enable = true;
	boolean ison;
	int _w,_h;
	
	public GEAera(VectorDynamic2 _vecDyn, IEventVoid _event, int w, int h)
	{
		vectorDyn = _vecDyn;
		aera = new Zone2D(vectorDyn.pos, vectorDyn.pos.copyAndAdd(w, h));
		event = _event;
		_w = w;
		_h = h;
		state = null;
	}
	
	public GEAera(VectorDynamic2 _vecDyn, IEventVoid _event, IEventOut<Boolean> _state, int w, int h)
	{
		vectorDyn = _vecDyn;
		aera = new Zone2D(vectorDyn.pos, vectorDyn.pos.copyAndAdd(w, h));
		event = _event;
		state = _state;
	}
	
	@Override
	public void resize(int w, int h)
	{
		vectorDyn.reCalculate();
		aera.start = vectorDyn.pos;
		aera.end = vectorDyn.pos.copyAndAdd(_w, _h);
	}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		if(enable && ison)
		{
			gui.drawRect(x + aera.start.x, y + aera.start.y, aera.end.x, aera.end.y, 0x44ffffff);
		}
	}

	@Override
	public void update()
	{
		if(enable && ison != aera.MouseIsOnArea(vectorDyn.screen.mouse))
		{
			ison = !ison;
			if(state != null)
				state.event(ison);
		}
		
	}

	@Override
	public boolean eventMouse(int x, int y, int button)
	{
		if(enable && aera.MouseIsOnArea(vectorDyn.screen.mouse) && button == 0)
		{
			update();
			event.event();
			return true;
		}
		return false;
	}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode)
	{
		return false;
	}
	
	public void setEnable(boolean _enable)
	{
		enable = _enable;
	}
	
	public boolean isEnable()
	{
		return enable;
	}

}