package javapower.netman.gui.util;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.util.VectorDynamic2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GEButtonBar implements IGuiElement
{
	Zone2D aera;
	VectorDynamic2 vectorDyn;
	IEventVoid event;
	byte id;
	boolean invert = false;
	boolean enable = true;
	
	public GEButtonBar(VectorDynamic2 _vecDyn, IEventVoid _event, int _id)
	{
		id = (byte) _id;
		vectorDyn = _vecDyn;
		aera = new Zone2D(vectorDyn.pos, vectorDyn.pos.copyAndAdd(40, 20));
		event = _event;
	}
	
	public GEButtonBar(VectorDynamic2 _vecDyn, IEventVoid _event, int _id, boolean _invert)
	{
		id = (byte) _id;
		vectorDyn = _vecDyn;
		aera = new Zone2D(vectorDyn.pos, vectorDyn.pos.copyAndAdd(40, 20));
		event = _event;
		invert = _invert;
	}
	
	@Override
	public void resize(int w, int h)
	{
		vectorDyn.reCalculate();
		aera.start = vectorDyn.pos;
		aera.end = vectorDyn.pos.copyAndAdd(40, 20);
	}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		if(enable)
		{
			if(invert)
			{
				GlStateManager.translate(x + aera.start.x + 20, y + aera.start.y + 10, 0);
				GlStateManager.rotate(180, 0,0,1);
				GlStateManager.translate(- (x + aera.start.x + 20), - (y + aera.start.y + 10), 0);
			}
			gui.drawTexturedModalRect(x + aera.start.x, y + aera.start.y, 200, id*20, 40, 20);
			
			if(invert)
			{
				GlStateManager.translate(x + aera.start.x + 20, y + aera.start.y + 10, 0);
				GlStateManager.rotate(180, 0,0,1);
				GlStateManager.translate(- (x + aera.start.x + 20), - (y + aera.start.y + 10), 0);
			}
			
			if(aera.MouseIsOnArea(vectorDyn.screen.mouse))
			{
				Gui.drawRect(x + aera.start.x, y + aera.start.y, aera.start.x +40, aera.start.y + 20, 0x40ffffff);
			}
		}
	}

	@Override
	public void update() {}

	@Override
	public boolean eventMouse(int x, int y, int button)
	{
		if(enable && aera.MouseIsOnArea(vectorDyn.screen.mouse) && button == 0)
		{
			event.event();
			return true;
		}
		return false;
	}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode)
	{
		return false;
	}
	
	public void setEnable(boolean _enable)
	{
		enable = _enable;
	}
	
	public boolean isEnable()
	{
		return enable;
	}
	
}
