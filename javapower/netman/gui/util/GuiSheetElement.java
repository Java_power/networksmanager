package javapower.netman.gui.util;

import javapower.netman.util.Vector2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.ResourceLocation;

public class GuiSheetElement
{
	Vector2 pos;
	Vector2 size;
	
	public GuiSheetElement(Vector2 posElement, Vector2 sizeElement)
	{
		pos = posElement;
		size = sizeElement;
	}
	
	public void drawTexture(Gui gui, int x, int y)
	{
		gui.drawTexturedModalRect(x, y, pos.x, pos.y, size.x, size.y);
	}
	
	public void drawTexture(Gui gui, Vector2 vec)
	{
		gui.drawTexturedModalRect(vec.x, vec.y, pos.x, pos.y, size.x, size.y);
	}
}
