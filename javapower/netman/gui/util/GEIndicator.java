package javapower.netman.gui.util;

import javapower.netman.eventio.IEventIn;
import javapower.netman.util.Vector2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GEIndicator implements IGuiElement
{
	Vector2 pos;
	IEventIn<Boolean> event;
	
	public GEIndicator(Vector2 _pos, IEventIn<Boolean> _event)
	{
		pos = _pos;
		event = _event;
	}

	@Override
	public void resize(int w, int h) {}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		gui.drawTexturedModalRect(x+ pos.x, y+ pos.y, 80, event.event() ? 0 : 5, 10, 5);
	}

	@Override
	public void update() {}

	@Override
	public boolean eventMouse(int x, int y, int button) {return false;}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode) {return false;}

}
