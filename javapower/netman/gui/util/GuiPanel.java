package javapower.netman.gui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public abstract class GuiPanel
{
	public void initPanel(GuiScreen gui)
	{
		
	}
	
	public void onPanelClosed()
	{
		
	}
	
	public abstract void draw();
	
	public abstract void update();
	
	public boolean mouseClicked(int mouseX, int mouseY, int mouseButton)
	{
		return false;
	}
	
	public void mouseWheelEvent(int delta_wheel)
	{
		
	}
	
	public boolean keyTyped(char typedChar, int keyCode)
	{
		return false;
	}
	
	public void onResize(Minecraft mcIn, int w, int h)
	{
		
	}
	
	public void reciveDataFromServer(NBTTagCompound nbt)
	{
		
	}
}
