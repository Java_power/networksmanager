package javapower.netman.gui.util;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GMEButton implements IGuiMenuElement
{
	int icon;
	IEventVoid event;
	String text;
	public boolean enable = true;
	
	public GMEButton(int iconId, IEventVoid _event, String _text)
	{
		icon = iconId;
		event = _event;
		text = _text;
	}
	
	@Override
	public int iconId()
	{
		return icon;
	}

	@Override
	public void event(Vector2 pos_to_draw)
	{
		event.event();
	}

	@Override
	public String text()
	{
		return text;
	}

	@Override
	public void disable()
	{
		
	}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		
	}

	@Override
	public void update()
	{
		
	}

	@Override
	public void eventMouse(int x, int y, int button)
	{
		
	}

	@Override
	public boolean isOn(VectorDynamic2 from_super)
	{
		return false;
	}

	@Override
	public boolean isEnable()
	{
		return enable;
	}

}
