package javapower.netman.gui.util;

import net.minecraft.client.gui.GuiScreen;

public interface IGuiMessage<T extends GuiPanel>
{
	public void draw(GuiScreen gui, T panel, int x_middle, int y_middle);
	public void update(int x_mouse, int y_mouse);
	public void eventMouse(GuiScreen gui, T panel, int x_middle, int y_middle, int button);
	public void eventKeyboard(GuiScreen gui, T panel, char typedChar, int keyCode);
	public void resize(int w, int h);
}
