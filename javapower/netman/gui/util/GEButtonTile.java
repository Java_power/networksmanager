package javapower.netman.gui.util;

import org.lwjgl.opengl.GL11;

import javapower.netman.eventio.IEventOut;
import javapower.netman.eventio.IEventVoid;
import javapower.netman.util.VectorDynamic2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GEButtonTile implements IGuiElement
{
	Zone2D aera;
	VectorDynamic2 vectorDyn;
	IEventVoid event;
	IEventOut<Boolean> state;
	boolean ison;
	byte id;
	
	public GEButtonTile(VectorDynamic2 _vecDyn, IEventVoid _event, int _id)
	{
		id = (byte) _id;
		vectorDyn = _vecDyn;
		aera = new Zone2D(vectorDyn.pos, vectorDyn.pos.copyAndAdd(100, 100));
		event = _event;
		state = null;
	}
	
	public GEButtonTile(VectorDynamic2 _vecDyn, IEventVoid _event, IEventOut<Boolean> _state, int _id)
	{
		id = (byte) _id;
		vectorDyn = _vecDyn;
		aera = new Zone2D(vectorDyn.pos, vectorDyn.pos.copyAndAdd(100, 100));
		event = _event;
		state = _state;
	}
	
	@Override
	public void resize(int w, int h)
	{
		vectorDyn.reCalculate();
		aera.start = vectorDyn.pos;
		aera.end = vectorDyn.pos.copyAndAdd(100, 100);
	}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		if(ison)
		{
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1);
		}
		gui.drawTexturedModalRect(x + aera.start.x, y + aera.start.y, 100*(id%2), 100*(id/2), 100, 100);
		GL11.glColor4f(1, 1, 1, 1);
	}

	@Override
	public void update()
	{
		if(ison != aera.MouseIsOnArea(vectorDyn.screen.mouse))
		{
			ison = !ison;
			if(state != null)
				state.event(ison);
		}
		
	}

	@Override
	public boolean eventMouse(int x, int y, int button)
	{
		if(aera.MouseIsOnArea(vectorDyn.screen.mouse) && button == 0)
		{
			update();
			event.event();
			return true;
		}
		return false;
	}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode)
	{
		return false;
	}

}
