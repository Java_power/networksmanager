package javapower.netman.gui.util;

import javapower.netman.eventio.IEventOut;
import javapower.netman.util.Vector2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GESwitch implements IGuiElement
{
	public boolean switch_pos;
	IEventOut<Boolean> event;
	Zone2D area;
	
	public GESwitch(Vector2 _pos, IEventOut<Boolean> _event)
	{
		area = new Zone2D(_pos, _pos.copyAndAdd(20, 10));
		event = _event;
	}
	
	@Override
	public void resize(int w, int h){}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		if(switch_pos)
		{
			gui.drawTexturedModalRect(x + area.start.x, y + area.start.y, 80, 46, 10, 10);
		}
		else
		{
			gui.drawTexturedModalRect(x + area.start.x + 10, y + area.start.y, 90, 46, 10, 10);
		}
	}

	@Override
	public void update()
	{
		
	}

	@Override
	public boolean eventMouse(int x, int y, int button)
	{
		if(area.MouseIsOnArea(x, y))
		{
			switch_pos = !switch_pos;
			event.event(switch_pos);
			return true;
		}
		return false;
	}

	@Override
	public boolean eventKeyboard(char typedChar, int keyCode)
	{
		return false;
	}

}
