package javapower.netman.gui.util;

import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public interface IGuiMenuElement
{
	public int iconId();
	public void event(Vector2 pos_to_draw);
	public String text();
	public void disable();
	public void draw(Minecraft mc, GuiScreen gui, int x, int y);
	public void update();
	public void eventMouse(int x, int y, int button);
	public boolean isOn(VectorDynamic2 from_super);
	public boolean isEnable();
}
