package javapower.netman.gui.util;

import javapower.netman.eventio.IEventOut;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.Var;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMERadioButton implements IGuiMenuElement
{
	IEventOut<Integer> event;
	Var<Integer> link;
	int id;
	String text;
	public boolean enable = true;
	
	public GMERadioButton(IEventOut<Integer> _event, Var<Integer> _link, int _id, String _text)
	{
		event = _event;
		link = _link;
		id = _id;
		text = _text;
	}
	
	@Override
	public int iconId()
	{
		return -1;
	}

	@Override
	public void event(Vector2 pos_to_draw)
	{
		if(enable)
		{
			link.setVar(id);
			event.event(id);
		}
	}

	@Override
	public String text()
	{
		return text;
	}

	@Override
	public void disable()
	{
		
	}

	@Override
	public void draw(Minecraft mc, GuiScreen gui, int x, int y)
	{
		mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		if(enable)
			GlStateManager.color(1, 1, 1);
		else
			GlStateManager.color(0.25f, 0.25f, 0.25f);
		gui.drawTexturedModalRect(x + 1, y + 1, id == link.var ? 38 : 56, 228, 18, 18);
	}

	@Override
	public void update()
	{
		
	}

	@Override
	public void eventMouse(int x, int y, int button)
	{
		
	}

	@Override
	public boolean isOn(VectorDynamic2 from_super)
	{
		return false;
	}
	
	@Override
	public boolean isEnable()
	{
		return enable;
	}

}