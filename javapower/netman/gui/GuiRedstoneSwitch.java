package javapower.netman.gui;

import java.io.IOException;

import javapower.netman.eventio.IEventIn;
import javapower.netman.eventio.IEventOut;
import javapower.netman.gui.util.GEBistableButton;
import javapower.netman.gui.util.GEIndicator;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.tileentity.TileEntityRedstoneSwitch;
import javapower.netman.util.Vector2;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class GuiRedstoneSwitch extends GuiBase<TileEntityRedstoneSwitch>
{
	GuiTextField textf_customname;
	
	GEBistableButton buttonSW = new GEBistableButton(new Vector2(7, 31), new Vector2(7, 53), mouspos, new IEventOut<Boolean>()
	{
		
		@Override
		public void event(Boolean bool)
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setBoolean("sw", bool);
			sendInfo(nbt);
		}
	});
	
	GEIndicator indicator = new GEIndicator(new Vector2(8, 8), new IEventIn<Boolean>()
	{
		
		@Override
		public Boolean event()
		{
			return buttonSW.sw_pos;
		}
	});
	
	/*GESwitch switch_aextract = new GESwitch(new Vector2(53, 18), new IEventOut<Boolean>()
	{
		
		@Override
		public void event(Boolean bool)
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setBoolean("ae", bool);
			sendInfo(nbt);
		}
	});*/
	
	public GuiRedstoneSwitch(Container inventorySlotsIn, TileEntityRedstoneSwitch te)
	{
		super(inventorySlotsIn, te, new Vector2(0, 0), new Vector2(80, 112));
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		elements.clear();
		elements.add(buttonSW);
		elements.add(indicator);
		//elements.add(switch_aextract);
		
		if(textf_customname == null)
			textf_customname = new GuiTextField(0, mc.fontRenderer, 6, 83, 69, 12);
	}

	@Override
	public void reciveDataFromServer(NBTTagCompound nbt)
	{
		if(nbt.hasKey("sw"))
			buttonSW.sw_pos = nbt.getBoolean("sw");
		
		/*if(nbt.hasKey("ae"))
			switch_aextract.switch_pos = nbt.getBoolean("ae");*/
		
		if(textf_customname != null && nbt.hasKey("cn"))
			textf_customname.setText(nbt.getString("cn"));
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
        textf_customname.mouseClicked(mouseX, mouseY, mouseButton);
        
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		String after = textf_customname.getText();
		textf_customname.textboxKeyTyped(typedChar, keyCode);
		if(after != textf_customname.getText())
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString("cn", textf_customname.getText());
			sendInfo(nbt);
		}
		
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected ResourceLocation getBindTexture()
	{
		return ResourceLocationRegister.texture_redstone_guis;
	}

	@Override
	protected void drawelements(int x, int y)
	{
		//drawString(mc.fontRenderer, "auto out:", x + 5, y + 18, 0xffaa00);
        
        textf_customname.x = x + 6;
        textf_customname.y = y + 83;
        textf_customname.drawTextBox();
	}

	@Override
	protected void update()
	{
		textf_customname.updateCursorCounter();
	}

}
