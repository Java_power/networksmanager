package javapower.netman.gui.terminal;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import javapower.netman.eventio.IEventOut;
import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.util.GEButtonBar;
import javapower.netman.gui.util.GEMenu;
import javapower.netman.gui.util.GMECheckBox;
import javapower.netman.gui.util.GuiPanel;
import javapower.netman.gui.util.IGuiElement;
import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineNetwork;
import javapower.netman.nww.client.MachinesClient.MachineLocalized;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.EScreenAnchor;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class GuiPanelListOfMachines extends GuiPanel
{
	GuiTerminal gui;
	
	List<IGuiElement> elements = new ArrayList<IGuiElement>();
	
	GEButtonBar exit;
	GEButtonBar back;
	GEButtonBar refresh;
	GEButtonBar page_back, page_next;
	GEMenu filter;
	
	int max_x = 0, max_y = 0, page = 0, max_page = 0;
	
	public List<MachineLocalized> machines_filtred = new ArrayList<MachineLocalized>();
	
	boolean filter_energy_rf = true,
			filter_energy_eu = true,
			filter_fluid = true,
			filter_gas = true,
			filter_item = true,
			filter_redstone = true,
			filter_machine = true,
			filter_calculating = true;
	
	
	@Override
	public void initPanel(GuiScreen _gui)
	{
		gui = (GuiTerminal) _gui;
		
		exit = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(0, 0)), new IEventVoid()
		{
			@Override
			public void event()
			{
				gui.mc.player.closeScreen();
			}
			
		}, 0);
		
		back = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(42, 0)), new IEventVoid()
		{
			@Override
			public void event()
			{
				gui.SetGuiPanel(new GuiPanelMainMenu());
			}
			
		}, 1);
		
		refresh = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(84, 0)), new IEventVoid()
		{
			@Override
			public void event()
			{
				NBTTagCompound nbt = new NBTTagCompound();
				nbt.setByte("fgi", (byte)0);
				gui.sendInfo(nbt);
				
				max_y = (gui.height - 30) / 50;
				max_x = (gui.width - 20) / 160;
				
				//max_page = gui.machines.machines.size() / (max_x * max_y) + (gui.machines.machines.size() % (max_x * max_y) != 0 ? 1 : 0);
				//page = 0;
				reCalculatingFilter();
			}
			
		}, 11);
		
		page_back = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(136, 0)), new IEventVoid()
		{

			@Override
			public void event()
			{
				if(page > 0)
					--page;
				else
					page = max_page;
			}
			
		}, 10, true);
		
		page_next = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(220, 0)), new IEventVoid()
		{

			@Override
			public void event()
			{
				if(page < max_page)
					++page;
				else
					page = 0;
			}
			
		}, 10);
		
		// ---------- filter menu ----------
		
		filter = new GEMenu(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(280, 0)), "Type Filter", 90);
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_energy_rf = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.ENERGY_RF.getText(), true));
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_energy_eu = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.ENERGY_EU.getText(), true));
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_fluid = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.FLUID.getText(), true));
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_gas = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.GAS.getText(), true));
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_item = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.ITEM.getText(), true));
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_redstone = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.REDSTONE.getText(), true));
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_machine = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.MACHINE.getText(), true));
		
		filter.elements.add(new GMECheckBox(new IEventOut<Boolean>()
		{
			@Override
			public void event(Boolean out)
			{
				filter_calculating = out;
				reCalculatingFilter();
			}
			
		}, EMachineType.CALCULATING.getText(), true));
		
		// ---------- page setup ----------
		max_y = (gui.thisScreen.screensize.y - 30) / 50;
		max_x = (gui.thisScreen.screensize.x - 20) / 160;
		reCalculatingFilter();
		//max_page = gui.machines.machines.size() / (max_x * max_y);// + (gui.machines.machines.size() % (max_x * max_y) != 0 ? 1 : 0);
		//page = 0;
		
		// ---------- register elements ----------
		
		elements.clear();
		
		elements.add(exit);
		elements.add(back);
		elements.add(refresh);
		
		elements.add(page_back);
		elements.add(page_next);
		
		elements.add(filter);
	}
	
	@Override
	public void draw()
	{
		//Gui.drawRect(0, 0, gui.width, gui.height+1, 0xff212121);
		Gui.drawRect(0, 0, gui.width, 20, 0xff0065ad);
		
		int msize = machines_filtred.size();
		int x1 = 0, y1 = 0;
		for(int i = page*max_x*max_y; i < (msize < (page+1)*max_x*max_y ? msize : (page+1)*max_x*max_y); ++i)
		{
			machines_filtred.get(i).machine.drawGuiInfo((x1*160) + 10, (y1*50) + 30, gui, gui.mc.fontRenderer);
			
			if(y1+1 < max_y)
			{
				++y1;
			}
			else
			{
				y1 = 0;
				++x1;
			}
		}
		
		GL11.glColor4f(1, 1, 1, 1);
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		
		for(IGuiElement e: elements)
		{
			e.draw(gui.mc, gui, 0, 0);
		}
		
		//page n
		gui.drawCenteredString(gui.mc.fontRenderer, (page+1)+"/"+(max_page+1), 198, 7, 0xffffff);
		
	}

	@Override
	public void update()
	{
		for(IGuiElement e: elements)
		{
			e.update();
		}
	}
	
	@Override
	public void onResize(Minecraft mcIn, int w, int h)
	{
		max_y = (h - 30) / 50;
		max_x = (w - 20) / 160;
		
		reCalculatingFilter();
		//max_page = gui.machines.machines.size() / (max_x * max_y);
		//max_page = gui.machines.machines.size() / (max_x * max_y) + (gui.machines.machines.size() % (max_x * max_y) != 0 ? 1 : 0);
		//page = 0;
		
		for(IGuiElement e: elements)
		{
			e.resize(w, h);
		}
	}
	
	@Override
	public boolean mouseClicked(int mouseX, int mouseY, int mouseButton)
	{
		for(IGuiElement e: elements)
		{
			if(e.eventMouse(mouseX, mouseY, mouseButton))
				return true;
		}
		return false;
	}
	
	@Override
	public boolean keyTyped(char typedChar, int keyCode)
	{
		for(IGuiElement e: elements)
		{
			if(e.eventKeyboard(typedChar, keyCode))
				return true;
		}
		return false;
	}
	
	// ---------- filter function ----------
	private void reCalculatingFilter()
	{
		machines_filtred.clear();
		for(MachineLocalized m : gui.machines.machines)
		{
			//TileEntity te = m.loc.GetTileEntity();
			if(m != null && m.machine != null/*te instanceof IMachineNetwork*/)
			{
				EMachineType t = m.machine.machine_type;
				
				if(t == EMachineType.ENERGY_RF && filter_energy_rf)
					machines_filtred.add(m);
				else if(t == EMachineType.ENERGY_EU && filter_energy_eu)
					machines_filtred.add(m);
				else if(t == EMachineType.FLUID && filter_fluid)
					machines_filtred.add(m);
				else if(t == EMachineType.GAS && filter_gas)
					machines_filtred.add(m);
				else if(t == EMachineType.ITEM && filter_item)
					machines_filtred.add(m);
				else if(t == EMachineType.REDSTONE && filter_redstone)
					machines_filtred.add(m);
				else if(t == EMachineType.MACHINE && filter_machine)
					machines_filtred.add(m);
				else if(t == EMachineType.CALCULATING && filter_calculating)
					machines_filtred.add(m);
			}
		}
		
		max_page = machines_filtred.size() / (max_x * max_y);
		page = 0;
	}

}
