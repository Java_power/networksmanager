package javapower.netman.gui.terminal.msg;

import javapower.netman.core.Config;
import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.terminal.project.DElementMapMachine;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.Zone2D;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class GMsgEMachine implements IGuiMessage<GuiPanelMyInstallation>
{
	DElementMapMachine element;
	Zone2D zoneCancel, zoneOk, zoneItemId, zoneItemMeta, zoneLabel, zonePreview;
	boolean isOn_Cancel, isOn_Ok, isOn_item_id, isOn_item_meta, isOn_label, isOn_preview;
	boolean selected_item_id, selected_item_meta, selected_label;
	
	String text;
	int pos_label;
	
	int item_id = 0;
	int item_meta = 0;
	ItemStack item_s = ItemStack.EMPTY;
	
	public GMsgEMachine(DElementMapMachine _element)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
		zoneItemId = new Zone2D(-35, -15, -5, 2);
		zoneItemMeta = new Zone2D(5, -15, 35, 2);
		zoneLabel = new Zone2D(-35, 4, 35, 21);
		zonePreview = new Zone2D(101, -32, 165, 32);
		
		element = _element;
		try
		{
			item_id = Item.getIdFromItem(element.itemstack.getItem());
		}
		catch (NullPointerException e)
		{
			item_id = 0;
		}
		text = element.name != null ? element.name : "";
		pos_label = element.pos_name;
		
		if(element.itemstack != null)
		{
			item_meta = element.itemstack.getItemDamage();
			item_s = element.itemstack.copy();
		}
	}

	@Override
	public void draw(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle)
	{
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 200, y_middle - 75, x_middle + 200, y_middle + 75, 0xff0094ff);
		gui.drawRect(x_middle - 196, y_middle - 71, x_middle + 196, y_middle + 71, 0xff404040);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		GlStateManager.color(1, 1, 1);
		
		gui.drawTexturedModalRect(x_middle - 70, y_middle + 45, 200, 60, 40, 20);
		gui.drawTexturedModalRect(x_middle + 30, y_middle + 45, 200, 160, 40, 20);
		
		if(isOn_Cancel)
		gui.drawRect(x_middle - 70, y_middle + 45, x_middle - 30, y_middle + 65, 0x40ffffff);
		
		if(isOn_Ok)
		gui.drawRect(x_middle + 30, y_middle + 45, x_middle + 70, y_middle + 65, 0x40ffffff);
		
		if(selected_item_id)
			gui.drawRect(x_middle - 36, y_middle - 16, x_middle - 4, y_middle + 3, 0xff0094ff);
		
		if(selected_item_meta)
			gui.drawRect(x_middle + 4, y_middle - 16, x_middle + 36, y_middle + 3, 0xff0094ff);
		
		if(selected_label)
			gui.drawRect(x_middle - 36, y_middle + 3, x_middle + 36, y_middle + 22, 0xff0094ff);
		
		gui.drawRect(x_middle - 35, y_middle - 15, x_middle - 5, y_middle + 2, isOn_item_id ? 0xff303030 : 0xff1e1e1e);
		gui.drawRect(x_middle + 5, y_middle - 15, x_middle + 35, y_middle + 2, isOn_item_meta ? 0xff303030 : 0xff1e1e1e);
		
		gui.drawRect(x_middle -35, y_middle + 4, x_middle + 35, y_middle + 21, isOn_label ? 0xff303030 : 0xff1e1e1e);
		
		gui.drawRect(x_middle + 101, y_middle - 32, x_middle + 165, y_middle + 32, isOn_preview ? 0xff303030 : 0xff1e1e1e);
		
		if(pos_label == 0)
		{
			gui.drawCenteredString(gui.mc.fontRenderer, text, x_middle + 133, y_middle - 43, 0xffffff);
		}
		else if(pos_label == 1)
		{
			gui.drawCenteredString(gui.mc.fontRenderer, text, x_middle + 133, y_middle + 35, 0xffffff);
			/*
				GlStateManager.rotate(90, 0, 0, 1);
				gui.drawCenteredString(gui.mc.fontRenderer, text, y_middle + 35, - (x_middle + 133), 0xffffff);
				GlStateManager.rotate(-90, 0, 0, 1);
			 */
		}
		
		gui.drawString(gui.mc.fontRenderer, "Item Id :         :", x_middle - 77, y_middle - 10, 0xffffff);
		
		gui.drawString(gui.mc.fontRenderer, "Label : "+text, x_middle - 70, y_middle + 9, 0xffffff);
		
		gui.drawString(gui.mc.fontRenderer, ""+item_id, x_middle - 34, y_middle - 10, 0xffffff);
		gui.drawString(gui.mc.fontRenderer, ""+item_meta, x_middle + 7, y_middle - 10, 0xffffff);
		
		RenderHelper.enableGUIStandardItemLighting();
		//if(element.itemstack != null)
		GlStateManager.scale(4f, 4f, 4f);
		if(gui instanceof GuiTerminal && Config.guiTerminal_scrale == 2)
			GlStateManager.translate(-0.5f, 0, 0);
		gui.mc.getRenderItem().renderItemIntoGUI(item_s, x_middle/4 + 26, y_middle/4 - 8);
		if(gui instanceof GuiTerminal && Config.guiTerminal_scrale == 2)
			GlStateManager.translate(0.5f, 0, 0);
		GlStateManager.enableAlpha();
		RenderHelper.disableStandardItemLighting();	
		GlStateManager.scale(0.25f, 0.25f, 0.25f);
		
		gui.drawString(gui.mc.fontRenderer, "Machine Icon", x_middle - 35, y_middle - 65, 0xffffff);
		
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		isOn_Cancel = zoneCancel.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Ok = zoneOk.MouseIsOnArea(x_mouse, y_mouse);
		
		isOn_item_id = zoneItemId.MouseIsOnArea(x_mouse, y_mouse);
		isOn_item_meta = zoneItemMeta.MouseIsOnArea(x_mouse, y_mouse);
		
		isOn_label = zoneLabel.MouseIsOnArea(x_mouse, y_mouse);
		
		isOn_preview = zonePreview.MouseIsOnArea(x_mouse, y_mouse);
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle, int button)
	{
		if(zoneCancel.MouseIsOnArea(x_middle, y_middle))
		{
			panel.messageBox_close();
		}
		
		if(zoneOk.MouseIsOnArea(x_middle, y_middle))
		{
			element.itemstack = item_s.copy();
			element.name = text;
			element.pos_name = pos_label;
			panel.project.changment = true;
			panel.messageBox_close();
		}
		
		if(zonePreview.MouseIsOnArea(x_middle, y_middle))
		{
			if(pos_label > 0)
				pos_label = 0;
			else
				++pos_label;
		}
		
		if(zoneItemId.MouseIsOnArea(x_middle, y_middle))
		{
			selected_item_id = true;
			selected_item_meta = false;
			selected_label = false;
		}
		
		if(zoneItemMeta.MouseIsOnArea(x_middle, y_middle))
		{
			selected_item_id = false;
			selected_item_meta = true;
			selected_label = false;
		}
		
		if(zoneLabel.MouseIsOnArea(x_middle, y_middle))
		{
			selected_item_id = false;
			selected_item_meta = false;
			selected_label = true;
		}
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanelMyInstallation panel, char typedChar, int keyCode)
	{
		boolean modify = false;
		
		if(selected_item_id)
		{
			if("0123456789".indexOf(typedChar) != -1)
			{
				item_id = (item_id*10) + Integer.parseInt(""+typedChar);
				modify = true;
			}
		}
		else if(selected_item_meta)
		{
			if("0123456789".indexOf(typedChar) != -1)
			{
				item_meta = (item_meta*10) + Integer.parseInt(""+typedChar);
				modify = true;
			}
		}
		else if(selected_label)
		{
			if("1234567890azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN &#{}[]()|*-+/,;:!?.~_\\^@=��$��%��<>".indexOf(typedChar) != -1 && text.length() < 10)
			{
				text += typedChar;
			}
		}
		
		if(keyCode == 14)
		{
			if(selected_item_id)
			{
				item_id = item_id/10;
				modify = true;
			}
			else if(selected_item_meta)
			{
				item_meta = item_meta/10;
				modify = true;
			}
			else if(selected_label)
			{
				if(text.length() > 0)
				{
					text = text.substring(0, text.length()-1);
				}
			}
		}
		
		if(modify)
		{
			Item i = Item.getItemById(item_id);
			if(i != null)
			{
				item_s = new ItemStack(i, 1, item_meta);
			}
			else
			{
				item_s = ItemStack.EMPTY;
			}
		}
	}

	@Override
	public void resize(int w, int h)
	{
		
	}

}
