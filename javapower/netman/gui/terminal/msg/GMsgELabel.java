package javapower.netman.gui.terminal.msg;

import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.terminal.project.DElementMapLabel;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.Zone2D;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMsgELabel implements IGuiMessage<GuiPanelMyInstallation>
{
	DElementMapLabel element;
	Zone2D zoneCancel, zoneOk, zoneLabel;
	boolean isOn_Cancel, isOn_Ok, isOn_label;
	boolean selected_label;
	
	String text;
	
	public GMsgELabel(DElementMapLabel _element)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
		zoneLabel = new Zone2D(-35, -15, 35, 2);
		
		element = _element;
		
		text = element.label != null ? element.label : "";
	}

	@Override
	public void draw(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle)
	{
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 200, y_middle - 75, x_middle + 200, y_middle + 75, 0xff0094ff);
		gui.drawRect(x_middle - 196, y_middle - 71, x_middle + 196, y_middle + 71, 0xff404040);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		GlStateManager.color(1, 1, 1);
		
		gui.drawTexturedModalRect(x_middle - 70, y_middle + 45, 200, 60, 40, 20);
		gui.drawTexturedModalRect(x_middle + 30, y_middle + 45, 200, 160, 40, 20);
		
		if(isOn_Cancel)
		gui.drawRect(x_middle - 70, y_middle + 45, x_middle - 30, y_middle + 65, 0x40ffffff);
		
		if(isOn_Ok)
		gui.drawRect(x_middle + 30, y_middle + 45, x_middle + 70, y_middle + 65, 0x40ffffff);
		
		if(selected_label)
			gui.drawRect(x_middle - 36, y_middle - 16, x_middle + 36, y_middle + 3, 0xff0094ff);
		
		gui.drawRect(x_middle -35, y_middle - 15, x_middle + 35, y_middle + 2, isOn_label ? 0xff303030 : 0xff1e1e1e);
		
		gui.drawString(gui.mc.fontRenderer, "Label : "+text, x_middle - 70, y_middle - 10, 0xffffff);
		
		gui.drawString(gui.mc.fontRenderer, "Label", x_middle - 35, y_middle - 65, 0xffffff);
		
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		isOn_Cancel = zoneCancel.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Ok = zoneOk.MouseIsOnArea(x_mouse, y_mouse);
		
		isOn_label = zoneLabel.MouseIsOnArea(x_mouse, y_mouse);
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle, int button)
	{
		if(zoneCancel.MouseIsOnArea(x_middle, y_middle))
		{
			panel.messageBox_close();
		}
		
		if(zoneOk.MouseIsOnArea(x_middle, y_middle))
		{
			element.label = text;
			panel.project.changment = true;
			panel.messageBox_close();
		}
		
		selected_label = zoneLabel.MouseIsOnArea(x_middle, y_middle);
		
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanelMyInstallation panel, char typedChar, int keyCode)
	{
		boolean modify = false;
		
		if(selected_label)
		{
			if("1234567890azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN &#{}[]()|*-+/,;:!?.~_\\^@=��$��%��<>".indexOf(typedChar) != -1 && text.length() < 10)
			{
				text += typedChar;
			}
			
			if(keyCode == 14 && text.length() > 0)
			{
				text = text.substring(0, text.length()-1);
			}
		}
	}

	@Override
	public void resize(int w, int h)
	{
		
	}

}
