package javapower.netman.gui.terminal.msg;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.Zone2D;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMsgRawString implements IGuiMessage<GuiPanelMyInstallation>
{
	Zone2D zoneCancel, zoneOk;
	boolean isOn_Cancel, isOn_Ok;
	IEventVoid event_cancel, event_ok;
	String tile;
	String[] msg;
	
	public GMsgRawString(IEventVoid eventCancel, IEventVoid eventOk, String _tile, String[] _msg)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
		
		event_cancel = eventCancel;
		event_ok = eventOk;
		
		tile = _tile;
		msg = _msg;
	}
	
	public GMsgRawString(String _tile, String[] _msg)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
		
		event_cancel = null;
		event_ok = null;
		
		tile = _tile;
		msg = _msg;
	}

	@Override
	public void draw(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle)
	{
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 200, y_middle - 75, x_middle + 200, y_middle + 75, 0xff0094ff);
		gui.drawRect(x_middle - 196, y_middle - 71, x_middle + 196, y_middle + 71, 0xff404040);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		GlStateManager.color(1, 1, 1);
		
		gui.drawTexturedModalRect(x_middle - 70, y_middle + 45, 200, 60, 40, 20);
		gui.drawTexturedModalRect(x_middle + 30, y_middle + 45, 200, 160, 40, 20);
		
		if(isOn_Cancel)
		gui.drawRect(x_middle - 70, y_middle + 45, x_middle - 30, y_middle + 65, 0x40ffffff);
		
		if(isOn_Ok)
		gui.drawRect(x_middle + 30, y_middle + 45, x_middle + 70, y_middle + 65, 0x40ffffff);
		
		if(tile != null)
			gui.drawCenteredString(gui.mc.fontRenderer, tile, x_middle, y_middle - 65, 0xffffff);
		
		if(msg != null)
		{
			int ystart = (msg.length/2)*10;
			int decal = 0;
			
			for(String linemsg : msg)
			{
				gui.drawCenteredString(gui.mc.fontRenderer, linemsg, x_middle, (y_middle - ystart) + decal*10, 0xffffff);
				++decal;
			}
		}
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		isOn_Cancel = zoneCancel.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Ok = zoneOk.MouseIsOnArea(x_mouse, y_mouse);
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle, int button)
	{
		if(zoneCancel.MouseIsOnArea(x_middle, y_middle))
		{
			if(event_cancel != null)
				event_cancel.event();
			panel.messageBox_close();
		}
		if(zoneOk.MouseIsOnArea(x_middle, y_middle))
		{
			if(event_ok != null)
				event_ok.event();
			panel.messageBox_close();
		}
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanelMyInstallation panel, char typedChar, int keyCode)
	{
		
	}

	@Override
	public void resize(int w, int h)
	{
		
	}

}
