package javapower.netman.gui.terminal.msg;

import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.terminal.project.Element;
import javapower.netman.gui.terminal.project.Project;
import javapower.netman.gui.terminal.project.RegisterElements;
import javapower.netman.gui.util.GuiPanel;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMsgSelectElement implements IGuiMessage
{
	Project project;
	GuiPanelMyInstallation guipan;
	int elementid = -1;
	
	public GMsgSelectElement(Project _project, GuiPanelMyInstallation _gui)
	{
		project = _project;
		guipan = _gui;
	}

	@Override
	public void draw(GuiScreen gui, GuiPanel panel, int x_middle, int y_middle)
	{
		int xx = 0, yy = 0;
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 94, y_middle - 80, x_middle + 94, y_middle + 72, 0xff0094ff);
		gui.drawRect(x_middle - 90, y_middle - 68, x_middle + 90, y_middle + 68, 0xff404040);
		int id = 0;
		for(Element e : RegisterElements.elements)
		{
			gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_items);
			if(e != null)
			{
				int[] icon = e.getIcon();
				
				if(icon.length >= 5)
				{
					GlStateManager.color((float)(icon[4] >> 16 & 255) / 255.0F, (float)(icon[4] >> 8 & 255) / 255.0F, (float)(icon[4] & 255) / 255.0F);
				}
				else
					GlStateManager.color(1, 1, 1);
				gui.drawTexturedModalRect(x_middle - 87 + xx*22, y_middle - 65 + yy*22, icon[0], icon[1], icon[2], icon[3]);
				if(id == elementid)
				{
					gui.drawRect(x_middle - 87 + xx*22, y_middle - 65 + yy*22, x_middle - 87 + xx*22 +20, y_middle - 65 + yy*22 + 20, 0x44ffffff);
					gui.drawCenteredString(gui.mc.fontRenderer, e.getName(), x_middle, y_middle - 78, 0xffffff);
				}
			}
			if(xx >= 7)
			{
				xx = 0;
				++yy;
			}
			else
			{
				++xx;
			}
			++id;
		}
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		int mx = (x_mouse + 87)/22;
		int my = (y_mouse + 65)/22;
		if(x_mouse >= -87 && y_mouse >= -65 && x_mouse <= 87 && y_mouse <= 65)
		{
			int id = 8*my + mx;
			if(id < RegisterElements.elements.length)
				elementid = id;
			else
				elementid = -1;
		}
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanel panel, int x_middle, int y_middle, int button)
	{
		if(elementid != -1)
		{
			project.current_element_id = elementid;
			guipan.messageBox_close();
		}
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanel panel, char typedChar, int keyCode)
	{

	}

	@Override
	public void resize(int w, int h)
	{

	}

}
