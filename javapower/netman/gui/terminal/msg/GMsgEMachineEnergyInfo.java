package javapower.netman.gui.terminal.msg;

import java.util.ArrayList;
import java.util.List;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.terminal.project.DElementMapEnergyInfo;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.nww.client.MachineCL_EnergyStorageInfo;
import javapower.netman.nww.client.MachinesClient.MachineLocalized;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.Zone2D;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMsgEMachineEnergyInfo implements IGuiMessage<GuiPanelMyInstallation>
{
	DElementMapEnergyInfo eenergyInfo;
	Zone2D zoneCancel, zoneOk, zoneBack, zoneNext;
	boolean isOn_Cancel, isOn_Ok, isOn_Back, isOn_Next;
	
	public List<MachineLocalized> machines_filtred = new ArrayList<MachineLocalized>();
	int id_selected = 0;
	
	public GMsgEMachineEnergyInfo(GuiTerminal terminal, DElementMapEnergyInfo _energyInfo)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
		zoneBack = new Zone2D(-60, 4, -20, 24);
		zoneNext = new Zone2D(20, 4, 60, 24);
		
		eenergyInfo = _energyInfo;
		
		machines_filtred.clear();
		
		for(MachineLocalized machloc : terminal.machines.machines)
		{
			if(machloc.machine instanceof MachineCL_EnergyStorageInfo)
			{
				machines_filtred.add(machloc);
			}
		}
	}

	@Override
	public void draw(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle)
	{
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 200, y_middle - 75, x_middle + 200, y_middle + 75, 0xff0094ff);
		gui.drawRect(x_middle - 196, y_middle - 71, x_middle + 196, y_middle + 71, 0xff404040);
		
		gui.drawRect(x_middle - 80, y_middle - 45, x_middle + 80, y_middle + 30, 0xff2d2d2d);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		GlStateManager.color(1, 1, 1);
		
		gui.drawTexturedModalRect(x_middle - 70, y_middle + 45, 200, 60, 40, 20);
		gui.drawTexturedModalRect(x_middle + 30, y_middle + 45, 200, 160, 40, 20);
		
		gui.drawTexturedModalRect(x_middle - 60, y_middle + 4, 119, 228, 40, 20);//<
		gui.drawTexturedModalRect(x_middle + 20, y_middle + 4, 200, 200, 40, 20);//>
		
		if(isOn_Cancel)
			gui.drawRect(x_middle - 70, y_middle + 45, x_middle - 30, y_middle + 65, 0x40ffffff);
			
		if(isOn_Ok)
			gui.drawRect(x_middle + 30, y_middle + 45, x_middle + 70, y_middle + 65, 0x40ffffff);
			
		if(isOn_Back)
			gui.drawRect(x_middle - 60, y_middle + 4, x_middle - 20, y_middle + 24, 0x40ffffff);
				
		if(isOn_Next)
			gui.drawRect(x_middle + 20, y_middle + 4, x_middle + 60, y_middle + 24, 0x40ffffff);
				
		gui.drawCenteredString(gui.mc.fontRenderer, "Select Machine Location", x_middle, y_middle - 65, 0xffffff);
		gui.drawCenteredString(gui.mc.fontRenderer, ""+id_selected, x_middle, y_middle + 10, 0xffffff);
		
		if(machines_filtred.size() > 0)
		{
			MachineLocalized ml = machines_filtred.get(id_selected);
			if(ml != null && ml.machine != null)
			{
				ml.machine.drawGuiInfo(x_middle - 75, y_middle - 40, gui, gui.mc.fontRenderer);
			}
		}
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		isOn_Cancel = zoneCancel.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Ok = zoneOk.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Back = zoneBack.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Next = zoneNext.MouseIsOnArea(x_mouse, y_mouse);
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle, int button)
	{
		if(zoneCancel.MouseIsOnArea(x_middle, y_middle))
		{
			panel.messageBox_close();
		}
		
		if(zoneOk.MouseIsOnArea(x_middle, y_middle))
		{
			if(eenergyInfo != null && machines_filtred.size() > 0 && machines_filtred.get(id_selected) != null && machines_filtred.get(id_selected).machine instanceof MachineCL_EnergyStorageInfo)
			{
				eenergyInfo.machine_energyInfo = (MachineCL_EnergyStorageInfo) machines_filtred.get(id_selected).machine;
				eenergyInfo.posdim = machines_filtred.get(id_selected).loc;
			}
			panel.project.changment = true;
			panel.messageBox_close();
		}
		
		if(zoneBack.MouseIsOnArea(x_middle, y_middle))
		{
			if(id_selected <= 0)
				id_selected = machines_filtred.size()-1;
			else
				--id_selected;
		}
		
		if(zoneNext.MouseIsOnArea(x_middle, y_middle))
		{
			if(id_selected < machines_filtred.size()-1)
				++id_selected;
			else
				id_selected = 0;
		}
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanelMyInstallation panel, char typedChar, int keyCode)
	{
		
	}

	@Override
	public void resize(int w, int h)
	{
		
	}

}