package javapower.netman.gui.terminal.msg;

import javapower.netman.eventio.IEventOut;
import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.terminal.project.Page;
import javapower.netman.gui.util.GEMenu;
import javapower.netman.gui.util.GMERadioButton;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.EScreenAnchor;
import javapower.netman.util.Var;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMsgSetDefaultPage implements IGuiMessage<GuiPanelMyInstallation>
{
	Zone2D zoneCancel, zoneOk;
	boolean isOn_Cancel, isOn_Ok;
	
	GEMenu menu_page;
	Var<Integer> menu_link;
	
	public GMsgSetDefaultPage(GuiPanelMyInstallation panel)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
		menu_link = new Var<Integer>(0);
		
		menu_page = new GEMenu(new VectorDynamic2(panel.gui.thisScreen, EScreenAnchor.MIDDLE, new Vector2(20, -16)), "page", 100);
		int id = 0;
		for(Page p : panel.project.pages)
		{
			menu_page.elements.add(new GMERadioButton(new IEventOut<Integer>()
			{
				
				@Override
				public void event(Integer out)
				{
					
				}
				
			}, menu_link, id, p.name));
			++id;
		}
	}

	@Override
	public void draw(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle)
	{
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 200, y_middle - 75, x_middle + 200, y_middle + 75, 0xff0094ff);
		gui.drawRect(x_middle - 196, y_middle - 71, x_middle + 196, y_middle + 71, 0xff404040);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		GlStateManager.color(1, 1, 1);
		
		gui.drawTexturedModalRect(x_middle - 70, y_middle + 45, 200, 60, 40, 20);
		gui.drawTexturedModalRect(x_middle + 30, y_middle + 45, 200, 160, 40, 20);
		
		if(isOn_Cancel)
		gui.drawRect(x_middle - 70, y_middle + 45, x_middle - 30, y_middle + 65, 0x40ffffff);
		
		if(isOn_Ok)
		gui.drawRect(x_middle + 30, y_middle + 45, x_middle + 70, y_middle + 65, 0x40ffffff);
		
		gui.drawString(gui.mc.fontRenderer, "Default Page", x_middle - 22, y_middle - 65, 0xffffff);
		gui.drawString(gui.mc.fontRenderer, "Choose the default page :", x_middle - 130, y_middle - 10, 0xffffff);
		
		menu_page.draw(gui.mc, gui, 0, 0);
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		isOn_Cancel = zoneCancel.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Ok = zoneOk.MouseIsOnArea(x_mouse, y_mouse);
		menu_page.update();
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle, int button)
	{
		if(menu_page.eventMouse(x_middle, y_middle, button)) return;
		if(zoneCancel.MouseIsOnArea(x_middle, y_middle))
		{
			panel.messageBox_close();
		}
		if(zoneOk.MouseIsOnArea(x_middle, y_middle))
		{
			if(menu_link.var < panel.project.pages.size())
			{
				if(menu_link.var.equals(panel.project.current_page_id))
					panel.project.setPage(-1);
				
				panel.project.default_page = (int) menu_link.var;
				panel.project.changment = true;
				//panel.project.updateMenuPages();
			}
			
			panel.messageBox_close();
		}
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanelMyInstallation panel, char typedChar, int keyCode)
	{
		
	}

	@Override
	public void resize(int w, int h)
	{
		menu_page.resize(w, h);
	}
}
