package javapower.netman.gui.terminal.msg;

import javapower.netman.eventio.IEventOut;
import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.terminal.project.Page;
import javapower.netman.gui.util.GEMenu;
import javapower.netman.gui.util.GMERadioButton;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.EScreenAnchor;
import javapower.netman.util.Var;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import javapower.netman.util.Zone2D;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMsgAddPage implements IGuiMessage<GuiPanelMyInstallation>
{
	Zone2D zoneCancel, zoneOk;
	boolean isOn_Cancel, isOn_Ok;
	
	String text = "";
	boolean blink;
	long time = System.currentTimeMillis();
	//int map_resol = 0
	GEMenu menu_map_resolution;
	Var<Integer> menu_link;
	
	public GMsgAddPage(GuiTerminal gui)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
		menu_link = new Var<Integer>(0);
		
		menu_map_resolution = new GEMenu(new VectorDynamic2(gui.thisScreen, EScreenAnchor.MIDDLE, new Vector2(75, -16)), "map size", 100);
		int id = 0;
		for(Vector2 resol : gui.map_sizes)
		{
			menu_map_resolution.elements.add(new GMERadioButton(new IEventOut<Integer>()
			{
				@Override
				public void event(Integer out)
				{
					
				}
				
			}, menu_link, id, resol.x+"x"+resol.y));
			++id;
		}
	}

	@Override
	public void draw(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle)
	{
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 200, y_middle - 75, x_middle + 200, y_middle + 75, 0xff0094ff);
		gui.drawRect(x_middle - 196, y_middle - 71, x_middle + 196, y_middle + 71, 0xff404040);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		GlStateManager.color(1, 1, 1);
		
		gui.drawTexturedModalRect(x_middle - 70, y_middle + 45, 200, 60, 40, 20);
		gui.drawTexturedModalRect(x_middle + 30, y_middle + 45, 200, 160, 40, 20);
		
		if(isOn_Cancel)
		gui.drawRect(x_middle - 70, y_middle + 45, x_middle - 30, y_middle + 65, 0x40ffffff);
		
		if(isOn_Ok)
		gui.drawRect(x_middle + 30, y_middle + 45, x_middle + 70, y_middle + 65, 0x40ffffff);
		
		gui.drawRect(x_middle - 18, y_middle - 15, x_middle + 70, y_middle + 2, 0xff1e1e1e);
		gui.drawString(gui.mc.fontRenderer, "New Page", x_middle - 22, y_middle - 65, 0xffffff);
		gui.drawString(gui.mc.fontRenderer, "Name (13 character max): "+text+ (blink ? "_" : ""), x_middle - 150, y_middle - 10, 0xffffff);
		
		menu_map_resolution.draw(gui.mc, gui, 0, 0);
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		isOn_Cancel = zoneCancel.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Ok = zoneOk.MouseIsOnArea(x_mouse, y_mouse);
		menu_map_resolution.update();
		
		if(System.currentTimeMillis() - time > 500)
		{
			time = System.currentTimeMillis();
			blink = !blink;
		}
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle, int button)
	{
		if(menu_map_resolution.eventMouse(x_middle, y_middle, button)) return;
		if(zoneCancel.MouseIsOnArea(x_middle, y_middle))
		{
			panel.messageBox_close();
		}
		if(zoneOk.MouseIsOnArea(x_middle, y_middle))
		{
			int name_add = 2;
			String text_page = text;
			while(true)
			{
				boolean exist = false;
				for(Page p : panel.project.pages)
				{
					if(p != null && p.name.equals(text_page))
					{
						exist = true;
						break;
					}
				}
				
				if(exist)
				{
					text_page = text +" "+name_add;
					++name_add;
				}
				else
				{
					break;
				}
			}
			
			Page new_page = new Page(text_page, panel.gui.map_sizes[menu_link.var].x, panel.gui.map_sizes[menu_link.var].y);
			new_page.initClear();
			panel.project.addPage(new_page);
			if(panel.project.pages.size() == 1)
			{
				panel.project.setPage(0);
			}
			panel.project.updateMenuPages();
			panel.messageBox_close();
		}
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanelMyInstallation panel, char typedChar, int keyCode)
	{
		if("1234567890azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN &#{}[]()|*-+/,;:!?.~_\\^@=��$��%��<>".indexOf(typedChar) != -1 && text.length() < 13)
		{
			text += typedChar;
		}
		else if(keyCode == 14 && text.length() > 0)
		{
			text = text.substring(0, text.length()-1);
		}
	}

	@Override
	public void resize(int w, int h)
	{
		menu_map_resolution.resize(w, h);
	}

}
