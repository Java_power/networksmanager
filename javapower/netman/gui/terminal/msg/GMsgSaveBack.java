package javapower.netman.gui.terminal.msg;

import javapower.netman.gui.terminal.GuiPanelMainMenu;
import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.Zone2D;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GMsgSaveBack implements IGuiMessage<GuiPanelMyInstallation>
{
	Zone2D zoneCancel, zoneOk;
	boolean isOn_Cancel, isOn_Ok;
	
	public GMsgSaveBack(GuiPanelMyInstallation panel)
	{
		zoneCancel = new Zone2D(-70, 45, -30, 65);
		zoneOk = new Zone2D(30, 45, 70, 65);
	}

	@Override
	public void draw(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle)
	{
		gui.drawRect(0, 0, gui.width, gui.height, 0x80000000);
		
		gui.drawRect(x_middle - 200, y_middle - 75, x_middle + 200, y_middle + 75, 0xff0094ff);
		gui.drawRect(x_middle - 196, y_middle - 71, x_middle + 196, y_middle + 71, 0xff404040);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		GlStateManager.color(1, 1, 1);
		
		gui.drawTexturedModalRect(x_middle - 70, y_middle + 45, 200, 60, 40, 20);
		gui.drawTexturedModalRect(x_middle + 30, y_middle + 45, 200, 160, 40, 20);
		
		if(isOn_Cancel)
		gui.drawRect(x_middle - 70, y_middle + 45, x_middle - 30, y_middle + 65, 0x40ffffff);
		
		if(isOn_Ok)
		gui.drawRect(x_middle + 30, y_middle + 45, x_middle + 70, y_middle + 65, 0x40ffffff);
		
		gui.drawString(gui.mc.fontRenderer, "Save ? Exit", x_middle - 22, y_middle - 65, 0xffffff);
		gui.drawString(gui.mc.fontRenderer, "Do you want to save changes before exit ?", x_middle - 130, y_middle - 10, 0xffffff);
	}

	@Override
	public void update(int x_mouse, int y_mouse)
	{
		isOn_Cancel = zoneCancel.MouseIsOnArea(x_mouse, y_mouse);
		isOn_Ok = zoneOk.MouseIsOnArea(x_mouse, y_mouse);
	}

	@Override
	public void eventMouse(GuiScreen gui, GuiPanelMyInstallation panel, int x_middle, int y_middle, int button)
	{
		if(zoneCancel.MouseIsOnArea(x_middle, y_middle))
		{
			panel.messageBox_close();
			panel.gui.SetGuiPanel(new GuiPanelMainMenu());
		}
		if(zoneOk.MouseIsOnArea(x_middle, y_middle))
		{
			panel.saveChanges();
			panel.messageBox_close();
			panel.gui.SetGuiPanel(new GuiPanelMainMenu());
		}
	}

	@Override
	public void eventKeyboard(GuiScreen gui, GuiPanelMyInstallation panel, char typedChar, int keyCode)
	{
		
	}

	@Override
	public void resize(int w, int h)
	{
		
	}
}