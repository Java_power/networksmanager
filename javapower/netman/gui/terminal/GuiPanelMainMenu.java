package javapower.netman.gui.terminal;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.util.GEButtonBar;
import javapower.netman.gui.util.GEButtonTile;
import javapower.netman.gui.util.GuiPanel;
import javapower.netman.gui.util.IGuiElement;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.EScreenAnchor;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;

public class GuiPanelMainMenu extends GuiPanel
{
	GuiTerminal gui;
	
	List<IGuiElement> elements = new ArrayList<IGuiElement>();
	
	// ---------- elements ----------
	
	GEButtonTile myInstallation;
	GEButtonTile listOfMaterial;
	//GEButtonTile info;
	//GEButtonTile parameter;
	GEButtonBar exit;
	
	
	// ---------- end elements ----------
	
	@Override
	public void initPanel(GuiScreen _gui)
	{
		gui = (GuiTerminal) _gui;
		
		//-215 -50
		myInstallation = new GEButtonTile(new VectorDynamic2(gui.thisScreen, EScreenAnchor.MIDDLE, new Vector2(-105, -50)), new IEventVoid()
		{
			
			@Override
			public void event()
			{
				gui.SetGuiPanel(new GuiPanelMyInstallation());
			}
			
		}, 0);
		
		//-105, -50
		listOfMaterial = new GEButtonTile(new VectorDynamic2(gui.thisScreen, EScreenAnchor.MIDDLE, new Vector2(5, -50)), new IEventVoid()
		{
			
			@Override
			public void event()
			{
				gui.SetGuiPanel(new GuiPanelListOfMachines());
			}
			
		}, 1);
		
		/*info = new GEButtonTile(new VectorDynamic2(gui.thisScreen, EScreenAnchor.MIDDLE, new Vector2(5, -50)), new IEventVoid()
		{
			
			@Override
			public void event()
			{
				System.out.println("event button info");
			}
			
		}, 2);
		
		parameter = new GEButtonTile(new VectorDynamic2(gui.thisScreen, EScreenAnchor.MIDDLE, new Vector2(115, -50)), new IEventVoid()
		{
			
			@Override
			public void event()
			{
				gui.SetGuiPanel(new GuiPanelParameter());
			}
			
		}, 3);*/
		
		exit = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(0, 0)), new IEventVoid()
		{
			
			@Override
			public void event()
			{
				gui.mc.player.closeScreen();
			}
			
		}, 0);
		
		// ---------- register elements ----------
		
		elements.clear();
		
		elements.add(myInstallation);
		elements.add(listOfMaterial);
		//elements.add(info);
		//elements.add(parameter);
		elements.add(exit);
		
	}
	
	@Override
	public void draw()
	{
		//Gui.drawRect(0, 0, gui.width, gui.height+1, 0xff212121);
		Gui.drawRect(0, 0, gui.width, 20, 0xff0065ad);
		//gui.drawCenteredString(gui.mc.fontRenderer, "preview indev !", gui.width/2, 40, 0xff2020);	//TODO
		GL11.glColor4f(1, 1, 1, 1);
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		
		for(IGuiElement e: elements)
		{
			e.draw(gui.mc, gui, 0, 0);
		}
	}

	@Override
	public void update()
	{
		for(IGuiElement e: elements)
		{
			e.update();
		}
	}
	
	public void onResize(net.minecraft.client.Minecraft mcIn, int w, int h)
	{
		for(IGuiElement e: elements)
		{
			e.resize(w, h);
		}
	}
	
	@Override
	public boolean mouseClicked(int mouseX, int mouseY, int mouseButton)
	{
		for(IGuiElement e: elements)
		{
			if(e.eventMouse(mouseX, mouseY, mouseButton))
				return true;
		}
		return false;
	}
	
	@Override
	public boolean keyTyped(char typedChar, int keyCode)
	{
		for(IGuiElement e: elements)
		{
			if(e.eventKeyboard(typedChar, keyCode))
				return true;
		}
		return false;
	}

}
