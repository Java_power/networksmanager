package javapower.netman.gui.terminal;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgAddPage;
import javapower.netman.gui.terminal.msg.GMsgDeletPage;
import javapower.netman.gui.terminal.msg.GMsgSaveBack;
import javapower.netman.gui.terminal.msg.GMsgSaveExit;
import javapower.netman.gui.terminal.msg.GMsgSelectElement;
import javapower.netman.gui.terminal.msg.GMsgSetDefaultPage;
import javapower.netman.gui.terminal.project.Project;
import javapower.netman.gui.util.GEAera;
import javapower.netman.gui.util.GEButtonBar;
import javapower.netman.gui.util.GEMenu;
import javapower.netman.gui.util.GMEButton;
import javapower.netman.gui.util.GuiPanel;
import javapower.netman.gui.util.IGuiElement;
import javapower.netman.gui.util.IGuiMessage;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.EScreenAnchor;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;

public class GuiPanelMyInstallation extends GuiPanel
{
	public GuiTerminal gui;
	
	List<IGuiElement> elements = new ArrayList<IGuiElement>();
	IGuiMessage<GuiPanelMyInstallation> messagebox = null;
	
	GEButtonBar exit;
	GEButtonBar back;
	GEAera element_selector;
	public GEMenu menu_edit, menu_page;
	
	public boolean editmode = false;
	public Project project;
	
	private GuiPanelMyInstallation _this = this;
	
	@Override
	public void initPanel(GuiScreen _gui)
	{
		gui = (GuiTerminal) _gui;
		project = new Project(this);
		project.loadFromServer();
		// ---------- button bar ----------
		
		exit = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(0, 0)), new IEventVoid()
		{
			@Override
			public void event()
			{
				if(project.changment && config_editmodeAccess())
				{
					messageBox_set(new GMsgSaveExit(_this));
				}
				else gui.mc.player.closeScreen();
			}
			
		}, 0);
		
		back = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(42, 0)), new IEventVoid()
		{
			@Override
			public void event()
			{
				if(project.changment && config_editmodeAccess())
				{
					messageBox_set(new GMsgSaveBack(_this));
				}
				else gui.SetGuiPanel(new GuiPanelMainMenu());
			}
			
		}, 1);
		
		element_selector = new GEAera(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_RIGHT, new Vector2(-50, 30)), new IEventVoid()
		{
			
			@Override
			public void event()
			{
				messageBox_set(new GMsgSelectElement(project, _this));
			}
			
		}, 40, 50);
		element_selector.setEnable(editmode);
		// ---------- menu elements ----------
		
		menu_edit = new GEMenu(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(84, 0)), "Edit", 120);
		menu_page = new GEMenu(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(206, 0)), "Page", 120);
		
		boolean editmodAccess = config_editmodeAccess();
		
		GMEButton button_editmode = new GMEButton(14, new IEventVoid()
		{
			
			@Override
			public void event()
			{
				editmode = ! editmode;
				element_selector.setEnable(editmode);
			}
			
		}, "edit mode");
		button_editmode.enable = editmodAccess;
		
		GMEButton button_newpage = new GMEButton(9, new IEventVoid()
		{
			
			@Override
			public void event()
			{
				messageBox_set(new GMsgAddPage(gui));
			}
			
		}, "new page");
		button_newpage.enable = editmodAccess;
		
		GMEButton button_deletepage = new GMEButton(15, new IEventVoid()
		{
			
			@Override
			public void event()
			{
				messageBox_set(new GMsgDeletPage(_this));
			}
			
		}, "delete page");
		button_deletepage.enable = editmodAccess;
		
		GMEButton button_defaultpage = new GMEButton(1, new IEventVoid()
		{
			
			@Override
			public void event()
			{
				messageBox_set(new GMsgSetDefaultPage(_this));
			}
			
		}, "set default page");
		button_defaultpage.enable = editmodAccess;
		
		GMEButton button_savepage = new GMEButton(10, new IEventVoid()
		{
			
			@Override
			public void event()
			{
				project.sendToServer();
				project.changment = false;
			}
			
		}, "save project");
		button_savepage.enable = editmodAccess;
		
		GMEButton button_loadpage = new GMEButton(11, new IEventVoid()
		{
			
			@Override
			public void event()
			{
				project.loadFromServer();
			}
			
		}, "load project");
		//button_loadpage.enable = editmodAccess;
		
		menu_edit.elements.add(button_editmode);
		menu_edit.elements.add(button_newpage);
		menu_edit.elements.add(button_deletepage);
		menu_edit.elements.add(button_defaultpage);
		menu_edit.elements.add(button_savepage);
		menu_edit.elements.add(button_loadpage);
		// ---------- register elements ----------
		
		elements.clear();
		
		elements.add(exit);
		elements.add(back);
		elements.add(element_selector);
		
		elements.add(menu_edit);
		elements.add(menu_page);
	}
	
	@Override
	public void draw()
	{
		//Gui.drawRect(0, 0, gui.width, gui.height+1, 0xff212121);
		GL11.glColor4f(1, 1, 1, 1);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_items);
		project.draw(gui.width/2, gui.height/2);
		
		Gui.drawRect(0, 0, gui.width, 20, 0xdd0065ad);
		GL11.glColor4f(1, 1, 1, 1);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		
		if(editmode)
		{
			gui.drawTexturedModalRect(gui.width - 40, 0, 79, 228, 40, 20);
		}
		
		for(IGuiElement e: elements)
		{
			e.draw(gui.mc, gui, 0, 0);
		}
		
		if(menu_page.enable && menu_page.elements.size() > 0)
		{
			gui.drawString(gui.mc.fontRenderer, "\u25CF", 209, 19 + project.current_page_id*20, 0xff0000);
			gui.drawString(gui.mc.fontRenderer, "\u25CF", 213, 19 + project.default_page*20, 0x00ffff);
		}
		
		if(messagebox != null)
			messagebox.draw(gui, this, gui.width/2, gui.height/2);
	}

	@Override
	public void update()
	{
		if(messagebox != null)
			messagebox.update(gui.thisScreen.mouse.x - gui.width/2, gui.thisScreen.mouse.y - gui.height/2);
		
		project.update();
		
		for(IGuiElement e: elements)
		{
			e.update();
		}
	}
	
	@Override
	public void onResize(Minecraft mcIn, int w, int h)
	{
		if(messagebox != null)
			messagebox.resize(w, h);
		else
		{
			for(IGuiElement e: elements)
			{
				e.resize(w, h);
			}
		}
	}
	
	@Override
	public boolean mouseClicked(int mouseX, int mouseY, int mouseButton)
	{
		if(messagebox != null)
			messagebox.eventMouse(gui, this, mouseX - gui.width/2, mouseY - gui.height/2, mouseButton);
		else
		{
			for(IGuiElement e: elements)
			{
				if(e.eventMouse(mouseX, mouseY, mouseButton))
					return true;
			}
			
			project.mouseEvent(gui.width/2, gui.height/2, mouseButton, mouseX, mouseY);
		}
		
		return false;
	}
	
	@Override
	public void mouseWheelEvent(int delta_wheel)
	{
		project.mouseWheelEvent(delta_wheel);
	}
	
	@Override
	public boolean keyTyped(char typedChar, int keyCode)
	{
		if(keyCode == 1)
		{
			if(project.changment && config_editmodeAccess())
			{
				messageBox_set(new GMsgSaveExit(_this));
				return true;
			}
			else gui.mc.player.closeScreen();
		}
		
		if(messagebox != null)
			messagebox.eventKeyboard(gui, this, typedChar, keyCode);
		else
		{
			for(IGuiElement e: elements)
			{
				if(e.eventKeyboard(typedChar, keyCode))
					return true;
			}
		}
		
		return false;
	}
	
	// ---------- Config ----------
	
	public boolean config_editmodeAccess()
	{
		return true;
	}
	
	// ---------- Message Box ----------
	
	public void messageBox_close()
	{
		messagebox = null;
	}
	
	public void messageBox_set(IGuiMessage msg)
	{
		messagebox = msg;
	}

	public void saveChanges()
	{
		project.sendToServer();
		project.changment = false;
	}

}
