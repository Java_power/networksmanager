package javapower.netman.gui.terminal;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.util.GEButtonBar;
import javapower.netman.gui.util.GuiPanel;
import javapower.netman.gui.util.IGuiElement;
import javapower.netman.util.EScreenAnchor;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorDynamic2;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;

public class GuiPanelParameter extends GuiPanel
{
	GuiTerminal gui;
	
	List<IGuiElement> elements = new ArrayList<IGuiElement>();
	
	GEButtonBar exit, back;
	
	@Override
	public void initPanel(GuiScreen _gui)
	{
		gui = (GuiTerminal) _gui;
		
		exit = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(0, 0)), new IEventVoid()
		{
			@Override
			public void event()
			{
				gui.mc.player.closeScreen();
			}
			
		}, 0);
		
		back = new GEButtonBar(new VectorDynamic2(gui.thisScreen, EScreenAnchor.TOP_LEFT, new Vector2(42, 0)), new IEventVoid()
		{
			@Override
			public void event()
			{
				gui.SetGuiPanel(new GuiPanelMainMenu());
			}
			
		}, 1);
		
		elements.clear();
		
		elements.add(exit);
		elements.add(back);
	}
	
	@Override
	public void draw()
	{
		//Gui.drawRect(0, 0, gui.width, gui.height, 0xff212121);
		Gui.drawRect(0, 0, gui.width, 20, 0xff0065ad);
		GL11.glColor4f(1, 1, 1, 1);
		
		for(IGuiElement e: elements)
		{
			e.draw(gui.mc, gui, 0, 0);
		}
	}

	@Override
	public void update()
	{
		for(IGuiElement e: elements)
		{
			e.update();
		}
	}
	
	public void onResize(net.minecraft.client.Minecraft mcIn, int w, int h)
	{
		for(IGuiElement e: elements)
		{
			e.resize(w, h);
		}
	}
	
	@Override
	public boolean mouseClicked(int mouseX, int mouseY, int mouseButton)
	{
		for(IGuiElement e: elements)
		{
			if(e.eventMouse(mouseX, mouseY, mouseButton))
				return true;
		}
		return false;
	}
	
	@Override
	public boolean keyTyped(char typedChar, int keyCode)
	{
		for(IGuiElement e: elements)
		{
			if(e.eventKeyboard(typedChar, keyCode))
				return true;
		}
		return false;
	}
}