package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineFluidFlowMeter;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_FluidFlowMeter;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import net.minecraft.nbt.NBTTagCompound;

public class ElementFluidFlowMeter extends Element
{
	@Override
	public String getName()
	{
		return "F Flow Meter";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{80,80,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 80, 80, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidFlowMeter && ((DElementMapFluidFlowMeter)map[x][y]).machine_flowmeter != null)
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ((DElementMapFluidFlowMeter)map[x][y]).machine_flowmeter.fluxValue+" mB/Tick", x_draw+20, y_draw+20, 0xffff00);
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidFlowMeter)
		{
			DElementMapFluidFlowMeter eFlowMeter =  ((DElementMapFluidFlowMeter)map[x][y]);
			
			if(eFlowMeter.machine_flowmeter != null && eFlowMeter.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, eFlowMeter.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "fluid flow meter info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+eFlowMeter.machine_flowmeter.customName, x_draw + 20, y_draw + 42, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "buffer size : "+ eFlowMeter.machine_flowmeter.fluxBufferSize, x_draw + 20, y_draw + 52, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "flow : "+ eFlowMeter.machine_flowmeter.fluxValue + " mB/Tick", x_draw + 20, y_draw + 62, 0xffffff);
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.FLUID;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapFluidFlowMeter();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidFlowMeter)
		{
			if(project.guipanel.editmode)
			{
				project.guipanel.messageBox_set(new GMsgEMachineFluidFlowMeter(project.guipanel.gui, (DElementMapFluidFlowMeter) map[x][y]));
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidFlowMeter)
		{
			((DElementMapFluidFlowMeter)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapFluidFlowMeter)map[x][y]).posdim);
			((DElementMapFluidFlowMeter)map[x][y]).machine_flowmeter = machine_ instanceof MachineCL_FluidFlowMeter ? (MachineCL_FluidFlowMeter) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidFlowMeter)
		{
			if(((DElementMapFluidFlowMeter)map[x][y]).posdim != null)
				((DElementMapFluidFlowMeter)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
