package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineFluidValve;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_FluidValve;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import net.minecraft.nbt.NBTTagCompound;

public class ElementFluidValve1 extends Element
{
	@Override
	public String getName()
	{
		return "Valve";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{20,100,20,20/*, 0x3daeff*/};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmodi)
	{
		if(map[x][y] instanceof DElementMapFluidValve)
		{
			if(((DElementMapFluidValve)map[x][y]).machine_fluidValve != null)
			{
				//GlStateManager.color(0.2392f, 0.6824f, 1.0f);
				gui.drawTexturedModalRect(x_draw, y_draw, 20, ((DElementMapFluidValve)map[x][y]).machine_fluidValve.valve_pos ? 80 : 100, 20, 20);
			}
			else
			{
				//GlStateManager.color(0.2392f, 0.6824f, 1.0f);
				gui.drawTexturedModalRect(x_draw, y_draw, 20, 120, 20, 20);
			}
		}
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidValve && ((DElementMapFluidValve)map[x][y]).machine_fluidValve != null)
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ((DElementMapFluidValve)map[x][y]).machine_fluidValve.valve_pos ? "ON" : "OFF", x_draw+20, y_draw+20, 0xffff00);
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidValve)
		{
			DElementMapFluidValve eswitch =  ((DElementMapFluidValve)map[x][y]);
			
			if(eswitch.machine_fluidValve != null && eswitch.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, eswitch.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "switch info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+eswitch.machine_fluidValve.customName, x_draw + 20, y_draw + 42, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "valve pos : "+ (eswitch.machine_fluidValve.valve_pos ? "Open" : "Closed"), x_draw + 20, y_draw + 52, 0xffffff);
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return (side_from == Direction2D.DOWN || side_from == Direction2D.TOP) && from_type == ElementType.FLUID;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapFluidValve();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidValve)
		{
			MachineCL_FluidValve mvalve = ((DElementMapFluidValve)map[x][y]).machine_fluidValve;
			if(!project.guipanel.editmode)
			{
				if(mvalve != null)
					mvalve.valveOnOff(!mvalve.valve_pos, project.guipanel.gui);
			}
			else
			{
				project.guipanel.messageBox_set(new GMsgEMachineFluidValve(project.guipanel.gui, (DElementMapFluidValve) map[x][y]));
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidValve)
		{
			((DElementMapFluidValve)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapFluidValve)map[x][y]).posdim);
			((DElementMapFluidValve)map[x][y]).machine_fluidValve = machine_ instanceof MachineCL_FluidValve ? (MachineCL_FluidValve) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidValve)
		{
			if(((DElementMapFluidValve)map[x][y]).posdim != null)
				((DElementMapFluidValve)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
