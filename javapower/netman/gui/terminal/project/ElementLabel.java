package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgELabel;
import net.minecraft.nbt.NBTTagCompound;

public class ElementLabel extends Element
{
	@Override
	public String getName()
	{
		return "Label";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{160,0,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		if(editmod)
			gui.drawTexturedModalRect(x_draw, y_draw, 160, 0, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapLabel && ((DElementMapLabel)map[x][y]).label != null)
		{
			project.guipanel.gui.drawCenteredString(project.guipanel.gui.mc.fontRenderer, ((DElementMapLabel)map[x][y]).label, x_draw + 10, y_draw + 7, 0xffffff);
		}
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapLabel();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapLabel)
		{
			if(project.guipanel.editmode && button == 1)
			{
				project.guipanel.messageBox_set(new GMsgELabel((DElementMapLabel) map[x][y]));
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapLabel)
		{
			((DElementMapLabel)map[x][y]).label = nbt.getString("lab");
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapLabel && ((DElementMapLabel)map[x][y]).label != null)
		{
			nbt.setString("lab", ((DElementMapLabel)map[x][y]).label);
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
