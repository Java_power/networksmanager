package javapower.netman.gui.terminal.project;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.terminal.msg.GMsgRawString;
import net.minecraft.nbt.NBTTagCompound;

public class ElementReturn extends Element
{
	@Override
	public String getName()
	{
		return "Return";
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(project.guipanel.editmode && map[x][y] instanceof DElementMapReturn)
		{
			if(((DElementMapReturn)map[x][y]).target != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "link", x_draw + 4, y_draw + 6, 0xffd800);
			}
		}
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapReturn)
		{
			if(project.guipanel.editmode)
			{
				if(button == 1)
				{
					if(((DElementMapReturn)map[x][y]).target == null)
					{
						if(project.tempTargetElement1 == null)
						{
							IEventVoid eventCancel = new IEventVoid()
							{
								
								@Override
								public void event()
								{
									project.tempTargetElement1 = null;
								}
							};
							
							IEventVoid eventOk = new IEventVoid()
							{
								
								@Override
								public void event()
								{
									project.tempTargetElement1 = new ProjectTarget(x, y, project.current_page.name);
								}
							};
							
							project.guipanel.messageBox_set(new GMsgRawString(eventCancel, eventOk, "Return Linking", new String[]{"Ok : to link element", "Please right click on the second item to link it to this one"}));
						}
						else
						{
							if(project.tempTargetElement1.x_map == x && project.tempTargetElement1.y_map == y && project.tempTargetElement1.name_map == project.current_page.name)
							{
								project.tempTargetElement1 = null;
							}
							else
							{
								((DElementMapReturn)map[x][y]).target = project.tempTargetElement1.copy();
								
								DElementMap dataElement = project.tempTargetElement1.getDataElement(project);
								if(dataElement != null && dataElement instanceof DElementMapReturn)
								{
									((DElementMapReturn)dataElement).target = new ProjectTarget(x, y, project.current_page.name);
								}
								
								project.tempTargetElement1 = null;
								project.guipanel.messageBox_set(new GMsgRawString("Return Linking", new String[]{"Successful linking", "info:", "A:[X:"+x+",Y:"+y+",PageName:"+project.current_page.name+"]", "B:"+((DElementMapReturn)map[x][y]).target.toString()}));
								project.changment = true;
							}
						}
					}
					else
					{
						IEventVoid eventCancel = new IEventVoid()
						{
							
							@Override
							public void event()
							{
								
							}
						};
						
						IEventVoid eventOk = new IEventVoid()
						{
							
							@Override
							public void event()
							{
								project.tempTargetElement1 = null;
								DElementMap dataElement = ((DElementMapReturn)map[x][y]).target.getDataElement(project);
								if(dataElement != null && dataElement instanceof DElementMapReturn)
								{
									((DElementMapReturn)dataElement).target = null;
								}
								((DElementMapReturn)map[x][y]).target = null;
							}
						};
						
						project.guipanel.messageBox_set(new GMsgRawString(eventCancel, eventOk, "Return Linking", new String[]{"Do you really want to delete the link ?"}));
					}
				}
			}
			else
			{
				if(button == 0 && ((DElementMapReturn)map[x][y]).target != null)
				{
					((DElementMapReturn)map[x][y]).target.goTo(project, true);
				}
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapReturn)
		{
			if(nbt.hasKey("te") && nbt.getBoolean("te"))
			{
				if(nbt.hasKey("tx") && nbt.hasKey("ty") && nbt.hasKey("tn"))
					((DElementMapReturn)map[x][y]).target = new ProjectTarget(nbt.getInteger("tx"), nbt.getInteger("ty"), nbt.getString("tn"));
			}
			else
			{
				((DElementMapReturn)map[x][y]).target = null;
			}
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapReturn)
		{
			if(((DElementMapReturn)map[x][y]).target == null)
			{
				nbt.setBoolean("te", false);
			}
			else
			{
				nbt.setBoolean("te", true);
				nbt.setInteger("tx", ((DElementMapReturn)map[x][y]).target.x_map);
				nbt.setInteger("ty", ((DElementMapReturn)map[x][y]).target.y_map);
				nbt.setString("tn", ((DElementMapReturn)map[x][y]).target.name_map);
			}
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapReturn();
	}
}
