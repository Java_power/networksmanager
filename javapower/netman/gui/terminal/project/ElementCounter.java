package javapower.netman.gui.terminal.project;

import org.lwjgl.input.Keyboard;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineCounter;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_Counter;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import javapower.netman.util.Tools;
import net.minecraft.nbt.NBTTagCompound;

public class ElementCounter extends Element
{
	@Override
	public String getName()
	{
		return "Counter";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{100,0,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 100, 0, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapCounter && ((DElementMapCounter)map[x][y]).machine_counter != null)
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, Tools.longFormatToString(((DElementMapCounter)map[x][y]).machine_counter.consomation)+" RF.Tick", x_draw+20, y_draw+20, 0xffff00);
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapCounter)
		{
			DElementMapCounter ecounter =  ((DElementMapCounter)map[x][y]);
			
			if(ecounter.machine_counter != null && ecounter.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ecounter.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "counter info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+ecounter.machine_counter.customName, x_draw + 20, y_draw + 42, 0xffffff);
				//project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "auto extract : "+ (ecounter.machine_counter.autoextract ? "Yes" : "No"), x_draw + 20, y_draw + 52, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "consomation : "+ Tools.longFormatToString(ecounter.machine_counter.consomation) + " RF.Tick", x_draw + 20, y_draw + 52, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "to reset press CTRL + LEFT CLICK", x_draw + 10, y_draw + 62, 0xffff00);
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.ENERGY;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapCounter();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapCounter)
		{
			if(project.guipanel.editmode)
			{
				project.guipanel.messageBox_set(new GMsgEMachineCounter(project.guipanel.gui, (DElementMapCounter) map[x][y]));
			}
			else
			{
				if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL))
				{
					MachineCL_Counter mcounter = ((DElementMapCounter) map[x][y]).machine_counter;
					if(mcounter != null)
						mcounter.zero(project.guipanel.gui);
				}
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapCounter)
		{
			((DElementMapCounter)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapCounter)map[x][y]).posdim);
			((DElementMapCounter)map[x][y]).machine_counter = machine_ instanceof MachineCL_Counter ? (MachineCL_Counter) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapCounter)
		{
			if(((DElementMapCounter)map[x][y]).posdim != null)
				((DElementMapCounter)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
