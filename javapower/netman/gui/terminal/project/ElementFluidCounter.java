package javapower.netman.gui.terminal.project;

import org.lwjgl.input.Keyboard;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineFluidCounter;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_FluidCounter;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import javapower.netman.util.Tools;
import net.minecraft.nbt.NBTTagCompound;

public class ElementFluidCounter extends Element
{
	@Override
	public String getName()
	{
		return "F Counter";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{60,80,20,20/*, 0x3daeff*/};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 60, 80, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidCounter && ((DElementMapFluidCounter)map[x][y]).machine_counter != null)
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, Tools.longFormatToString(((DElementMapFluidCounter)map[x][y]).machine_counter.consomation)+" mB.Tick", x_draw+20, y_draw+20, 0xffff00);
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidCounter)
		{
			DElementMapFluidCounter ecounter =  ((DElementMapFluidCounter)map[x][y]);
			
			if(ecounter.machine_counter != null && ecounter.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ecounter.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "counter info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+ecounter.machine_counter.customName, x_draw + 20, y_draw + 42, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "consomation : "+ Tools.longFormatToString(ecounter.machine_counter.consomation) + " mB.Tick", x_draw + 20, y_draw + 52, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "to reset press CTRL + LEFT CLICK", x_draw + 10, y_draw + 62, 0xffff00);
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.FLUID;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapFluidCounter();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidCounter)
		{
			if(project.guipanel.editmode)
			{
				project.guipanel.messageBox_set(new GMsgEMachineFluidCounter(project.guipanel.gui, (DElementMapFluidCounter) map[x][y]));
			}
			else
			{
				if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL))
				{
					MachineCL_FluidCounter mcounter = ((DElementMapFluidCounter) map[x][y]).machine_counter;
					if(mcounter != null)
						mcounter.zero(project.guipanel.gui);
				}
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidCounter)
		{
			((DElementMapFluidCounter)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapFluidCounter)map[x][y]).posdim);
			((DElementMapFluidCounter)map[x][y]).machine_counter = machine_ instanceof MachineCL_FluidCounter ? (MachineCL_FluidCounter) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidCounter)
		{
			if(((DElementMapFluidCounter)map[x][y]).posdim != null)
				((DElementMapFluidCounter)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
