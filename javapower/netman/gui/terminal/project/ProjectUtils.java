package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.util.Direction2D;

public class ProjectUtils
{
	public static DElementMap getElementData(int x, int y, Direction2D dir, DElementMap[][] map)
	{
		if(elementIsOnMap(x + dir.getDX(), y + dir.getDY(), map))
		{
			return map[x + dir.getDX()][y + dir.getDY()]; 
		}
		return null;
	}
	
	public static boolean elementIsOnMap(int x, int y, DElementMap[][] map)
	{
		return x >= 0 && y >= 0 && x < map.length && y < map[0].length;
	}
	
	public static Element getElement(int id)
	{
		return RegisterElements.elements[id];
	}
	
	public static Element getElement(DElementMap elementdata)
	{
		if(elementdata == null)
			return null;
		return RegisterElements.elements[elementdata.id];
	}
	
	public static boolean mapIsEmpty(DElementMap[][] map)
	{
		for(DElementMap[] a : map)
			for(DElementMap b : a)
				if(b.id != 0)
					return false;
		return true;
	}
	
	public static void updateCross(int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		DElementMap m = map[x][y];
		if(m != null)
			getElement(m).updateFlag(x, y, map, gui);
		
		DElementMap l = getElementData(x, y, Direction2D.LEFT, map);
		if(l != null)
			getElement(l).updateFlag(x + Direction2D.LEFT.getDX(), y + Direction2D.LEFT.getDY(), map, gui);
		
		DElementMap t = getElementData(x, y, Direction2D.TOP, map);
		if(t != null)
			getElement(t).updateFlag(x + Direction2D.TOP.getDX(), y + Direction2D.TOP.getDY(), map, gui);
		
		DElementMap r = getElementData(x, y, Direction2D.RIGHT, map);
		if(r != null)
			getElement(r).updateFlag(x + Direction2D.RIGHT.getDX(), y + Direction2D.RIGHT.getDY(), map, gui);
		
		DElementMap d = getElementData(x, y, Direction2D.DOWN, map);
		if(d != null)
			getElement(d).updateFlag(x + Direction2D.DOWN.getDX(), y + Direction2D.DOWN.getDY(), map, gui);
	}
	
	public static void updateFullFlag(GuiTerminal gui, Page page)
	{
		for(int y = 0; y < page.height; ++y)
			for(int x = 0; x < page.width; ++x)
			{
				getElement(page.map[x][y]).updateFlag(x, y, page.map, gui);
			}
	}
	
	public static DElementMap createDataBuilder(int id)
	{
		Element e = getElement(id);
		
		if(e != null)
			return e.getDataBuilder();
		
		return new DElementMap();
	}
}
