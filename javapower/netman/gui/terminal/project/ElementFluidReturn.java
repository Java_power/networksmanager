package javapower.netman.gui.terminal.project;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.terminal.msg.GMsgRawString;
import net.minecraft.nbt.NBTTagCompound;

public class ElementFluidReturn extends Element
{
	@Override
	public String getName()
	{
		return "Fluid Return";
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(project.guipanel.editmode && map[x][y] instanceof DElementMapFluidReturn)
		{
			if(((DElementMapFluidReturn)map[x][y]).target != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "link", x_draw + 4, y_draw + 6, 0xffd800);
			}
		}
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidReturn)
		{
			if(project.guipanel.editmode)
			{
				if(button == 1)
				{
					if(((DElementMapFluidReturn)map[x][y]).target == null)
					{
						if(project.tempTargetElement2 == null)
						{
							IEventVoid eventCancel = new IEventVoid()
							{
								
								@Override
								public void event()
								{
									project.tempTargetElement2 = null;
								}
							};
							
							IEventVoid eventOk = new IEventVoid()
							{
								
								@Override
								public void event()
								{
									project.tempTargetElement2 = new ProjectTargetFluid(x, y, project.current_page.name);
								}
							};
							
							project.guipanel.messageBox_set(new GMsgRawString(eventCancel, eventOk, "Return Linking", new String[]{"Ok : to link element", "Please right click on the second item to link it to this one"}));
						}
						else
						{
							if(project.tempTargetElement2.x_map == x && project.tempTargetElement2.y_map == y && project.tempTargetElement2.name_map == project.current_page.name)
							{
								project.tempTargetElement2 = null;
							}
							else
							{
								((DElementMapFluidReturn)map[x][y]).target = project.tempTargetElement2.copy();
								
								DElementMap dataElement = project.tempTargetElement2.getDataElement(project);
								if(dataElement != null && dataElement instanceof DElementMapFluidReturn)
								{
									((DElementMapFluidReturn)dataElement).target = new ProjectTargetFluid(x, y, project.current_page.name);
								}
								
								project.tempTargetElement2 = null;
								project.guipanel.messageBox_set(new GMsgRawString("Return Linking", new String[]{"Successful linking", "info:", "A:[X:"+x+",Y:"+y+",PageName:"+project.current_page.name+"]", "B:"+((DElementMapFluidReturn)map[x][y]).target.toString()}));
								project.changment = true;
							}
						}
					}
					else
					{
						IEventVoid eventCancel = new IEventVoid()
						{
							
							@Override
							public void event()
							{
								
							}
						};
						
						IEventVoid eventOk = new IEventVoid()
						{
							
							@Override
							public void event()
							{
								project.tempTargetElement2 = null;
								DElementMap dataElement = ((DElementMapFluidReturn)map[x][y]).target.getDataElement(project);
								if(dataElement != null && dataElement instanceof DElementMapFluidReturn)
								{
									((DElementMapFluidReturn)dataElement).target = null;
								}
								((DElementMapFluidReturn)map[x][y]).target = null;
							}
						};
						
						project.guipanel.messageBox_set(new GMsgRawString(eventCancel, eventOk, "Return Linking", new String[]{"Do you really want to delete the link ?"}));
					}
				}
			}
			else
			{
				if(button == 0 && ((DElementMapFluidReturn)map[x][y]).target != null)
				{
					((DElementMapFluidReturn)map[x][y]).target.goTo(project, true);
				}
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidReturn)
		{
			if(nbt.hasKey("te") && nbt.getBoolean("te"))
			{
				if(nbt.hasKey("tx") && nbt.hasKey("ty") && nbt.hasKey("tn"))
					((DElementMapFluidReturn)map[x][y]).target = new ProjectTargetFluid(nbt.getInteger("tx"), nbt.getInteger("ty"), nbt.getString("tn"));
			}
			else
			{
				((DElementMapFluidReturn)map[x][y]).target = null;
			}
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidReturn)
		{
			if(((DElementMapFluidReturn)map[x][y]).target == null)
			{
				nbt.setBoolean("te", false);
			}
			else
			{
				nbt.setBoolean("te", true);
				nbt.setInteger("tx", ((DElementMapFluidReturn)map[x][y]).target.x_map);
				nbt.setInteger("ty", ((DElementMapFluidReturn)map[x][y]).target.y_map);
				nbt.setString("tn", ((DElementMapFluidReturn)map[x][y]).target.name_map);
			}
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapFluidReturn();
	}
}
