package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;

public class ElementSwitch2 extends ElementSwitch1
{
	@Override
	public int[] getIcon()
	{
		return new int[]{0,40,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmodi)
	{
		if(map[x][y] instanceof DElementMapSwitch)
		{
			if(((DElementMapSwitch)map[x][y]).machine_switch != null)
			{
				gui.drawTexturedModalRect(x_draw, y_draw, 0, ((DElementMapSwitch)map[x][y]).machine_switch.switch_pos ? 20 : 40, 20, 20);
			}
			else
			{
				gui.drawTexturedModalRect(x_draw, y_draw, 0, 60, 20, 20);
			}
		}
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return (side_from == Direction2D.LEFT || side_from == Direction2D.RIGHT) && from_type == ElementType.ENERGY;
	}
}
