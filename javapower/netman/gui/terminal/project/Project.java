package javapower.netman.gui.terminal.project;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;

import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.terminal.GuiPanelMyInstallation;
import javapower.netman.gui.util.GMEButton;
import javapower.netman.message.NetworkTerminalProject;
import javapower.netman.proxy.ClientProxy;
import javapower.netman.proxy.CommonProxy;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.Vector2;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.nbt.NBTTagCompound;

public class Project
{
	public List<Page> pages;
	public Page current_page;
	public int current_page_id;
	public Vector2 map_pos;
	public Vector2 element_pos = new Vector2();
	public boolean in_move_map;
	public Vector2 mouse_start_pos = new Vector2();
	public Vector2 mouse_delta_pos = new Vector2();
	public int default_page = 0;
	public boolean changment = false;
	
	public ProjectTarget tempTargetElement1 = null;
	public ProjectTargetFluid tempTargetElement2 = null;
	
	GuiPanelMyInstallation guipanel;
	public int current_element_id;
	
	public Project(GuiPanelMyInstallation _guipanel)
	{
		guipanel = _guipanel;
		pages = new ArrayList<Page>();
		map_pos = new Vector2();
	}
	
	public void draw(int x, int y)
	{
		if(current_page != null)
		{
			current_page.draw(x, y, this);
			GlStateManager.translate(0, 0, 110);
			
			if(ProjectUtils.elementIsOnMap(current_page.element_selected.x, current_page.element_selected.y, current_page.map))
			{
				Element e = ProjectUtils.getElement(current_page.currentElement());
				if(e != null && e.hasInfoPrompt())
				{
					int x_draw = guipanel.gui.thisScreen.mouse.x >= guipanel.gui.width/2 ? 0 : guipanel.gui.width-200;
					
					guipanel.gui.drawRect(x_draw, 20, x_draw+200, 200, 0xdd0065ad);
					e.drawInfo(x_draw, 20, current_page.element_selected.x, current_page.element_selected.y, current_page.map, this);
				}
			}
		}
		
		guipanel.gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_items);
		GlStateManager.clearColor(1, 1, 1, 1);
		
		if(guipanel.editmode)
		{
			int[] icon = ProjectUtils.getElement(current_element_id).getIcon();
			Gui.drawRect(guipanel.gui.width - 50, 30, guipanel.gui.width - 10, 80, 0xdd0065ad);
			if(icon.length >= 5)
			{
				GlStateManager.color((float)(icon[4] >> 16 & 255) / 255.0F, (float)(icon[4] >> 8 & 255) / 255.0F, (float)(icon[4] & 255) / 255.0F);
			}
			else
				GlStateManager.color(1, 1, 1);
			guipanel.gui.drawTexturedModalRect(guipanel.gui.width - 40, 40, icon[0], icon[1], icon[2], icon[3]);
			guipanel.gui.drawCenteredString(guipanel.gui.mc.fontRenderer, ProjectUtils.getElement(current_element_id).getName(), guipanel.gui.width - 30, 68, 0xffffff);
		
			if(tempTargetElement1 != null)
			{
				guipanel.gui.drawString(guipanel.gui.mc.fontRenderer, "link", guipanel.gui.thisScreen.mouse.x, guipanel.gui.thisScreen.mouse.y, 0xffd800);
			}
		}
	}
	
	public void update()
	{
		if(current_page != null)
		{
			current_page.update(guipanel.gui.width/2, guipanel.gui.height/2, guipanel.gui.thisScreen.mouse.x, guipanel.gui.thisScreen.mouse.y, this);
			
			if(Mouse.isButtonDown(1))
			{
				if(!in_move_map)
				{
					in_move_map = true;
					mouse_start_pos = guipanel.gui.thisScreen.mouse.copy();
					mouse_delta_pos = mouse_start_pos.copy();
				}
				if(!mouse_delta_pos.equals(guipanel.gui.thisScreen.mouse))
				{
					//map_pos.x += Mouse.getDX()*1.8f;
					//map_pos.y -= Mouse.getDY()*1.8f;
					map_pos.x += guipanel.gui.thisScreen.mouse.x - mouse_delta_pos.x;
					map_pos.y += guipanel.gui.thisScreen.mouse.y - mouse_delta_pos.y;
					
					mouse_delta_pos = guipanel.gui.thisScreen.mouse.copy();
				}
				if(map_pos.x > 0)
					map_pos.x = 0;
				
				if(map_pos.y > 0)
					map_pos.y = 0;
				
				if(map_pos.x + (20*current_page.width) < 0)
					map_pos.x = -20*current_page.width;
				
				if(map_pos.y + (20*current_page.height) < 0)
					map_pos.y = -20*current_page.height;
			}
			else
			{
				in_move_map = false;
			}
		}
		else
		{
			in_move_map = false;
		}
		
		if(current_page != null)
		{
			element_pos.x = (guipanel.gui.thisScreen.mouse.x - map_pos.x - (guipanel.gui.width/2))/20;
			element_pos.y = (guipanel.gui.thisScreen.mouse.y - map_pos.y - (guipanel.gui.height/2))/20;
		}
	}
	
	public void mouseEvent(int x, int y, int mouseButton, int xMouse, int yMouse)
	{
		if(current_page != null)
		{
			current_page.mouseEvent(x, y, mouseButton, xMouse, yMouse, this);
		}
	}
	
	public void mouseWheelEvent(int delta_wheel)
	{
		if(guipanel.editmode)
		{
			if(delta_wheel < 0)
			{
				if(current_element_id <= 0)
					current_element_id = RegisterElements.elements.length - 1;
				else if(current_element_id > 0)
					--current_element_id;
			}
			else
			{
				if(current_element_id < RegisterElements.elements.length - 1)
					++current_element_id;
				else current_element_id = 0;
			}
		}
	}
	
	public void updateMenuPages()
	{
		guipanel.menu_page.elements.clear();
		
		int idp = 0;
		for(Page p : pages)
		{
			final int idp_f = idp;
			guipanel.menu_page.elements.add(new GMEButton(3, new IEventVoid()
			{
				@Override
				public void event()
				{
					setPage(idp_f);
				}
				
			}, p.name));
			
			++idp;
		}
	}
	
	public void addPage(Page page)
	{
		changment = true;
		pages.add(page);
	}
	
	public void setPage(int id)
	{
		current_page_id = id;
		if(id >= 0 && id < pages.size())
		{
			current_page = pages.get(id);
			if(current_page != null)
			{
				map_pos.x = -(current_page.width/2)*20;
				map_pos.y = -(current_page.height/2)*20;
				
				if(ProjectUtils.mapIsEmpty(current_page.map))
				{
					guipanel.editmode = guipanel.config_editmodeAccess();
				}
			}
		}
		else
		{
			current_page = null;
		}
	}
	
	public NBTTagCompound getPacketInfo()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setInteger("s", pages.size());
		int idp = 0;
		for(Page p : pages)
		{
			NBTTagCompound nbtp = new NBTTagCompound();
			
			nbtp.setInteger("w", p.width);
			nbtp.setInteger("h", p.height);
			nbtp.setString("n", p.name);
			
			nbt.setTag("p"+idp, nbtp);
			++idp;
		}
		nbt.setInteger("d", default_page);
		
		nbt.setByte("tag", (byte) 1);
		return nbt;
	}
	
	public NBTTagCompound getPacketMap(int idp)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		Page page = pages.get(idp);
		
		for(int y = 0; y < page.height; ++y)
			for(int x = 0; x < page.width; ++x)
			{
				int loc_element = x + y*page.width;
				int element_id = page.map[x][y].id;
				
				if(element_id != 0)
				{
					NBTTagCompound nbt_element = new NBTTagCompound();
					nbt_element.setInteger("i", element_id);
					
					Element e = ProjectUtils.getElement(element_id);
					if(e.hasCustomData())
					{
						NBTTagCompound nbt_element_data = new NBTTagCompound();
						
						e.writeDataToNBT(nbt_element_data, x, y, page.map, this);
						nbt_element.setTag("d", nbt_element_data);
					}
					
					nbt.setTag("l"+loc_element, nbt_element);
				}
			}
		return nbt;
	}

	public void sendToServer()
	{
		ClientProxy.project_temp = this;
		CommonProxy.network_TerminalProject.sendToServer(new NetworkTerminalProject(guipanel.gui.te_terminal.thispos(), getPacketInfo()));
	}
	
	public void loadFromServer()
	{
		ClientProxy.project_temp = this;
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setByte("tag", (byte) 3);
		CommonProxy.network_TerminalProject.sendToServer(new NetworkTerminalProject(guipanel.gui.te_terminal.thispos(), nbt));
	}

	public void setPacketInfo(NBTTagCompound nbt)
	{
		int max_p = nbt.getInteger("s");
		default_page = nbt.getInteger("d");
		pages.clear();
		for(int id = 0; id < max_p; ++id)
		{
			NBTTagCompound nbtp = nbt.getCompoundTag("p"+id);
			pages.add(new Page(nbtp.getString("n"), nbtp.getInteger("w"), nbtp.getInteger("h")));
		}
	}
	
	public void setPacketPage(int page_id, NBTTagCompound nbt)
	{
		Page page = pages.get(page_id);
		page.initClear();
		for(int y = 0; y < page.height; ++y)
			for(int x = 0; x < page.width; ++x)
			{
				int loc_element = x + y*page.width;
				if(nbt.hasKey("l"+loc_element))
				{
					NBTTagCompound nbt_element = nbt.getCompoundTag("l"+loc_element);
					
					//System.out.println(x+"-"+y);
					page.map[x][y] = ProjectUtils.createDataBuilder(nbt_element.getInteger("i"));
					page.map[x][y].id = nbt_element.getInteger("i");
					
					Element e = ProjectUtils.getElement(page.map[x][y].id);
					if(e.hasCustomData())
					{
						NBTTagCompound nbt_element_data = nbt_element.getCompoundTag("d");
						e.readDataFromNBT(nbt_element_data, x, y, page.map, this);
					}
				}
			}
		ProjectUtils.updateFullFlag(guipanel.gui, page);
	}
	
	public void endLoadPackets()
	{
		updateMenuPages();
		setPage(default_page);
		changment = false;
	}
}
