package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineEnergyInfo;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_EnergyStorageInfo;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import net.minecraft.nbt.NBTTagCompound;

public class ElementEnergyInfo extends Element
{
	@Override
	public String getName()
	{
		return "Energy Info";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{80,0,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 80, 0, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapEnergyInfo && ((DElementMapEnergyInfo)map[x][y]).machine_energyInfo != null)
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ((DElementMapEnergyInfo)map[x][y]).machine_energyInfo.percentage(), x_draw+20, y_draw+20, 0xffff00);
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapEnergyInfo)
		{
			DElementMapEnergyInfo eenergyinfo =  ((DElementMapEnergyInfo)map[x][y]);
			
			if(eenergyinfo.machine_energyInfo != null && eenergyinfo.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, eenergyinfo.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "energy info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+eenergyinfo.machine_energyInfo.customName, x_draw + 20, y_draw + 42, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "stored energy : "+ eenergyinfo.machine_energyInfo.energy + " RF", x_draw + 20, y_draw + 52, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "energy max : "+ eenergyinfo.machine_energyInfo.energyMax + " RF", x_draw + 20, y_draw + 62, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "energy : " + eenergyinfo.machine_energyInfo.percentage(), x_draw + 20, y_draw + 72, 0xffffff);
				
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.ENERGY;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapEnergyInfo();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapEnergyInfo)
		{
			if(project.guipanel.editmode)
			{
				project.guipanel.messageBox_set(new GMsgEMachineEnergyInfo(project.guipanel.gui, (DElementMapEnergyInfo) map[x][y]));
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapEnergyInfo)
		{
			((DElementMapEnergyInfo)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapEnergyInfo)map[x][y]).posdim);
			((DElementMapEnergyInfo)map[x][y]).machine_energyInfo = machine_ instanceof MachineCL_EnergyStorageInfo ? (MachineCL_EnergyStorageInfo) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapEnergyInfo)
		{
			if(((DElementMapEnergyInfo)map[x][y]).posdim != null)
				((DElementMapEnergyInfo)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
