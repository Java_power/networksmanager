package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;

public class ElementTransformer2 extends Element
{
	@Override
	public String getName()
	{
		return "Transformer";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{40,0,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 40, 0, 20, 20);
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return (side_from == Direction2D.LEFT || side_from == Direction2D.RIGHT) && from_type == ElementType.ENERGY;
	}
}