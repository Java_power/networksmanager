package javapower.netman.gui.terminal.project;

public class ProjectTarget
{
	int x_map, y_map;
	String name_map;
	
	public ProjectTarget(int x, int y, String name)
	{
		x_map = x;
		y_map = y;
		name_map = name;
	}
	
	public int getTarget(Project project)
	{
		if(project.pages != null)
		{
			int id = 0;
			for(Page page : project.pages)
			{
				if(page.name.equals(name_map))
					return id;
				++id;
			}
		}
		
		return -1;
	}
	
	public DElementMap getDataElement(Project project)
	{
		int page_id = getTarget(project);
		if(page_id >= 0)
		{
			Page page = project.pages.get(page_id);
			if(ProjectUtils.elementIsOnMap(x_map, y_map, page.map))
			{
				return page.map[x_map][y_map];
			}
		}
		return null;
	}
	
	public void goTo(Project project, boolean centred)
	{
		DElementMap dataElement = getDataElement(project);
		if(dataElement != null)
		{
			project.setPage(getTarget(project));
			if(centred)
			{
				project.map_pos.x = project.current_page.width/2 - ((project.current_page.width/2 + x_map)*20)/2;
				project.map_pos.y = project.current_page.height/2 - ((project.current_page.height/2 + y_map)*20)/2;
			}
		}
	}
	
	public ProjectTarget copy()
	{
		return new ProjectTarget(x_map, y_map, name_map);
	}
	
	public String toString()
	{
		return "[X:"+x_map+",Y:"+y_map+",PageName:"+name_map+"]";
	}
}
