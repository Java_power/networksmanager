package javapower.netman.gui.terminal.project;

import javapower.netman.util.Vector2;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;

public class Page
{
	public String name;
	public int width, height;
	public Vector2 element_selected = new Vector2();
	public DElementMap[][] map;
	
	public Page(String _name, int _width, int _height)
	{
		name = _name;
		width = _width;
		height = _height;
		map = new DElementMap[width][height];
	}
	
	public void initClear()
	{
		for(int y = 0; y < height; ++y)
			for(int x = 0; x < width; ++x)
				map[x][y] = new DElementMap();
	}
	
	public void draw(int x, int y, Project project)
	{
		int ixs = ((x + project.map_pos.x)/20)*-1;
		int iys = ((y + project.map_pos.y)/20)*-1;
		
		if(ixs < 0) ixs = 0;
		if(iys < 0) iys = 0;
		
		int ixe = ((project.guipanel.gui.thisScreen.screensize.x / 20) + ixs) + 2;
		int iye = ((project.guipanel.gui.thisScreen.screensize.y / 20) + iys) + 1;
		
		if(ixe > map.length) ixe = map.length;
		if(iye > map[0].length) iye = map[0].length;
		
		for(int _y = iys; _y < iye; ++_y)
			for(int _x = ixs; _x < ixe; ++_x)
			{
				DElementMap data_element = map[_x][_y];
				if(project.element_pos.equals(_x, _y))
				{
					GlStateManager.color(1, 0, 0);
					
					if(data_element != null && (data_element.id != 0 || project.guipanel.editmode))
						ProjectUtils.getElement(data_element).draw(project.map_pos.x + x + _x*20, project.map_pos.y + y + _y*20, _x, _y, map, project.guipanel.gui, project.guipanel.editmode);
					
					GlStateManager.color(1, 1, 1);
				}
				else
				{
					if(data_element != null && (data_element.id != 0 || project.guipanel.editmode))
						ProjectUtils.getElement(data_element).draw(project.map_pos.x + x + _x*20, project.map_pos.y + y + _y*20, _x, _y, map, project.guipanel.gui, project.guipanel.editmode);
					
				}
			}
		
		RenderHelper.enableGUIStandardItemLighting();
		
		for(int _y = iys; _y < iye; ++_y)
			for(int _x = ixs; _x < ixe; ++_x)
			{
				DElementMap data_element = map[_x][_y];
				if(data_element != null)
					ProjectUtils.getElement(data_element).drawOverlay(project.map_pos.x + x + _x*20, project.map_pos.y + y + _y*20, _x, _y, map, project);
			}
		
		GlStateManager.enableAlpha();
		RenderHelper.disableStandardItemLighting();
	}
	
	public void update(int x, int y, int xMouse, int yMouse, Project project)
	{
		element_selected.x = (xMouse - project.map_pos.x - x)/20;
		element_selected.y = (yMouse - project.map_pos.y - y)/20;
	}
	
	public DElementMap currentElement()
	{
		return map[element_selected.x][element_selected.y];
	}
	
	public void mouseEvent(int x, int y, int mouseButton, int xMouse, int yMouse, Project project)
	{
		int x_map = (xMouse - project.map_pos.x - x)/20;
		int y_map = (yMouse - project.map_pos.y - y)/20;
		boolean used = false;
		
		if(project.guipanel.editmode)
		{	
			if(mouseButton == 0)
			{
				if(ProjectUtils.elementIsOnMap(x_map, y_map, map))
				{
					map[x_map][y_map] = ProjectUtils.createDataBuilder(project.current_element_id);
					map[x_map][y_map].id = project.current_element_id;
					ProjectUtils.updateCross(x_map, y_map, map, project.guipanel.gui);
					project.changment = true;
					used = true;
				}
			}
		}
		
		if(ProjectUtils.elementIsOnMap(x_map, y_map, map) && !used)
		{
			DElementMap data_element = map[x_map][y_map];
			if(data_element != null)
			{
				Element element = ProjectUtils.getElement(data_element);
					if(element != null)
					{
						used = element.mouseClick(x_map, y_map, ((xMouse - project.map_pos.x - x)/20f)-x_map, ((yMouse - project.map_pos.y - y)/20f)-y_map, mouseButton, map, project);
					}
			}
		}
	}
}
