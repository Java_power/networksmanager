package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachine;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ElementFluidMachine extends Element
{
	@Override
	public String getName()
	{
		return "F Machine";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{100,80,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 100, 80, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapMachine && ((DElementMapMachine)map[x][y]).itemstack != null)
		{
			project.guipanel.gui.mc.getRenderItem().renderItemIntoGUI(((DElementMapMachine) map[x][y]).itemstack, x_draw + 2, y_draw + 2);
			
			if(((DElementMapMachine)map[x][y]).name != null)
				project.guipanel.gui.drawCenteredString(project.guipanel.gui.mc.fontRenderer, ((DElementMapMachine)map[x][y]).name, x_draw + 10, ((DElementMapMachine)map[x][y]).pos_name == 0 ? y_draw - 8 : y_draw + 22, 0xffffff);
		}
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.FLUID;
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapMachine)
		{
			if(project.guipanel.editmode && button == 1)
			{
				project.guipanel.messageBox_set(new GMsgEMachine((DElementMapMachine) map[x][y]));
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapMachine)
		{
			int id = nbt.getInteger("id");
			if(id == 0)
			{
				((DElementMapMachine)map[x][y]).itemstack = ItemStack.EMPTY;
			}
			else
			{
				((DElementMapMachine)map[x][y]).itemstack = new ItemStack(Item.getItemById(id), 1, nbt.getInteger("met"));
			}
			
			((DElementMapMachine)map[x][y]).name = nbt.getString("name");
			((DElementMapMachine)map[x][y]).pos_name = nbt.getInteger("npos");
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapMachine && ((DElementMapMachine)map[x][y]).itemstack != null)
		{
			nbt.setInteger("id", Item.getIdFromItem(((DElementMapMachine)map[x][y]).itemstack.getItem()));
			nbt.setInteger("met", ((DElementMapMachine)map[x][y]).itemstack.getItemDamage());
			nbt.setString("name", ((DElementMapMachine)map[x][y]).name);
			nbt.setInteger("npos", ((DElementMapMachine)map[x][y]).pos_name);
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapMachine();
	}
}
