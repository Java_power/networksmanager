package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineFluidStorageInfo;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_FluidStorageInfo;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import javapower.netman.util.FluidTankInfoClient;
import net.minecraft.nbt.NBTTagCompound;

public class ElementFluidStorageInfo extends Element
{
	@Override
	public String getName()
	{
		return "Fluid Info";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{40,80,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 40, 80, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidStorageInfo && ((DElementMapFluidStorageInfo)map[x][y]).machine_storageinfo != null)
		{
			if(((DElementMapFluidStorageInfo)map[x][y]).machine_storageinfo.tanks != null && ((DElementMapFluidStorageInfo)map[x][y]).machine_storageinfo.tanks.size() > 0)
			{
				FluidTankInfoClient ftic = ((DElementMapFluidStorageInfo)map[x][y]).machine_storageinfo.tanks.get(0);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ftic._fluidname, x_draw+20, y_draw+20, 0xffff00);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ftic._amount+"/"+ftic._capacity, x_draw+20, y_draw+28, 0xffff00);
				
			}
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidStorageInfo)
		{
			DElementMapFluidStorageInfo eStorageInfo =  ((DElementMapFluidStorageInfo)map[x][y]);
			
			if(eStorageInfo.machine_storageinfo != null && eStorageInfo.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, eStorageInfo.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "fluid storage info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+eStorageInfo.machine_storageinfo.customName, x_draw + 20, y_draw + 42, 0xffffff);
				
				int y_dec = 0;
				for(FluidTankInfoClient ftic : ((DElementMapFluidStorageInfo)map[x][y]).machine_storageinfo.tanks)
				{
					project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "tank "+y_dec, x_draw + 20, y_draw + 52 + y_dec*30, 0xffffff);
					if(ftic != null)
					{
						project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "fluid : "+ftic._fluidname, x_draw + 30, y_draw + 62 + y_dec*30, 0xffffff);
						project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "amount : "+ ftic._amount +"/"+ftic._capacity+ " mB", x_draw + 30, y_draw + 72 + y_dec*30, 0xffffff);
					}
					else
					{
						project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "empty", x_draw + 30, y_draw + 62 + y_dec*30, 0xffffff);
					}
					++y_dec;
				}
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.FLUID;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapFluidStorageInfo();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidStorageInfo)
		{
			if(project.guipanel.editmode)
			{
				project.guipanel.messageBox_set(new GMsgEMachineFluidStorageInfo(project.guipanel.gui, (DElementMapFluidStorageInfo) map[x][y]));
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidStorageInfo)
		{
			((DElementMapFluidStorageInfo)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapFluidStorageInfo)map[x][y]).posdim);
			((DElementMapFluidStorageInfo)map[x][y]).machine_storageinfo = machine_ instanceof MachineCL_FluidStorageInfo ? (MachineCL_FluidStorageInfo) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluidStorageInfo)
		{
			if(((DElementMapFluidStorageInfo)map[x][y]).posdim != null)
				((DElementMapFluidStorageInfo)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
