package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;

public class ElementFluidReturnDown extends ElementFluidReturn
{
	@Override
	public int[] getIcon()
	{
		return new int[]{80,110,20,10};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw + 10, 80, 110, 20, 10);
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return side_from == Direction2D.DOWN && from_type == ElementType.FLUID;
	}
}
