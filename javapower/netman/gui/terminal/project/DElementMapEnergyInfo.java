package javapower.netman.gui.terminal.project;

import javapower.netman.nww.client.MachineCL_EnergyStorageInfo;
import javapower.netman.util.BlockPosDim;

public class DElementMapEnergyInfo extends DElementMap
{
	public MachineCL_EnergyStorageInfo machine_energyInfo;
	public BlockPosDim posdim;
}
