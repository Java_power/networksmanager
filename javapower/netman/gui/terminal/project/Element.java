package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import net.minecraft.nbt.NBTTagCompound;

public class Element
{
	public String getName()
	{
		return "stringNull";
	}
	
	/**
	 * @return return new int[]{0(sprite x), 0(sprite y), 20(w), 20(h)(, color)};
	 */
	public int[] getIcon()
	{
		return new int[]{0,0,20,20};
	}
	
	public boolean IsUsable()
	{
		return true;
	}
	
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 0, 0, 20, 20);
	}
	
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		
	}
	
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		
	}
	
	public boolean hasInfoPrompt()
	{
		return false;
	}
	
	public void updateFlag(int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		
	}
	
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		return false;
	}
	
	public void mouseDrag(int x, int y, int dx, int dy, DElementMap[][] map, Project project)
	{
		
	}
	
	public void init(int x, int y, DElementMap[][] map, Project project)
	{
		
	}
	
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return false;
	}
	
	public boolean hasCustomData()
	{
		return false;
	}
	
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		
	}
	
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		
	}
	
	public DElementMap getDataBuilder()
	{
		return new DElementMap();
	}
}