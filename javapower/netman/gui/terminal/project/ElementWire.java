package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;

public class ElementWire extends Element
{
	@Override
	public String getName()
	{
		return "Wire";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{60,0,20,20};
	}
	
	@Override
	public void updateFlag(int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		if(map[x][y] instanceof DElementMapFlag)
		{
			((DElementMapFlag)map[x][y]).flag =
				(canConnectToThis(Direction2D.LEFT, x, y, map, gui)? 1 : 0) |
				(canConnectToThis(Direction2D.TOP, x, y, map, gui)? 2 : 0) |
				(canConnectToThis(Direction2D.RIGHT, x, y, map, gui)? 4 : 0) |
				(canConnectToThis(Direction2D.DOWN, x, y, map, gui)? 8 : 0);
		}
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		int flag = map[x][y] instanceof DElementMapFlag ? ((DElementMapFlag)map[x][y]).flag : 0;
		
		int xs = (flag & 1) != 0 ? 0 : 9;
		int ys = (flag & 2) != 0 ? 0 : 9;
		int xe = (flag & 4) != 0 ? 0 : 9;
		int ye = (flag & 8) != 0 ? 0 : 9;
		
		gui.drawTexturedModalRect(x_draw + xs, y_draw + ys, 60 + xs, ys, 20 - (xs + xe), 20 - (ys + ye));
	}
	
	private boolean canConnectToThis(Direction2D side, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		DElementMap element_data = ProjectUtils.getElementData(x, y, side, map);
		if(element_data != null)
		{
			return ProjectUtils.getElement(element_data).canConnectTo(side.opposite(), ElementType.ENERGY, x + side.getDX(), y + side.getDY(), map, gui);
		}
		return false;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType form_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return form_type == ElementType.ENERGY;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapFlag();
	}
}
