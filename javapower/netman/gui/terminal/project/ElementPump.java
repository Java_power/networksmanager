package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;

public class ElementPump extends Element
{
	@Override
	public String getName()
	{
		return "Pump";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{60,100,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmodi)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 60, 100, 20, 20);
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.FLUID;
	}
}
