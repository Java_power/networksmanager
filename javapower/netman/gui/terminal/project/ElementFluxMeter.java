package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineFluxMeter;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_FluxMeter;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import net.minecraft.nbt.NBTTagCompound;

public class ElementFluxMeter extends Element
{
	@Override
	public String getName()
	{
		return "Flux Meter";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{120,0,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmod)
	{
		gui.drawTexturedModalRect(x_draw, y_draw, 120, 0, 20, 20);
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluxMeter && ((DElementMapFluxMeter)map[x][y]).machine_fluxMeter != null)
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ((DElementMapFluxMeter)map[x][y]).machine_fluxMeter.fluxValue+" RF/Tick", x_draw+20, y_draw+20, 0xffff00);
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluxMeter)
		{
			DElementMapFluxMeter efluxmeter =  ((DElementMapFluxMeter)map[x][y]);
			
			if(efluxmeter.machine_fluxMeter != null && efluxmeter.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, efluxmeter.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "flux meter info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+efluxmeter.machine_fluxMeter.customName, x_draw + 20, y_draw + 42, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "buffer size : "+ efluxmeter.machine_fluxMeter.fluxBufferSize, x_draw + 20, y_draw + 52, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "flux : "+ efluxmeter.machine_fluxMeter.fluxValue + " RF/Tick", x_draw + 20, y_draw + 62, 0xffffff);
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return from_type == ElementType.ENERGY;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapFluxMeter();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluxMeter)
		{
			if(project.guipanel.editmode)
			{
				project.guipanel.messageBox_set(new GMsgEMachineFluxMeter(project.guipanel.gui, (DElementMapFluxMeter) map[x][y]));
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluxMeter)
		{
			((DElementMapFluxMeter)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapFluxMeter)map[x][y]).posdim);
			((DElementMapFluxMeter)map[x][y]).machine_fluxMeter = machine_ instanceof MachineCL_FluxMeter ? (MachineCL_FluxMeter) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapFluxMeter)
		{
			if(((DElementMapFluxMeter)map[x][y]).posdim != null)
				((DElementMapFluxMeter)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
