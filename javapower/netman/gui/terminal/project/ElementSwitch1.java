package javapower.netman.gui.terminal.project;

import javapower.netman.gui.GuiTerminal;
import javapower.netman.gui.terminal.msg.GMsgEMachineSwitch;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachineCL_Switch;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Direction2D;
import javapower.netman.util.ElementType;
import net.minecraft.nbt.NBTTagCompound;

public class ElementSwitch1 extends Element
{
	@Override
	public String getName()
	{
		return "Switch";
	}
	
	@Override
	public int[] getIcon()
	{
		return new int[]{20,40,20,20};
	}
	
	@Override
	public void draw(int x_draw, int y_draw, int x, int y, DElementMap[][] map, GuiTerminal gui, boolean editmodi)
	{
		if(map[x][y] instanceof DElementMapSwitch)
		{
			if(((DElementMapSwitch)map[x][y]).machine_switch != null)
			{
				gui.drawTexturedModalRect(x_draw, y_draw, 20, ((DElementMapSwitch)map[x][y]).machine_switch.switch_pos ? 20 : 40, 20, 20);
			}
			else
			{
				gui.drawTexturedModalRect(x_draw, y_draw, 20, 60, 20, 20);
			}
		}
	}
	
	@Override
	public void drawOverlay(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapSwitch && ((DElementMapSwitch)map[x][y]).machine_switch != null)
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, ((DElementMapSwitch)map[x][y]).machine_switch.switch_pos ? "ON" : "OFF", x_draw+20, y_draw+20, 0xffff00);
		}
	}
	
	@Override
	public void drawInfo(int x_draw, int y_draw, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapSwitch)
		{
			DElementMapSwitch eswitch =  ((DElementMapSwitch)map[x][y]);
			
			if(eswitch.machine_switch != null && eswitch.posdim != null)
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "world pos :", x_draw + 10, y_draw + 10, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, eswitch.posdim.toText(), x_draw + 20, y_draw + 20, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "switch info :", x_draw + 10, y_draw + 32, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "custom name : "+eswitch.machine_switch.customName, x_draw + 20, y_draw + 42, 0xffffff);
				//project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "auto extract : "+ (eswitch.machine_switch.autoextract ? "Yes" : "No"), x_draw + 20, y_draw + 52, 0xffffff);
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "switch pos : "+ (eswitch.machine_switch.switch_pos ? "ON" : "OFF"), x_draw + 20, y_draw + 52, 0xffffff);
			}
			else
			{
				project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
			}
		}
		else
		{
			project.guipanel.gui.drawString(project.guipanel.gui.mc.fontRenderer, "no info / err", x_draw + 10, y_draw + 10, 0xffffff);
		}
	}
	
	@Override
	public boolean hasInfoPrompt()
	{
		return true;
	}
	
	@Override
	public boolean canConnectTo(Direction2D side_from, ElementType from_type, int x, int y, DElementMap[][] map, GuiTerminal gui)
	{
		return (side_from == Direction2D.DOWN || side_from == Direction2D.TOP) && from_type == ElementType.ENERGY;
	}
	
	@Override
	public DElementMap getDataBuilder()
	{
		return new DElementMapSwitch();
	}
	
	@Override
	public boolean mouseClick(int x, int y, float dx, float dy, int button, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapSwitch)
		{
			MachineCL_Switch mswitch = ((DElementMapSwitch)map[x][y]).machine_switch;
			if(!project.guipanel.editmode)
			{
				if(mswitch != null)
					mswitch.switchOnOff(!mswitch.switch_pos, project.guipanel.gui);
			}
			else
			{
				project.guipanel.messageBox_set(new GMsgEMachineSwitch(project.guipanel.gui, (DElementMapSwitch) map[x][y]));
			}
		}
		
		return false;
	}
	
	@Override
	public void readDataFromNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapSwitch)
		{
			((DElementMapSwitch)map[x][y]).posdim = new BlockPosDim(nbt, "mp");
			MachineCL machine_ = project.guipanel.gui.machines.get(((DElementMapSwitch)map[x][y]).posdim);
			((DElementMapSwitch)map[x][y]).machine_switch = machine_ instanceof MachineCL_Switch ? (MachineCL_Switch) machine_ : null;
		}
	}
	
	@Override
	public void writeDataToNBT(NBTTagCompound nbt, int x, int y, DElementMap[][] map, Project project)
	{
		if(map[x][y] instanceof DElementMapSwitch)
		{
			if(((DElementMapSwitch)map[x][y]).posdim != null)
				((DElementMapSwitch)map[x][y]).posdim.WriteToNBT(nbt, "mp");
		}
	}
	
	@Override
	public boolean hasCustomData()
	{
		return true;
	}
}
