package javapower.netman.gui.terminal.project;

public class RegisterElements
{
	
	//elements
	public static final Element element_void = new ElementVoid();
	public static final Element element_wire = new ElementWire();
	public static final Element element_transformerV = new ElementTransformer1();
	public static final Element element_transformerH = new ElementTransformer2();
	public static final Element element_generator = new ElementGenerator();
	public static final Element element_machine = new ElementMachine();
	public static final Element element_returnUp = new ElementReturnUp();
	public static final Element element_returnDown = new ElementReturnDown();
	public static final Element element_returnLeft = new ElementReturnLeft();
	public static final Element element_returnRight = new ElementReturnRight();
	public static final Element element_label = new ElementLabel();
	
	//machines elements
	public static final Element element_switch1 = new ElementSwitch1();
	public static final Element element_switch2 = new ElementSwitch2();
	public static final Element element_energyInfo = new ElementEnergyInfo();
	public static final Element element_counter = new ElementCounter();
	public static final Element element_fluxMeter = new ElementFluxMeter();
	
	//fluid machines elements
	public static final Element element_fluidValve1 = new ElementFluidValve1();
	public static final Element element_fluidValve2 = new ElementFluidValve2();
	public static final Element element_fluidStorageInfo = new ElementFluidStorageInfo();
	public static final Element element_fluidCounter = new ElementFluidCounter();
	public static final Element element_fluidFlowMeter = new ElementFluidFlowMeter();
	
	//fluid elements
	public static final Element element_pipe = new ElementPipe();
	public static final Element element_pump = new ElementPump();
	public static final Element element_fluidReturnUp = new ElementFluidReturnUp();
	public static final Element element_fluidReturnDown = new ElementFluidReturnDown();
	public static final Element element_fluidReturnLeft = new ElementFluidReturnLeft();
	public static final Element element_fluidReturnRight = new ElementFluidReturnRight();
	public static final Element element_fluidMachine = new ElementFluidMachine();
	
	public static final Element element_bridge = new ElementBridge();
	//register
	public static final Element[] elements = new Element[]
			{
					element_void,//0
					element_wire,//1
					element_transformerV,//2
					element_transformerH,//3
					element_generator,//4
					element_machine,//5
					element_returnUp,//6
					element_returnDown,//7
					element_returnLeft,//8
					element_returnRight,//9
					element_label,//10
					element_switch1,//11
					element_switch2,//12
					element_energyInfo,//13
					element_fluxMeter,//14
					element_counter,//15
					element_fluidValve1,//16
					element_fluidValve2,//17
					element_fluidStorageInfo,//18
					element_fluidCounter,//19
					element_fluidFlowMeter,//20
					element_pipe,//21
					element_pump,//22
					element_fluidReturnUp,//23
					element_fluidReturnDown,//24
					element_fluidReturnLeft,//25
					element_fluidReturnRight,//26
					element_fluidMachine,//27
					element_bridge,//28
			};
}
