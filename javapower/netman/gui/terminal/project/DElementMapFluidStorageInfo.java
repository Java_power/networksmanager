package javapower.netman.gui.terminal.project;

import javapower.netman.nww.client.MachineCL_FluidStorageInfo;
import javapower.netman.util.BlockPosDim;

public class DElementMapFluidStorageInfo extends DElementMap
{
	public MachineCL_FluidStorageInfo machine_storageinfo;
	public BlockPosDim posdim;
}
