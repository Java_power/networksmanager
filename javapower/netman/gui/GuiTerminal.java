package javapower.netman.gui;

import java.io.IOException;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import javapower.netman.core.Config;
import javapower.netman.gui.terminal.GuiPanelMainMenu;
import javapower.netman.gui.util.GuiPanel;
import javapower.netman.nww.client.MachineCL;
import javapower.netman.nww.client.MachinesClient;
import javapower.netman.proxy.ClientProxy;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.tileentity.TileEntityTerminal;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.IGUITileSync;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Vector2;
import javapower.netman.util.VectorScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class GuiTerminal extends GuiScreen implements IGUITileSync
{
	public TileEntityTerminal te_terminal;
	//---------- terminal setup ----------
	boolean init = false;
	int guiScrale = 0;
	
	//public int gui_opacity = 0;
	//public int terminalScrale = 1;
	
	public VectorScreen thisScreen;
	
	//---------- resolution ----------
	Vector2 minimumResolution = new Vector2(854, 480);
	boolean resolutionIsOk = true;
	
	// ---------- pre config map size ----------
	public Vector2[] map_sizes = new Vector2[]{
			new Vector2(16, 16),
			new Vector2(32, 32),
			new Vector2(64, 64),
			new Vector2(25, 13),
			new Vector2(50, 25),
			new Vector2(100, 50)
	};
	
	//---------- machines ----------
	public MachinesClient machines = new MachinesClient();
	
	//---------- gui panel ----------
	GuiPanel guip = null;
	
	public void SetGuiPanel(GuiPanel panel)
	{
		if(guip != null)
			guip.onPanelClosed();
		
		guip = panel;
		if(guip != null)
			guip.initPanel(this);
	}
	
	public GuiTerminal(Container inventorySlotsIn, TileEntityTerminal terminal)
	{
		super();
		//terminalScrale = Config.conf_client.get("general", "terminal gui scale", "x1", "", new String[] {"x1","x2"}).getString().equals("x2")? 2 : 1;
		//gui_opacity = Config.conf_client.get("general", "terminal background opacity", 255, "", 1, 255).getInt();
		te_terminal = terminal;
		fontRenderer = ClientProxy.minecraft.fontRenderer;
		
		thisScreen = new VectorScreen(new Vector2(ClientProxy.minecraft.displayWidth/Config.guiTerminal_scrale, ClientProxy.minecraft.displayHeight/Config.guiTerminal_scrale), new Vector2());
		
		NetworkUtils.sendToServerPlayerAsOpenGUI(terminal, this);
	}
	
	@Override
	public void initGui()
	{
		//JEIIntegration.DisableOverlay();
		//NEIIntegration.DisableOverlay();
		
		if(!init)
		{
			init = true;
			SetGuiPanel(new GuiPanelMainMenu());
			guiScrale = ClientProxy.minecraft.gameSettings.guiScale;
			ClientProxy.minecraft.gameSettings.guiScale = Config.guiTerminal_scrale;
			ClientProxy.minecraft.gameSettings.saveOptions();
			ClientProxy.minecraft.gameSettings.sendSettingsToServer();
			machines.machines.clear();
		}
		
		width = mc.displayWidth/Config.guiTerminal_scrale;
		height = mc.displayHeight/Config.guiTerminal_scrale;
		thisScreen.onResizeScreen(width, height);
		
		resolutionIsOk = width*Config.guiTerminal_scrale >= minimumResolution.x && height*Config.guiTerminal_scrale >= minimumResolution.y;
		
		super.initGui();
		
		// force update machines
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setByte("fgi", (byte)0);
		sendInfo(nbt);
	}
	
	@Override
	public void onGuiClosed()
	{
		ClientProxy.minecraft.gameSettings.guiScale = guiScrale;
		ClientProxy.minecraft.gameSettings.saveOptions();
		ClientProxy.minecraft.gameSettings.sendSettingsToServer();
		
		//JEIIntegration.ReApplyConfigurationOverlay();
		//NEIIntegration.ReApplyConfigurationOverlay();
		
		super.onGuiClosed();
	}

	@Override
	//protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		if(resolutionIsOk)
		{
			Gui.drawRect(0, 0, width, height+1, 0x00212121 | Config.guiTerminal_backgroudOpacity << 24);
			if(guip != null)
				guip.draw();
		}
		else
		{
			int x = width/2;
			int y = height/2;
			drawRect(0, 0, width, height, 0xff212121);
			drawRect(x - 250, y - 120, x + 250, y + 120, 0xffa00000);
			GL11.glColor4f(1, 1, 1, 1);
			mc.renderEngine.bindTexture(ResourceLocationRegister.texture_pictogram);
			drawTexturedModalRect(x - 280, y - 128, 0, 0, 255, 256);
			drawString(fontRenderer, "your resolution is too small for this interface !", x - 40, y + 20, 0xffffff);
			drawString(fontRenderer, "your resolution: width:"+ width+" height:"+height, x - 40, y + 35, 0xffffff);
			drawString(fontRenderer, "minimum resolution: width:"+ minimumResolution.x+" height:"+minimumResolution.y, x - 40, y + 50, 0xffffff);
		}
	}

	@Override
	public Class<? extends TileEntity> tileEntityLink()
	{
		return TileEntityTerminal.class;
	}

	@Override
	public void reciveDataFromServer(NBTTagCompound nbt)
	{
		if(nbt.hasKey("mi0"))
		{
			for(int i = 0 ; nbt.hasKey("mi"+i); ++i)
			{
				NBTTagCompound nbtm = nbt.getCompoundTag("mi"+i);
				if(nbtm != null)
				{
					BlockPosDim posd = new BlockPosDim(nbtm, "mwp");
					if(machines.exist(posd))
					{
						machines.updateNBT(posd, nbtm);
					}
					else
					{
						MachineCL m = MachineCL.getMachine(posd, nbtm.getString("class"));
						
						if(nbtm.hasKey("mnm"))
						{
							m.name = nbtm.getString("mnm");
						}
						
						m.customName = nbtm.getString("cn");
						m.updateNBT(nbtm);
						
						machines.put(posd, m);
					}
				}
			}
				
		}
		
		if(guip != null)
			guip.reciveDataFromServer(nbt);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if(resolutionIsOk)
		{
			if(guip == null || !guip.mouseClicked(mouseX, mouseY, mouseButton))
			super.mouseClicked(mouseX, mouseY, mouseButton);
		}
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(keyCode == 1 && resolutionIsOk && !Config.guiTerminal_KeyEscEnable)
			return;
		
		if(resolutionIsOk)
		{
			if(guip == null || !guip.keyTyped(typedChar, keyCode))
			{
				super.keyTyped(typedChar, keyCode);
				return;
			}
			else
				return;
		}
		
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	public void onResize(Minecraft mcIn, int w, int h)
	{
		width = w;
		height = h;
		
		thisScreen.onResizeScreen(w, h);
		
		if(guip != null)
			guip.onResize(mcIn, w, h);
		
		super.onResize(mcIn, w, h);
		resolutionIsOk = width*Config.guiTerminal_scrale >= minimumResolution.x && height*Config.guiTerminal_scrale >= minimumResolution.y;
	}
	
	@Override
	public void updateScreen()
	{
		thisScreen.update(Config.guiTerminal_scrale);
		
		if(resolutionIsOk)
		{
			if(guip != null)
				guip.update();
		}
		
		int mousewheel = Mouse.getDWheel();
		if(mousewheel != 0)
			mouseWheelEvent(mousewheel);
		
		super.updateScreen();

        if (!this.mc.player.isEntityAlive() || this.mc.player.isDead)
        {
            this.mc.player.closeScreen();
        }
	}
	
	public void mouseWheelEvent(int delta_wheel)
	{
		if(guip != null)
			guip.mouseWheelEvent(delta_wheel);
	}
	
	public void sendInfo(NBTTagCompound nbt)
	{
		NetworkUtils.sendToServerTheData(te_terminal, this, nbt);
	}
	
	public boolean doesGuiPauseGame()
    {
        return false;
    }

}
