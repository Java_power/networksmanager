package javapower.netman.gui;

import java.io.IOException;

import javapower.netman.eventio.IEventOut;
import javapower.netman.eventio.IEventVoid;
import javapower.netman.gui.util.GEButtonZero;
import javapower.netman.gui.util.GESwitch;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.tileentity.TileEntityRFCounter;
import javapower.netman.util.Tools;
import javapower.netman.util.Vector2;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class GuiRFCounter extends GuiBase<TileEntityRFCounter>
{
	GuiTextField textf_customname;
	long consomation = 0l;
	
	/*GESwitch switch_aextract = new GESwitch(new Vector2(73, 28), new IEventOut<Boolean>()
	{
		
		public void event(Boolean bool)
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setBoolean("ae", bool);
			sendInfo(nbt);
		}
	});*/
	
	GEButtonZero button_zero = new GEButtonZero(new Vector2(83, 8), mouspos, new IEventVoid()
	{
		
		@Override
		public void event()
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setBoolean("z0", true);
			sendInfo(nbt);
		}
	});
	
	public GuiRFCounter(Container inventorySlotsIn, TileEntityRFCounter te)
	{
		super(inventorySlotsIn, te, new Vector2(0, 165), new Vector2(100, 66));
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		elements.clear();
		//elements.add(switch_aextract);
		elements.add(button_zero);
		
		if(textf_customname == null)
			textf_customname = new GuiTextField(0, mc.fontRenderer, 6, 47, 88, 12);
	}

	@Override
	public void reciveDataFromServer(NBTTagCompound nbt)
	{
		/*if(nbt.hasKey("ae"))
			switch_aextract.switch_pos = nbt.getBoolean("ae");*/
		
		if(nbt.hasKey("cm"))
			consomation = nbt.getLong("cm");
		
		if(textf_customname != null && nbt.hasKey("cn"))
			textf_customname.setText(nbt.getString("cn"));
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
        textf_customname.mouseClicked(mouseX, mouseY, mouseButton);
        
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		String after = textf_customname.getText();
		textf_customname.textboxKeyTyped(typedChar, keyCode);
		if(after != textf_customname.getText())
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString("cn", textf_customname.getText());
			sendInfo(nbt);
		}
		
		super.keyTyped(typedChar, keyCode);
	}

	@Override
	protected ResourceLocation getBindTexture()
	{
		return ResourceLocationRegister.texture_rf_guis;
	}

	@Override
	protected void drawelements(int x, int y)
	{
		//drawString(mc.fontRenderer, "auto out:", x + 10, y + 28, 0xffaa00);
        
        drawString(mc.fontRenderer, Tools.longFormatToString(consomation)+" RF.Tick", x + 9, y + 10, 0xffffff);
        
        textf_customname.x = x + 6;
        textf_customname.y = y + 47;
        textf_customname.drawTextBox();
	}

	@Override
	protected void update()
	{
		textf_customname.updateCursorCounter();
	}

}
