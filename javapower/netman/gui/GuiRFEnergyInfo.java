package javapower.netman.gui;

import java.io.IOException;
import java.text.DecimalFormat;

import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.tileentity.TileEntityRFEnergyInfo;
import javapower.netman.util.Vector2;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class GuiRFEnergyInfo extends GuiBase<TileEntityRFEnergyInfo>
{
	GuiTextField textf_customname;
	
	int energy = 0;
	int energyMax = 0;
	
	DecimalFormat format = new DecimalFormat("##.##");
	
	public GuiRFEnergyInfo(Container inventorySlotsIn, TileEntityRFEnergyInfo _tileEntity)
	{
		super(inventorySlotsIn, _tileEntity, new Vector2(80, 112), new Vector2(156, 53));
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		elements.clear();
		
		if(textf_customname == null)
			textf_customname = new GuiTextField(0, mc.fontRenderer, 6, 47, 88, 12);
	}

	@Override
	public void reciveDataFromServer(NBTTagCompound nbt)
	{
		if(nbt.hasKey("ev"))
			energy = nbt.getInteger("ev");
		
		if(nbt.hasKey("em"))
			energyMax = nbt.getInteger("em");
		
		if(textf_customname != null && nbt.hasKey("cn"))
			textf_customname.setText(nbt.getString("cn"));
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
        textf_customname.mouseClicked(mouseX, mouseY, mouseButton);
        
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		String after = textf_customname.getText();
		textf_customname.textboxKeyTyped(typedChar, keyCode);
		if(after != textf_customname.getText())
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString("cn", textf_customname.getText());
			sendInfo(nbt);
		}
		
		super.keyTyped(typedChar, keyCode);
	}

	@Override
	protected ResourceLocation getBindTexture()
	{
		return ResourceLocationRegister.texture_rf_guis;
	}

	@Override
	protected void drawelements(int x, int y)
	{
		drawString(mc.fontRenderer, energy+"/"+energyMax+" RF", x + 9, y + 10, 0xffffff);
		drawString(mc.fontRenderer, format.format((((float)energy/(float)energyMax)*100))+" %", x + 9, y + 23, 0xffffff);
        
        textf_customname.x = x + 6;
        textf_customname.y = y + 34;
        textf_customname.drawTextBox();
	}

	@Override
	protected void update()
	{
		textf_customname.updateCursorCounter();
	}

}
