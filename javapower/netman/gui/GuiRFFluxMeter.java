package javapower.netman.gui;

import java.io.IOException;

import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.tileentity.TileEntityRFFluxMeter;
import javapower.netman.util.Vector2;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class GuiRFFluxMeter extends GuiBase<TileEntityRFFluxMeter>
{
	GuiTextField textf_customname;
	GuiTextField textf_buffer;
	int flux = 0;
	
	/*GESwitch switch_aextract = new GESwitch(new Vector2(73, 22), new IEventOut<Boolean>()
	{
		
		@Override
		public void event(Boolean bool)
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setBoolean("ae", bool);
			sendInfo(nbt);
		}
	});*/
	
	public GuiRFFluxMeter(Container inventorySlotsIn, TileEntityRFFluxMeter _tileEntity)
	{
		super(inventorySlotsIn, _tileEntity, new Vector2(100, 165), new Vector2(100, 71));
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		elements.clear();
		//elements.add(switch_aextract);
		
		if(textf_customname == null)
			textf_customname = new GuiTextField(0, mc.fontRenderer, 6, 52, 88, 12);
		
		if(textf_buffer == null)
		{
			textf_buffer = new GuiTextField(1, mc.fontRenderer, 72, 35, 26, 12);
			textf_buffer.setMaxStringLength(3);
		}
	}

	@Override
	public void reciveDataFromServer(NBTTagCompound nbt)
	{
		/*if(nbt.hasKey("ae"))
			switch_aextract.switch_pos = nbt.getBoolean("ae");*/
		
		if(nbt.hasKey("fv"))
			flux = nbt.getInteger("fv");
		
		if(textf_customname != null && nbt.hasKey("cn"))
			textf_customname.setText(nbt.getString("cn"));
		
		if(textf_buffer != null && nbt.hasKey("fs"))
		{
			textf_buffer.setText(""+nbt.getInteger("fs"));
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
        textf_customname.mouseClicked(mouseX, mouseY, mouseButton);
        textf_buffer.mouseClicked(mouseX, mouseY, mouseButton);
        
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		String after = textf_customname.getText();
		textf_customname.textboxKeyTyped(typedChar, keyCode);
		if(after != textf_customname.getText())
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString("cn", textf_customname.getText());
			sendInfo(nbt);
		}
		
		if("0123456789".indexOf(typedChar) != -1 || keyCode == 14)//TODO
		{
			textf_buffer.textboxKeyTyped(typedChar, keyCode);
		}
		
		if(textf_buffer.isFocused() && keyCode == 28)
		{
			int i = 1;
			if(textf_buffer.getText() != null || textf_buffer.getText() != "")
			{
				try
				{
					i = Integer.parseInt(textf_buffer.getText());
				}
				catch(NumberFormatException e)
				{
					i = 1;
				}
			}
			
			if(i > 256)
			{
				i = 255;
			}
			
			if(i <= 0)
			{
				i = 1;
			}
			
			textf_buffer.setText(""+i);
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setInteger("fs", i);
			sendInfo(nbt);
			textf_buffer.setFocused(false);
		}
		
		super.keyTyped(typedChar, keyCode);
	}

	@Override
	protected ResourceLocation getBindTexture()
	{
		return ResourceLocationRegister.texture_rf_guis;
	}
	
	@Override
	protected void drawelements(int x, int y)
	{
		//drawString(mc.fontRenderer, "auto out:", x + 10, y + 22, 0xffaa00);
		drawString(mc.fontRenderer, "buffer size:", x + 6, y + 37, 0xffaa00);
		
        drawString(mc.fontRenderer, flux+" RF/Tick", x + 9, y + 10, 0xffffff);
        
        if(textf_buffer.isFocused())
        {
        	drawString(mc.fontRenderer, "press enter for", x, y + 72, 0x0094FF);
        	drawString(mc.fontRenderer, "validate your value", x, y + 80, 0x0094FF);
        }
        
        textf_customname.x = x + 6;
        textf_customname.y = y + 52;
        textf_customname.drawTextBox();
        
        textf_buffer.x = x + 68;
        textf_buffer.y = y + 35;
        textf_buffer.drawTextBox();
	}

	@Override
	protected void update()
	{
		textf_customname.updateCursorCounter();
		textf_buffer.updateCursorCounter();
	}
	
}
