package javapower.netman.gui;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.tileentity.TileEntityFluidStorageInfo;
import javapower.netman.util.FluidTankInfoClient;
import javapower.netman.util.Tools;
import javapower.netman.util.Vector2;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidUtil;

public class GuiFluidStorageInfo extends GuiBase<TileEntityFluidStorageInfo>
{
	GuiTextField textf_customname;
	GuiButton button_page = new GuiButton(0, 10, 10, 20, 20, "0");
	
	DecimalFormat format = new DecimalFormat("##.##");
	
	List<FluidTankInfoClient> fluidtank = new ArrayList<FluidTankInfoClient>();
	int id_draw = 0;
	
	public GuiFluidStorageInfo(Container inventorySlotsIn, TileEntityFluidStorageInfo _tileEntity)
	{
		super(inventorySlotsIn, _tileEntity, new Vector2(80, 98), new Vector2(156, 67));
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		elements.clear();
		
		buttonList.add(button_page);
		
		if(textf_customname == null)
			textf_customname = new GuiTextField(0, mc.fontRenderer, 6, 47, 88, 12);
	}

	@Override
	public void reciveDataFromServer(NBTTagCompound nbt)
	{
		if(textf_customname != null && nbt.hasKey("cn"))
			textf_customname.setText(nbt.getString("cn"));
		if(nbt != null)
			for(String key : nbt.getKeySet())
			{
				if(key.startsWith("fn"))
				{
					int id = Tools.getInteger(key.substring(2));
					if(id >= 0)
					{
						String fluid_name = nbt.getString("fn"+id);
						if(fluidtank.size() > id)
						{
							if(fluid_name != null && fluid_name.length() > 0)
							{
								fluidtank.get(id)._fluidname = fluid_name;
							}
							else
							{
								fluidtank.remove(id);
							}
						}
						else
						{
							if(fluid_name != null && fluid_name.length() > 0)
								fluidtank.add(new FluidTankInfoClient(fluid_name, 0, 0));
						}
					}
				}
				else if(key.startsWith("fc"))
				{
					int id = Tools.getInteger(key.substring(2));
					if(id >= 0)
					{
						int fluid_cap = nbt.getInteger("fc"+id);
						if(fluidtank.size() > id)
						{
							if(fluid_cap > 0)
							{
								fluidtank.get(id)._capacity = fluid_cap;
							}
							else
							{
								fluidtank.remove(id);
							}
						}
						else
						{
							if(fluid_cap > 0)
								fluidtank.add(new FluidTankInfoClient("", fluid_cap, 0));
						}
					}
				}
				else if(key.startsWith("fa"))
				{
					int id = Tools.getInteger(key.substring(2));
					if(id >= 0)
					{
						int fluid_amt = nbt.getInteger("fa"+id);
						if(fluidtank.size() > id)
						{
								fluidtank.get(id)._amount = fluid_amt;
						}
						else
						{
								fluidtank.add(new FluidTankInfoClient("", 0, fluid_amt));
						}
					}
				}
			}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
        textf_customname.mouseClicked(mouseX, mouseY, mouseButton);
        
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		String after = textf_customname.getText();
		textf_customname.textboxKeyTyped(typedChar, keyCode);
		if(after != textf_customname.getText())
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString("cn", textf_customname.getText());
			sendInfo(nbt);
		}
		
		super.keyTyped(typedChar, keyCode);
	}

	@Override
	protected ResourceLocation getBindTexture()
	{
		return ResourceLocationRegister.texture_fluid_guis;
	}

	@Override
	protected void drawelements(int x, int y)
	{
		if(fluidtank.size() > id_draw)
		{
			FluidTankInfoClient ftc = fluidtank.get(id_draw);
			if(ftc != null)
			{
				drawString(mc.fontRenderer, ftc._fluidname, x + 9, y + 10, 0xffffff);
				drawString(mc.fontRenderer, ftc._amount+"/"+ftc._capacity+" mB", x + 9, y + 24, 0xffffff);
				drawString(mc.fontRenderer, format.format((((float)ftc._amount/(float)ftc._capacity)*100))+" %", x + 9, y + 37, 0xffffff);
			}
		}
		
        textf_customname.x = x + 6;
        textf_customname.y = y + 48;
        
        button_page.x = (width/2) + 40;
        button_page.y = (height/2) + 5;
        textf_customname.drawTextBox();
	}

	@Override
	protected void update()
	{
		textf_customname.updateCursorCounter();
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(id_draw +1 < fluidtank.size())
		{
			++id_draw;
		}
		else
		{
			id_draw = 0;
		}
		
		button_page.displayString = ""+id_draw;
	}

}
