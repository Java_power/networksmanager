package javapower.netman.eventio;

public interface IEventIn<T>
{
	public T event();
}
