package javapower.netman.eventio;

public interface IEventOut<T>
{
	public void event(T out);
}
