package javapower.netman.eventio;

public interface IEventIO<I,O>
{
	public I event(O out);
}
