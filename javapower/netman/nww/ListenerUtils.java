package javapower.netman.nww;

import java.util.ArrayList;
import java.util.List;

import javapower.netman.util.Result;
import net.minecraft.tileentity.TileEntity;

public class ListenerUtils
{
	public static boolean ListenerIsOnList(List<IModuleListener> listeners, IModuleListener listener)
    {
    	if(listeners != null && !listeners.isEmpty())
    		for(IModuleListener ln : listeners)
    		{
    			if(ln.equals(listener))
    				return true;
    		}
    	return false;
    }
    
    public static void RemoveListenerOnList(List<IModuleListener> listeners, IModuleListener listener)
    {
    	if(listeners != null && !listeners.isEmpty())
    	{
    		boolean isPresent = false;
    		int id = 0;
    		for(IModuleListener ln : listeners)
    		{
    			if(ln.equals(listener))
    			{
    				isPresent = true;
    				break;
    			}
    			++id;
    		}
    		
    		if(isPresent)
    			listeners.remove(id);
    	}
    }
    
    public static void UpdateListenerAccess(List<IModuleListener> listeners)
    {
    	List<Integer> notAccess = new ArrayList<Integer>();
    	
    	int id = 0;
    	for(IModuleListener l : listeners)
    	{
    		if(!ListenerIsOnList(listeners, l))
    		{
    			notAccess.add(id);
    		}
    		++id;
    	}
    	
    	for(int iid = notAccess.size() -1 ; iid > 0; ++iid)
    	{
    		listeners.remove((int)notAccess.get(iid));
    	}
    }
    
    public static <T extends TileEntity & IModuleListener> Result addThisMachine(T module, TileEntity target)
    {
    	if(target instanceof IModuleListenerAcceptor)
    	{
    		((IModuleListenerAcceptor)target).addListener(module);
    		return Result.SUCCESSFUL;
    	}
    	return Result.ERROR;
    }
    
    public static <T extends TileEntity & IModuleListener> Result removeThisMachine(T module, TileEntity target)
    {
    	if(target instanceof IModuleListenerAcceptor)
    	{
    		((IModuleListenerAcceptor)target).removeListener(module);
    		return Result.SUCCESSFUL;
    	}
    	return Result.ERROR;
    }
    
    public static void UpdateListenersNW(List<IMachineNetwork> machines)
    {
    	for(IMachineNetwork m : machines)
    	{
    		if(m instanceof IModuleListenerAcceptor)
    		{
    			((IModuleListenerAcceptor)m).updateListener();
    		}
    	}
    }
}
