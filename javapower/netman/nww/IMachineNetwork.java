package javapower.netman.nww;

import net.minecraft.tileentity.TileEntity;

public interface IMachineNetwork extends INetwork
{
	public TileEntity entity();
	public String customName();
	public EMachineType type();
	public String name();
}
