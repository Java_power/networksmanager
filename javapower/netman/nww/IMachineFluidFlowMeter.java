package javapower.netman.nww;

public interface IMachineFluidFlowMeter extends IMachineNetwork
{
	public int GetFlow();
}
