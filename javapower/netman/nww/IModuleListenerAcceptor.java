package javapower.netman.nww;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public interface IModuleListenerAcceptor
{
	public <T extends TileEntity & IModuleListener> void addListener(T module);
	public <T extends TileEntity & IModuleListener> void removeListener(T module);
	public <T extends TileEntity & IModuleListener> NBTTagCompound forceGetInfo(T module);
	public <T extends TileEntity & IModuleListener> void reciveInfoFromListener(T module, NBTTagCompound data);
	public void updateListener();
}
