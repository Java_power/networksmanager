package javapower.netman.nww;

import javapower.netman.util.IFluidTankInfo;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public interface IMachineFluidStorageInfo extends IMachineNetwork
{
	public IFluidTankInfo[] GetFluidTankInfo();
}
