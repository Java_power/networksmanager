package javapower.netman.nww;

public interface IMachineEnergyStorageInfo extends IMachineNetwork
{
	public int GetEnergyStored();
	public int GetMaxEnergyStored();
}
