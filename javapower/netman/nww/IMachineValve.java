package javapower.netman.nww;

public interface IMachineValve extends IMachineNetwork
{
	public void ValveOnOff(boolean onOff);
	public boolean getValvePos();
}
