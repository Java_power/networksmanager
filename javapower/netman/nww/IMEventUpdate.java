package javapower.netman.nww;

import java.util.List;

public interface IMEventUpdate
{
	public void onEventListMachinesUpdate(List<IMachineNetwork> machines);
}
