package javapower.netman.nww;

public interface IMachineFluidCounter extends IMachineNetwork
{
	public long GetConsomation();
	public void Reset();
}
