package javapower.netman.nww;

import javapower.netman.proxy.CommonProxy;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;

public class MachineProvider
{
	BlockPos block_pos;
	int dim_ID;
	IMachineNetwork machine;
	int node = 0;
	
	public MachineProvider(BlockPos _pos, int _dimId, IMachineNetwork _machine)
	{
		block_pos = _pos;
		dim_ID = _dimId;
		machine = _machine;
	}
	
	public MachineProvider(NBTTagCompound nbt)
	{
		if(nbt.hasKey("x") && nbt.hasKey("y") && nbt.hasKey("z"))
		{
			block_pos = new BlockPos(nbt.getInteger("x"), nbt.getInteger("y"), nbt.getInteger("z"));
		}
		
		dim_ID = nbt.getInteger("d");
		node = nbt.getInteger("n");
	}
	
	public IMachineNetwork getMachine()
	{
		return machine;
	}
	
	public int getDimID()
	{
		return dim_ID;
	}
	
	public BlockPos getBlockPos()
	{
		return block_pos;
	}
	
	public int getNode()
	{
		return node;
	}
	
	public void setNode(int _node)
	{
		node = _node;
	}
	
	public void setBlockPos(BlockPos blockpos)
	{
		block_pos = blockpos;
	}
	
	public void setDimID(int dimID)
	{
		dim_ID = dimID;
	}
	
	public void setMachine(IMachineNetwork _machine)
	{
		machine = _machine;
	}
	
	public void UpdateMachineOnWorld()
	{
		WorldServer world = CommonProxy.minecraftServer.getWorld(dim_ID);
		if(world != null)
		{
			TileEntity te = world.getTileEntity(block_pos);
			if(te instanceof IMachineNetwork)
			{
				machine = (IMachineNetwork) te;
			}
		}
		
		machine = null;
	}
	
	public void SaveInfoToNBT(NBTTagCompound nbt)
	{
		if(block_pos != null)
		{
			nbt.setInteger("x", block_pos.getX());
			nbt.setInteger("y", block_pos.getY());
			nbt.setInteger("z", block_pos.getZ());
		}
		
		nbt.setInteger("d", dim_ID);
		nbt.setInteger("n", node);
	}
	
	public void ReadInfoFromNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("x") && nbt.hasKey("y") && nbt.hasKey("z"))
		{
			block_pos = new BlockPos(nbt.getInteger("x"), nbt.getInteger("y"), nbt.getInteger("z"));
		}
		
		dim_ID = nbt.getInteger("d");
		node = nbt.getInteger("n");
	}
}
