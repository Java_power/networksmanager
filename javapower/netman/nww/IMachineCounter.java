package javapower.netman.nww;

public interface IMachineCounter extends IMachineNetwork
{
	public long GetConsomation();
	public void Reset();
}
