package javapower.netman.nww;

import net.minecraft.nbt.NBTTagCompound;

public interface IModuleListener
{
	public void listenerReciveInfo(IMachineNetwork from, NBTTagCompound data);
}
