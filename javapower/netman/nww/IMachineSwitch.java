package javapower.netman.nww;

public interface IMachineSwitch extends IMachineNetwork
{
	public void switchOnOff(boolean onOff);
	public boolean getSwitchPos();
}
