package javapower.netman.nww;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

public class MachineUtil
{
	public static List<IMachineNetwork> GetAllMachines(World world, BlockPos pos)
	{
		List<IMachineNetwork> machines = new ArrayList<IMachineNetwork>();
		List<Vec3i> visited = new ArrayList<Vec3i>();
		scan(machines, visited, pos, world);
		return machines;
	}
	
	private static void scan(List<IMachineNetwork> tes, List<Vec3i> vecs, BlockPos pos, World world)
	{
		if(world == null)return;
		Block b = world.getBlockState(pos).getBlock();
		if(!isVisited(vecs, pos) && b instanceof IBlockNetwork)
		{
			vecs.add(new Vec3i(pos.getX(), pos.getY(), pos.getZ()));
			if(((IBlockNetwork)b).IsMachine())
			{
				TileEntity te = world.getTileEntity(pos);
				if(te instanceof IMachineNetwork)tes.add((IMachineNetwork) te);
			}
			
			for(EnumFacing dir : EnumFacing.VALUES)
			{
				scan(tes, vecs, pos.offset(dir), world);
			}
		}
	}
	
	public static boolean isVisited(List<Vec3i> list_vec, BlockPos this_te)
	{
		if(this_te != null)for(Vec3i v : list_vec)
			if(v.getX() == this_te.getX() && v.getY() == this_te.getY() && v.getZ() == this_te.getZ()) return true;
		return false;
	}
	
	public static List<IMachineController> GetControllers(World world, BlockPos pos)
	{
		List<Vec3i> visited = new ArrayList<Vec3i>();
		List<IMachineController> tes = new ArrayList<IMachineController>();
		for(EnumFacing dir : EnumFacing.VALUES)
			scanController(tes, visited, pos.offset(dir), world);
		
		return tes;
	}
	
	private static void scanController(List<IMachineController> tes, List<Vec3i> vecs, BlockPos pos, World world)
	{
		if(world == null)return;
		Block b = world.getBlockState(pos).getBlock();
		if(!isVisited(vecs, pos) && b instanceof IBlockNetwork)
		{
			vecs.add(new Vec3i(pos.getX(), pos.getY(), pos.getZ()));
			if(((IBlockNetwork)b).IsMachine())
			{
				TileEntity te = world.getTileEntity(pos);
				if(te instanceof IMachineController)tes.add((IMachineController) te);
			}
			
			for(EnumFacing dir : EnumFacing.VALUES)
			{
				scanController(tes, vecs, pos.offset(dir), world);
			}
		}
	}
	
	public static void UpdateController(World world, BlockPos pos)
	{
		if(world == null || world.isRemote) return;
		List<IMachineController> mc = GetControllers(world, pos);
		if(mc != null && mc.size() == 1)
			mc.get(0).UpdateController();
		else if(mc != null && mc.size() > 1)
		{
			for(int i = 1; i < mc.size(); ++i)
				world.destroyBlock(mc.get(i).thispos().getPos(), true);
		}
	}
	
	public static String getMachineClassName(IMachineNetwork m)
	{
		if(m instanceof IMachineController)
			return IMachineController.class.getName();
		
		if(m instanceof IMachineCounter)
			return IMachineCounter.class.getName();
		
		if(m instanceof IMachineEnergyStorageInfo)
			return IMachineEnergyStorageInfo.class.getName();
		
		if(m instanceof IMachineFluidCounter)
			return IMachineFluidCounter.class.getName();
		
		if(m instanceof IMachineFluidFlowMeter)
			return IMachineFluidFlowMeter.class.getName();
		
		if(m instanceof IMachineFluidStorageInfo)
			return IMachineFluidStorageInfo.class.getName();
		
		if(m instanceof IMachineFluxMeter)
			return IMachineFluxMeter.class.getName();
		
		if(m instanceof IMachineSwitch)
			return IMachineSwitch.class.getName();
		
		if(m instanceof IMachineValve)
			return IMachineValve.class.getName();
		
		return IMachineNetwork.class.getName();
	}
}
