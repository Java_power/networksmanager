package javapower.netman.nww;

public enum EMachineType
{
	ENERGY_RF("RF", "energy", "Energy RF"),
	ENERGY_EU("EU", "energy", "Energy EU"),
	FLUID("B", "fluid", "Fluid"),
	GAS("B", "gas", "Gas"),
	ITEM("Item", "item", "Item"),
	REDSTONE("", "redstone", "Redstone"),
	MACHINE("", "machine", "Machine"),
	CALCULATING("", "calculating", "Calculating");
	
	private String unit, type, text;
	
	private EMachineType(String _unit, String _type, String _text)
	{
		unit = _unit;
		type = _type;
		text = _text;
	}
	
	public String getUnit()
	{
		return unit;
	}
	
	public String getType()
	{
		return type;
	}
	
	public String getText()
	{
		return text;
	}
	
}
