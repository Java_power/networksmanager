package javapower.netman.nww.client;

import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineCounter;
import javapower.netman.nww.IMachineEnergyStorageInfo;
import javapower.netman.nww.IMachineFluidCounter;
import javapower.netman.nww.IMachineFluidFlowMeter;
import javapower.netman.nww.IMachineFluidStorageInfo;
import javapower.netman.nww.IMachineFluxMeter;
import javapower.netman.nww.IMachineSwitch;
import javapower.netman.nww.IMachineValve;
import javapower.netman.proxy.ResourceLocationRegister;
import javapower.netman.util.BlockPosDim;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL
{
	public BlockPosDim dimensionalPosition;
	public String name = "";
	public String customName = "";
	public EMachineType machine_type = EMachineType.MACHINE;
	
	public MachineCL(BlockPosDim _dimensionalPosition)
	{
		/*if(te_class.equals(IMachineNetwork)
		{
			machine_type = ((IMachineNetwork)te).type();
		}*/
		
		dimensionalPosition = _dimensionalPosition;
	}
	
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		gui.drawRect(x, y, x+150, y+40, color());
		
		gui.drawString(font, "Name:"+name, x +1, y +1, 0xffffff);
		gui.drawString(font, "CustomName:"+customName, x +1, y +11, 0xffffff);
		gui.drawString(font, "Pos:["+dimensionalPosition.toText()+"]", x +1, y +21, 0xffffff);
		
		gui.mc.renderEngine.bindTexture(ResourceLocationRegister.texture_terminal_icons);
		gui.drawTexturedModalRect(x + 135, y+1, machine_type.ordinal()*14, 200, 14, 14);
	}
	
	protected int color()
	{
		return 0xff404040;
	}

	public void updateNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("cn"))
			customName = nbt.getString("cn");
		if(nbt.hasKey("tp"))
			machine_type = EMachineType.values()[nbt.getInteger("tp")];
	}
	
	//---------- static ----------
	
	public static MachineCL getMachine(BlockPosDim posd, String te_class)
	{
		if(te_class.equals(IMachineSwitch.class.getName()))
			return new MachineCL_Switch(posd);
		
		if(te_class.equals(IMachineFluxMeter.class.getName()))
			return new MachineCL_FluxMeter(posd);
		
		if(te_class.equals(IMachineCounter.class.getName()))
			return new MachineCL_Counter(posd);
		
		if(te_class.equals(IMachineEnergyStorageInfo.class.getName()))
			return new MachineCL_EnergyStorageInfo(posd);
		
		if(te_class.equals(IMachineValve.class.getName()))
			return new MachineCL_FluidValve(posd);
		
		if(te_class.equals(IMachineFluidCounter.class.getName()))
			return new MachineCL_FluidCounter(posd);
		
		if(te_class.equals(IMachineFluidFlowMeter.class.getName()))
			return new MachineCL_FluidFlowMeter(posd);
		
		if(te_class.equals(IMachineFluidStorageInfo.class.getName()))
			return new MachineCL_FluidStorageInfo(posd);
					
		return new MachineCL(posd);
	}
}
