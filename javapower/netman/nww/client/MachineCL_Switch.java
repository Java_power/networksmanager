package javapower.netman.nww.client;

import javapower.netman.util.BlockPosDim;
import javapower.netman.util.IGUITileSync;
import javapower.netman.util.NetworkUtils;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL_Switch extends MachineCL
{
	public boolean switch_pos = false;
	//public boolean autoextract = false;
	
	public MachineCL_Switch(BlockPosDim _dimensionalPosition)
	{
		super(_dimensionalPosition);
	}
	
	@Override
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		super.drawGuiInfo(x, y, gui, font);
		//gui.drawString(font, "SwitchPos:"+ (switch_pos ? "ON":"OFF") + " | AE:"+ (autoextract ? "Yes":"No"), x +1, y +31, 0xffffff);
		gui.drawString(font, "SwitchPos:"+ (switch_pos ? "ON":"OFF"), x +1, y +31, 0xffffff);
	}
	
	@Override
	protected int color()
	{
		return 0xffba0000;
	}
	
	public void updateNBT(NBTTagCompound nbt)
	{
		super.updateNBT(nbt);
		
		if(nbt.hasKey("sw"))
			switch_pos = nbt.getBoolean("sw");
		
		/*if(nbt.hasKey("ae"))
			autoextract = nbt.getBoolean("ae");*/
	}
	
	public void switchOnOff(boolean onOff, IGUITileSync gui)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("sw", onOff);
		NetworkUtils.sendToServerTheData(dimensionalPosition, gui, nbt);
	}
}
