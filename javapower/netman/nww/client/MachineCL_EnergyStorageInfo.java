package javapower.netman.nww.client;

import javapower.netman.util.BlockPosDim;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL_EnergyStorageInfo extends MachineCL
{
	public int energy = 0;
	public int energyMax = 0;
	
	public MachineCL_EnergyStorageInfo(BlockPosDim _dimensionalPosition)
	{
		super(_dimensionalPosition);
	}
	
	@Override
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		super.drawGuiInfo(x, y, gui, font);
		gui.drawString(font, "energy:"+ energy+ "/"+ energyMax, x +1, y +31, 0xffffff);
	}
	
	@Override
	protected int color()
	{
		return 0xffba0000;
	}
	
	public void updateNBT(NBTTagCompound nbt)
	{
		super.updateNBT(nbt);
		
		if(nbt.hasKey("ev"))
			energy = nbt.getInteger("ev");
		
		if(nbt.hasKey("em"))
			energyMax = nbt.getInteger("em");
	}

	public String percentage()
	{
		try
		{
			return (energy*100)/energyMax+" %";
		}
		catch (ArithmeticException e)
		{
			return "0 %";
		}
	}
}
