package javapower.netman.nww.client;

import javapower.netman.util.BlockPosDim;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL_FluidFlowMeter extends MachineCL
{
	public int fluxValue = 0;
	public int fluxBufferSize = 0;
	
	public MachineCL_FluidFlowMeter(BlockPosDim _dimensionalPosition)
	{
		super(_dimensionalPosition);
	}
	
	@Override
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		super.drawGuiInfo(x, y, gui, font);
		gui.drawString(font, "FVal:"+ fluxValue+ " FBufferSize:"+ fluxBufferSize, x +1, y +31, 0xffffff);
	}
	
	@Override
	protected int color()
	{
		return 0xff0064b7;
	}
	
	public void updateNBT(NBTTagCompound nbt)
	{
		super.updateNBT(nbt);
		
		if(nbt.hasKey("fv"))
			fluxValue = nbt.getInteger("fv");
		
		if(nbt.hasKey("fs"))
			fluxBufferSize = nbt.getInteger("fs");
	}
}
