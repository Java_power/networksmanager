package javapower.netman.nww.client;

import javapower.netman.util.BlockPosDim;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL_FluxMeter extends MachineCL
{
	//public boolean autoextract = false;
	public int fluxValue = 0;
	public int fluxBufferSize = 0;
	
	public MachineCL_FluxMeter(BlockPosDim _dimensionalPosition)
	{
		super(_dimensionalPosition);
	}
	
	@Override
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		super.drawGuiInfo(x, y, gui, font);
		//gui.drawString(font, "FVal:"+ fluxValue+ " FBufferSize:"+ fluxBufferSize+ " | AE:"+ (autoextract ? "Yes":"No"), x +1, y +31, 0xffffff);
		gui.drawString(font, "FVal:"+ fluxValue+ " FBufferSize:"+ fluxBufferSize, x +1, y +31, 0xffffff);
	}
	
	@Override
	protected int color()
	{
		return 0xffba0000;
	}
	
	public void updateNBT(NBTTagCompound nbt)
	{
		super.updateNBT(nbt);
		
		/*if(nbt.hasKey("ae"))
			autoextract = nbt.getBoolean("ae");*/
		
		if(nbt.hasKey("fv"))
			fluxValue = nbt.getInteger("fv");
		
		if(nbt.hasKey("fs"))
			fluxBufferSize = nbt.getInteger("fs");
	}
}
