package javapower.netman.nww.client;

import javapower.netman.util.BlockPosDim;
import javapower.netman.util.IGUITileSync;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL_Counter extends MachineCL
{
	//public boolean autoextract = false;
	public long consomation = 0l;
	
	public MachineCL_Counter(BlockPosDim _dimensionalPosition)
	{
		super(_dimensionalPosition);
	}
	
	@Override
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		super.drawGuiInfo(x, y, gui, font);
		//gui.drawString(font, "conso:"+ Tools.longFormatToString(consomation)+ " | AE:"+ (autoextract ? "Yes":"No"), x +1, y +31, 0xffffff);
		gui.drawString(font, "conso:"+ Tools.longFormatToString(consomation), x +1, y +31, 0xffffff);
		
	}
	
	@Override
	protected int color()
	{
		return 0xffba0000;
	}
	
	public void updateNBT(NBTTagCompound nbt)
	{
		super.updateNBT(nbt);
		
		/*if(nbt.hasKey("ae"))
			autoextract = nbt.getBoolean("ae");*/
		
		if(nbt.hasKey("cm"))
			consomation = nbt.getLong("cm");
	}
	
	public void zero(IGUITileSync gui)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("z0", true);
		NetworkUtils.sendToServerTheData(dimensionalPosition, gui, nbt);
	}
}
