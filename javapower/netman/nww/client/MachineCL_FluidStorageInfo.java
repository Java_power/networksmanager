package javapower.netman.nww.client;

import java.util.ArrayList;
import java.util.List;

import javapower.netman.util.BlockPosDim;
import javapower.netman.util.FluidTankInfoClient;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL_FluidStorageInfo extends MachineCL
{
	public List<FluidTankInfoClient> tanks = new ArrayList<FluidTankInfoClient>();
	int next = 0;
	int id_page = 0;
	
	public MachineCL_FluidStorageInfo(BlockPosDim _dimensionalPosition)
	{
		super(_dimensionalPosition);
	}

	@Override
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		super.drawGuiInfo(x, y, gui, font);
		
		if(next > 10)
		{
			next = 0;
			if(tanks.size()-1 > id_page)
				++id_page;
			else
				id_page = 0;
		}
		else
		{
			++next;
		}
		
		if(tanks.size() > 0)
		{
			FluidTankInfoClient ftic = tanks.get(id_page);
			if(ftic == null)
				return;
			gui.drawString(font, "FTank:"+ ftic._fluidname+ " -> "+ ftic._amount+"/"+ftic._capacity, x +1, y +31, 0xffffff);
		}
	}
	
	
	@Override
	protected int color()
	{
		return 0xff0064b7;
	}
	
	public void updateNBT(NBTTagCompound nbt)
	{
		super.updateNBT(nbt);
		
		for(String key : nbt.getKeySet())
		{
			if(key.startsWith("fc"))//fluid capacity
			{
				int id = "0123456789".indexOf(key.charAt(2));
				if(tanks.size() > id)
				{
					tanks.get(id)._capacity = nbt.getInteger(key);
				}
				else
				{
					tanks.add(new FluidTankInfoClient("", nbt.getInteger(key), 0));
				}
			}
			else if(key.startsWith("fa"))//fluid amount
			{
				int id = "0123456789".indexOf(key.charAt(2));
				if(tanks.size() > id)
				{
					tanks.get(id)._amount = nbt.getInteger(key);
				}
				else
				{
					tanks.add(new FluidTankInfoClient("", 0, nbt.getInteger(key)));
				}
			}
			else if(key.startsWith("fn"))//fluid name
			{
				int id = "0123456789".indexOf(key.charAt(2));
				if(tanks.size() > id)
				{
					tanks.get(id)._fluidname = nbt.getString(key);
				}
				else
				{
					tanks.add(new FluidTankInfoClient(nbt.getString(key), 0, 0));
				}
			}
		}
	}
	
}
