package javapower.netman.nww.client;

import javapower.netman.util.BlockPosDim;
import javapower.netman.util.IGUITileSync;
import javapower.netman.util.NetworkUtils;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;

public class MachineCL_FluidValve extends MachineCL
{
	public boolean valve_pos = false;
	
	public MachineCL_FluidValve(BlockPosDim _dimensionalPosition)
	{
		super(_dimensionalPosition);
	}
	
	@Override
	public void drawGuiInfo(int x, int y, GuiScreen gui, FontRenderer font)
	{
		super.drawGuiInfo(x, y, gui, font);
		gui.drawString(font, "ValvePos:"+ (valve_pos ? "Open":"Closed"), x +1, y +31, 0xffffff);
	}
	
	@Override
	protected int color()
	{
		return 0xff0064b7;
	}
	
	public void updateNBT(NBTTagCompound nbt)
	{
		super.updateNBT(nbt);
		if(nbt.hasKey("vp"))
			valve_pos = nbt.getBoolean("vp");
	}
	
	public void valveOnOff(boolean onOff, IGUITileSync gui)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("vp", onOff);
		NetworkUtils.sendToServerTheData(dimensionalPosition, gui, nbt);
	}
}
