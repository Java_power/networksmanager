package javapower.netman.nww.client;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javapower.netman.core.NetworksManager;
import javapower.netman.util.BlockPosDim;
import net.minecraft.nbt.NBTTagCompound;

public class MachinesClient
{
	public List<MachineLocalized> machines = new ArrayList<MachineLocalized>();
	
	public MachinesClient()
	{
		
	}
	
	public void updateNBT(BlockPosDim pos, NBTTagCompound nbt)
	{
		for(MachineLocalized ml : machines)
		{
			if(ml != null && ml.loc != null && ml.loc.equals(pos))
			{
				ml.machine.updateNBT(nbt);
			}
		}
	}
	
	public void put(BlockPosDim pos, MachineCL cl)
	{
		try
		{
			if(pos == null)
				return;
			for(MachineLocalized ml : machines)
			{
				if(ml != null && ml.loc != null && ml.loc.equals(pos))
				{
					ml.machine = cl;
					return;
				}
			}
			
			MachineLocalized ml = new MachineLocalized(pos);
			ml.machine = cl;
			machines.add(ml);
		}
		catch (ConcurrentModificationException e)
		{
			if(NetworksManager.PrintStackTrace_TileEntity)
				e.printStackTrace();
		}
	}
	
	public MachineCL get(BlockPosDim pos)
	{
		for(MachineLocalized ml : machines)
		{
			if(ml != null && ml.loc != null && ml.loc.equals(pos))
			{
				return ml.machine;
			}
		}
		return null;
	}
	
	public boolean exist(BlockPosDim pos)
	{
		for(MachineLocalized ml : machines)
		{
			if(ml != null && ml.loc != null && ml.loc.equals(pos))
			{
				return true;
			}
		}
		return false;
	}
	
	public void remove(BlockPosDim pos)
	{
		int id = 0;
		boolean found = false;
		for(MachineLocalized ml : machines)
		{
			if(ml != null && ml.loc != null && ml.loc.equals(pos))
			{
				found = true;
				break;
			}
			
			++id;
		}
		
		if(found)
		machines.remove(id);
	}
	
	public void removeVoids()
	{
		while(true)
		{
			int id = 0;
			boolean found = false;
			for(MachineLocalized ml : machines)
			{
				if(ml == null || ml.machine == null)
				{
					found = true;
					break;
				}
				
				++id;
			}
			
			if(found)
				machines.remove(id);
			else return;
		}
	}
	
	public class MachineLocalized
	{
		public BlockPosDim loc;
		public MachineCL machine = null;
		
		public MachineLocalized(BlockPosDim _loc)
		{
			loc = _loc;
		}
	}
}
