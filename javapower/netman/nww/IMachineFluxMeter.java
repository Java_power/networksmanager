package javapower.netman.nww;

public interface IMachineFluxMeter extends IMachineNetwork
{
	public int GetFlux();
}
