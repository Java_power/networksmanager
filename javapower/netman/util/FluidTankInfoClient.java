package javapower.netman.util;

import net.minecraft.nbt.NBTTagCompound;

public class FluidTankInfoClient implements IFluidTankInfo
{
	public int _capacity = 0, _amount = 0;
	public String _fluidname = "";
	
	public FluidTankInfoClient(String fluidn, int capacity, int amount)
	{
		_fluidname = fluidn;
		_capacity = capacity;
		_amount = amount;
	}
	
	@Override
	public int capacity()
	{
		return _capacity;
	}

	@Override
	public int amount()
	{
		return _amount;
	}

	@Override
	public String fluidName()
	{
		return _fluidname;
	}
	
	public void putCap(NBTTagCompound nbt, int id, boolean fu)
	{
		if(fu || nbt.hasKey("fc"+id))
			_capacity = nbt.getInteger("fc"+id);
	}
	
	public void putAmt(NBTTagCompound nbt, int id, boolean fu)
	{
		if(fu || nbt.hasKey("fa"+id))
			_amount = nbt.getInteger("fa"+id);
	}
	
	public void putName(NBTTagCompound nbt, int id, boolean fu)
	{
		if(fu || nbt.hasKey("fn"+id))
			_fluidname = nbt.getString("fn"+id);
	}

}
