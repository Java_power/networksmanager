package javapower.netman.util;

import net.minecraft.block.Block;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class GuiUtils
{
	public static GuiScreen getGuiContainer(World world, BlockPos pos)
	{
		Block block = world.getBlockState(pos).getBlock();
		if(block != null && block instanceof IGuiRegister)
				return ((IGuiRegister)block).getGui(world.getTileEntity(pos));
		
		return null;
	}
	
	public static Container getContainer(World world, BlockPos pos)
	{
		Block block = world.getBlockState(pos).getBlock();
		if(block != null && block instanceof IGuiRegister)
				return ((IGuiRegister)block).getContainer(world.getTileEntity(pos));
		
		return null;
	}
}
