package javapower.netman.util;

public enum ElementType
{
	ENERGY,
	FLUID,
	GAS
}
