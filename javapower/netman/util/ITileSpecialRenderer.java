package javapower.netman.util;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface ITileSpecialRenderer
{
	@SideOnly(Side.CLIENT)
	public <T extends TileEntity> TileEntitySpecialRenderer<? extends T> getRender(int tileID);
}
