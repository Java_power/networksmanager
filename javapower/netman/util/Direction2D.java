package javapower.netman.util;

public enum Direction2D
{
	LEFT(-1,0),
	TOP(0,-1),
	RIGHT(1,0),
	DOWN(0,1);
	
	int dx, dy;
	private Direction2D(int _x, int _y)
	{
		dx = _x;
		dy = _y;
	}
	
	public int getDX()
	{
		return dx;
	}
	
	public int getDY()
	{
		return dy;
	}
	
	public Direction2D opposite()
	{
		if(this == Direction2D.LEFT)
			return Direction2D.RIGHT;
		else if(this == Direction2D.RIGHT)
			return Direction2D.LEFT;
		else if(this == Direction2D.TOP)
			return Direction2D.DOWN;
		else
			return Direction2D.TOP;
	}
}
