package javapower.netman.util;

public interface IFluidTankInfo
{
	public int capacity();
	public int amount();
	public String fluidName();
}
