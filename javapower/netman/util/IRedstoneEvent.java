package javapower.netman.util;

import net.minecraft.util.EnumFacing;

public interface IRedstoneEvent
{
	public void redstoneEvent(int redstonePower, EnumFacing face);
	public int getRedstoneOutputSinal(EnumFacing face);
}
