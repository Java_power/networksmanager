package javapower.netman.util;

import net.minecraft.block.Block;

public interface IBlockRegister extends IItemRegister
{
	public Block getBlock();
}
