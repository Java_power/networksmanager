package javapower.netman.util;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class FluidTankInfo implements IFluidTankInfo
{
	public int _capacity = 0, _amount = 0;
	public String _fluidname = "";
	public IFluidTankProperties _fluid = null;
	
	public NBTTagCompound update_pakett = null;
	public Var<Boolean> event_update = null;
	
	public FluidTankInfo(IFluidTankProperties fluid, NBTTagCompound update_nbt, Var<Boolean> _event_update)
	{
		_fluid = fluid;
		update_pakett = update_nbt;
		event_update = _event_update;
	}
	
	@Override
	public int capacity()
	{
		return _capacity;
	}

	@Override
	public int amount()
	{
		return _amount;
	}

	@Override
	public String fluidName()
	{
		return _fluidname;
	}
	
	public void forceUpdate(int id)
	{
		if(_fluid == null)
		{
			update_pakett.setString("fn"+id, _fluidname);
			
			_capacity = 0;
			update_pakett.setInteger("fc"+id, _capacity);
			
			_amount = 0;
			update_pakett.setInteger("fa"+id, _amount);
		}
		else
		{
			FluidStack fl = _fluid.getContents();
			
			_capacity = _fluid.getCapacity();
			update_pakett.setInteger("fc"+id, _capacity);
			
			if(fl != null)
			{
				_fluidname = fl.getLocalizedName();
				update_pakett.setString("fn"+id, _fluidname);
					
				_amount = fl.amount;
				update_pakett.setInteger("fa"+id, _amount);
			}
			else
			{
				_fluidname = "";
				update_pakett.setString("fn"+id, _fluidname);
					
				_amount = 0;
				update_pakett.setInteger("fa"+id, _amount);
			}
		}
		
		event_update.setVar(true);
	}
	
	public void forceUpdateInThisNBT(int id, NBTTagCompound nbt)
	{
		if(_fluid == null)
		{
			nbt.setString("fn"+id, _fluidname);
			
			_capacity = 0;
			nbt.setInteger("fc"+id, _capacity);
			
			_amount = 0;
			nbt.setInteger("fa"+id, _amount);
		}
		else
		{
			FluidStack fl = _fluid.getContents();
			
			_capacity = _fluid.getCapacity();
			nbt.setInteger("fc"+id, _capacity);
			
			if(fl != null)
			{
				_fluidname = fl.getLocalizedName();
				nbt.setString("fn"+id, _fluidname);
					
				_amount = fl.amount;
				nbt.setInteger("fa"+id, _amount);
			}
			else
			{
				_fluidname = "";
				nbt.setString("fn"+id, _fluidname);
					
				_amount = 0;
				nbt.setInteger("fa"+id, _amount);
			}
		}
		
		//event_update.setVar(true);
	}
	
	public void update(int id, IFluidTankProperties fluid)
	{
		if(fluid != null && fluid != _fluid)
			_fluid = fluid;
		
		if(_fluid == null)
		{
			if(_fluidname != "")
			{
				_fluidname = "";
				update_pakett.setString("fn"+id, _fluidname);
				event_update.setVar(true);
			}
			
			if(_capacity != 0)
			{
				_capacity = 0;
				update_pakett.setInteger("fc"+id, _capacity);
				event_update.setVar(true);
			}
			
			if(_amount != 0)
			{
				_amount = 0;
				update_pakett.setInteger("fa"+id, _amount);
				event_update.setVar(true);
			}
		}
		else
		{
			FluidStack fl = _fluid.getContents();
			
			if(_fluid.getCapacity() != _capacity)
			{
				_capacity = _fluid.getCapacity();
				update_pakett.setInteger("fc"+id, _capacity);
				event_update.setVar(true);
			}
			
			if(fl != null)
			{
				if(!_fluidname.equals(fl.getLocalizedName()))
				{
					_fluidname = fl.getLocalizedName();
					update_pakett.setString("fn"+id, _fluidname);
					event_update.setVar(true);
				}
				
				if(_amount != fl.amount)
				{
					_amount = fl.amount;
					update_pakett.setInteger("fa"+id, _amount);
					event_update.setVar(true);
				}
			}
			else
			{
				if(_fluidname != "")
				{
					_fluidname = "";
					update_pakett.setString("fn"+id, _fluidname);
					event_update.setVar(true);
				}
				
				if(_amount != 0)
				{
					_amount = 0;
					update_pakett.setInteger("fa"+id, _amount);
					event_update.setVar(true);
				}
			}
		}
	}

}
