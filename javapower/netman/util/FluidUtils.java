package javapower.netman.util;

import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class FluidUtils
{
	
	final static IFluidTankProperties[] fluidtankdrain = new IFluidTankProperties[]
			{
				new IFluidTankProperties()
				{
					
					@Override
					public FluidStack getContents()
					{
						return null;
					}
					
					@Override
					public int getCapacity()
					{
						return Integer.MAX_VALUE-1;
					}
					
					@Override
					public boolean canFillFluidType(FluidStack fluidStack)
					{
						return false;
					}
					
					@Override
					public boolean canFill()
					{
						return false;
					}
					
					@Override
					public boolean canDrainFluidType(FluidStack fluidStack)
					{
						return false;
					}
					
					@Override
					public boolean canDrain()
					{
						return true;
					}
				}	
			};
	
	public static IFluidTankProperties[] getTankPropertiesDrain()
	{
		return fluidtankdrain;
	}
}
