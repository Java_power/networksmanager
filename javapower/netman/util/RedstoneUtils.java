package javapower.netman.util;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class RedstoneUtils
{
	public static int block_getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		return blockState.getWeakPower(blockAccess, pos, side);
	}
	
	public static int block_getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		TileEntity te = blockAccess.getTileEntity(pos);
		if(te instanceof IRedstoneEvent && side != null)
			return ((IRedstoneEvent)te).getRedstoneOutputSinal(side);
		return 0;
	}
	
	public void block_neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
	{
		TileEntity te = worldIn.getTileEntity(pos);
		if(!worldIn.isRemote && te instanceof IRedstoneEvent)
		{
			EnumFacing fromface = Tools.getFacingForm2Blocks(pos, fromPos);
			((IRedstoneEvent)te).redstoneEvent(worldIn.getRedstonePower(fromPos, fromface), fromface);
		}
	}
}
