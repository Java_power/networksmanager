package javapower.netman.util;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IGuiRegister
{
	@SideOnly(Side.CLIENT)
	public GuiScreen getGui(TileEntity tile);
	public Container getContainer(TileEntity tile);
}
