package javapower.netman.util;

import java.lang.reflect.Field;

import javapower.netman.core.NetworksManager;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class RegisterUtils
{
	public static void RegisterItems(Field[] fields, IForgeRegistry<Item> register)
	{
		for(Field field : fields)
    	{
    		try
    		{
				Object var = field.get(null);
				if(allModsIsLoaded(var) && var instanceof IItemRegister)
				{
					Item i = ((IItemRegister)var).getItem();
					if(i != null)
						register.register(i);
				}
			}
    		catch (Exception ex)
    		{
				ex.printStackTrace();
			}
    	}
	}
	
	public static void RegisterBlocks(Field[] fields, IForgeRegistry<Block> register)
	{
		for(Field field : fields)
    	{
    		try
    		{
				Object var = field.get(null);
				if(allModsIsLoaded(var) && var instanceof IBlockRegister)
				{
					Block b = ((IBlockRegister)var).getBlock();
					if(b != null)
						register.register(b);
				}
			}
    		catch (Exception ex)
    		{
				ex.printStackTrace();
			}
    	}
	}
	
	public static void RegisterTilesEntity(Field[] fields)
	{
		for(Field field : fields)
    	{
    		try
    		{
				Object var = field.get(null);
				if(allModsIsLoaded(var) && var instanceof ITileRegister)
				{
					TileNamed[] tiles = ((ITileRegister)var).getTilesEntity();
					if(tiles != null && tiles.length > 0)
						for(TileNamed cte : tiles)
						{
							GameRegistry.registerTileEntity(cte.TClass, NetworksManager.MODID + ":" + cte.TName);
						}
				}
			}
    		catch (Exception ex)
    		{
				ex.printStackTrace();
			}
    	}
	}
	
	@SideOnly(Side.CLIENT)
	public static void RegisterIRender(Field[] fields)
	{
		for(Field field : fields)
    	{
    		try
    		{
				Object var = field.get(null);
				if(allModsIsLoaded(var) && var instanceof IRenderItemRegister && var instanceof IItemRegister)
				{
					Item item = ((IItemRegister)var).getItem();
					if(var instanceof ILoadRenderModel)
					{
						for(ItemRenderCast renderCast : ((IRenderItemRegister)var).getItemsRender())
						{
							if(renderCast != null)
							{
								String name = item.getUnlocalizedName(new ItemStack(item, 1, renderCast.getMetadata())).replace("item.", "");
								ModelBakery.registerItemVariants(item, renderCast.getModelRL(name));
							}
						}
					}
					for(ItemRenderCast renderCast : ((IRenderItemRegister)var).getItemsRender())
					{
						if(renderCast != null)
						{
							String name = item.getUnlocalizedName(new ItemStack(item, 1, renderCast.getMetadata())).replace("item.", "");
							if(name.startsWith("tile."))
								name = name.replace("tile.", "");
							ModelLoader.setCustomModelResourceLocation(item, renderCast.getMetadata(), renderCast.getModelRL(name));
						}
					}
				}
			}
    		catch (Exception ex)
    		{
				ex.printStackTrace();
			}
    	}
	}
	
	public static boolean allModsIsLoaded(Object var)
	{
		if(var instanceof IModsDependency)
		{
			for(String modid : ((IModsDependency)var).modsDependency())
			{
				if(!Loader.isModLoaded(modid))
				{
					return false;
				}
			}
		}
		return true;
	}
	
	/*public static void RegisterTilesEntitySR(Field[] fields)
	{
		for(Field field : fields)
    	{
    		try
    		{
				Object var = field.get(null);
				if(var instanceof ITileRegister && var instanceof ITileSpecialRenderer)
				{
					TileNamed[] tiles = ((ITileRegister)var).getTilesEntity();
					
					if(tiles != null && tiles.length > 0)
					{
						int _id = 0;
						for(TileNamed cte : tiles)
						{
							ClientRegistry.bindTileEntitySpecialRenderer(cte.TClass, ((ITileSpecialRenderer)var).getRender(_id));
							++_id;
						}
					}
				}
			}
    		catch (Exception ex)
    		{
				ex.printStackTrace();
			}
    	}
	}*/
}
