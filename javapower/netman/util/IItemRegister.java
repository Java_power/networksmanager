package javapower.netman.util;

import net.minecraft.item.Item;

public interface IItemRegister
{
	public Item getItem();
}
