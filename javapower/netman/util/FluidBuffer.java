package javapower.netman.util;

import javax.annotation.Nullable;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class FluidBuffer
{
	@Nullable
	FluidStack fluid;
	protected IFluidTankProperties[] tankProperties;
	
	public FluidBuffer()
	{
		fluid = null;
	}
	
	public void readFromNBT(NBTTagCompound nbt)
    {
        if (!nbt.hasKey("Empty"))
            fluid = FluidStack.loadFluidStackFromNBT(nbt);
        else
            fluid = null;
    }

    public void writeToNBT(NBTTagCompound nbt)
    {
        if (fluid != null)
            fluid.writeToNBT(nbt);
        else
            nbt.setString("Empty", "");
    }
    
    public int fill(FluidStack resource, boolean doFill)
    {
    	if (resource == null || resource.amount <= 0)
        {
            return 0;
        }
    	
    	if(doFill)
    	{
    		if(fluid == null || fluid.getFluid() == null)
    		{
    			fluid = resource.copy();
    			return resource.amount;
    		}
    	}
    	else
    	{
    		if(fluid == null || fluid.getFluid() == null)
    		{
    			return resource.amount;
    		}
    	}
    	
    	return 0;
    }
    
    public IFluidTankProperties[] getTankProperties()
    {
        if (tankProperties == null)
        {
            tankProperties = new IFluidTankProperties[] { new IFluidTankProperties()
            {
				
				@Override
				public FluidStack getContents()
				{
					return fluid;
				}
				
				@Override
				public int getCapacity()
				{
					return Integer.MAX_VALUE-1;
				}
				
				@Override
				public boolean canFillFluidType(FluidStack fluidStack)
				{
					return fluid != null ? fluid.isFluidEqual(fluidStack) : true;
				}
				
				@Override
				public boolean canFill()
				{
					return true;
				}
				
				@Override
				public boolean canDrainFluidType(FluidStack fluidStack)
				{
					return fluid != null ? fluid.isFluidEqual(fluidStack) : false;
				}
				
				@Override
				public boolean canDrain()
				{
					return true;
				}
			}};
        }
        return tankProperties;
    }
    
    @Nullable
    public FluidStack drain(FluidStack resource, boolean doDrain)
    {
    	if (resource == null || !resource.isFluidEqual(fluid))
        {
            return null;
        }
    	return drain(resource.amount, doDrain);
    }
    
    @Nullable
    public FluidStack drain(int maxDrain, boolean doDrain)
    {
    	if (fluid == null || maxDrain <= 0)
            return null;
    	
    	int drained = maxDrain;
        if (fluid.amount < drained)
        {
            drained = fluid.amount;
        }

        FluidStack stack = new FluidStack(fluid, drained);
        if (doDrain)
        {
            fluid.amount -= drained;
            if (fluid.amount <= 0)
            {
                fluid = null;
            }
        }
        return stack;
    }
    
    public int getCapacity()
    {
        return Integer.MAX_VALUE-1;
    }
	
}
