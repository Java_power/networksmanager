package javapower.netman.util;

import net.minecraft.block.Block;
import net.minecraft.util.math.BlockPos;

public interface INeighborChange
{
	public void observedNeighborChange(Block changedBlock, BlockPos changedBlockPos);
}
