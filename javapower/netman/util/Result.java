package javapower.netman.util;

public enum Result
{
	SUCCESSFUL,
	DECLINE,
	ERROR
}
