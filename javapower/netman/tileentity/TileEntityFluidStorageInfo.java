package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javapower.netman.block.BlockRFCounter;
import javapower.netman.core.NetworksManager;
import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineFluidStorageInfo;
import javapower.netman.nww.IModuleListener;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.ListenerUtils;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.FluidTankInfo;
import javapower.netman.util.IFluidTankInfo;
import javapower.netman.util.INeighborChange;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import javapower.netman.util.Var;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileEntityFluidStorageInfo extends TileEntitySynchronized implements IMachineFluidStorageInfo, IModuleListenerAcceptor, INeighborChange
{
	String custom_name = "";
	public EnumFacing face = null;
	
	boolean init = false;
	
	boolean update = true;
	int updateTank = -1;
	boolean updateAll = true;
	
	//int tankHashCode = -1;
	IFluidHandler tankfh = null;
	FluidTankInfo[] tanks = null;
	
	NBTTagCompound update_nbt = new NBTTagCompound();
	Var<Boolean> event_update = new Var<Boolean>(false);
	
	private List<IModuleListener> listeners = new ArrayList<IModuleListener>();
	
	@Override
	public void update()
	{
		
		if(!init && world != null && face != null)
		{
			observedNeighborChange(null, pos.offset(face.getOpposite()));
			world.notifyNeighborsOfStateChange(pos, blockType, true);
			init = true;
		}
		
		if(face == null && world != null)
		{
			face = world.getBlockState(pos).getValue(BlockRFCounter.PROPERTY_DIR);
		}
		
		if(tankfh != null)
		{
			IFluidTankProperties[] tankinfo = tankfh.getTankProperties();
			
			if(tankinfo != null && tankinfo.length > 0)
			{
				if(tanks == null || tanks.length != tankinfo.length)
				{
					tanks = new FluidTankInfo[tankinfo.length];
					
					for(int id = 0; id < tankinfo.length; ++id)
					{
						tanks[id] = new FluidTankInfo(tankinfo[id], update_nbt, event_update);
					}
				}
				
				if(tanks != null)
				{
					int id = 0;
					for(FluidTankInfo fti : tanks)
					{
						fti.update(id, tankinfo[id]);
						++id;
					}
				}
			}
		}
		
		if(!players.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				for(EntityPlayerMP pl : players)
				{
					if(pl != null)
					NetworkUtils.sendToPlayerTheData(this, update, pl);
				}
				
				if(listeners != null && listeners.size() > 0)
				{
					for(IModuleListener l : listeners)
					{
						l.listenerReciveInfo(this, update);
					}
				}
			}
		}
		else if(!listeners.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				try
				{
					for(IModuleListener l : listeners)
					{
						if(l != null)
						l.listenerReciveInfo(this, update);
					}
				}
				catch (ConcurrentModificationException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)
						e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void observedNeighborChange(Block changedBlock, BlockPos changedBlockPos)
	{
		EnumFacing blockface = Tools.getFacingForm2Blocks(pos, changedBlockPos);
		Block newblock = world.getBlockState(changedBlockPos).getBlock();
		if(face != null && blockface == face.getOpposite())
		{
			TileEntity teblock = world.getTileEntity(changedBlockPos);
			if(teblock != null && teblock.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face))
			{
				tankfh = teblock.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face);
			}
			else
			{
				//tankHashCode = -1;
				tankfh = null;
				tanks = null;
			}
		}
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		init = false;
		
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
	}
	
	@Override
	public NBTTagCompound write(NBTTagCompound compound)
	{
		compound.setString("cn", custom_name);
		return compound;
	}
	
	@Override
	public void reciveDataFromClient(NBTTagCompound nbt, EntityPlayer player)
	{
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
		}
	}

	@Override
	public void onPlayerOpenGUISendData(NBTTagCompound nbt, EntityPlayer player)
	{
		nbt.setString("cn", custom_name);
		if(tanks != null)
		{
			int id = 0;
			for(FluidTankInfo fti : tanks)
			{
				fti.forceUpdate(id);
				++id;
			}
		}
	}

	@Override
	public NBTTagCompound updateData()
	{
		if(update)
		{
			update = false;
			
			NBTTagCompound tag = new NBTTagCompound();
			tag.setString("cn", custom_name);
			return tag;
		}
		
		if(event_update.var)
		{
			event_update.var = false;
			NBTTagCompound tag = update_nbt.copy();
			update_nbt.getKeySet().clear();
			return tag;
		}
		
		return null;
	}

	@Override
	public TileEntity entity()
	{
		return this;
	}

	@Override
	public String customName()
	{
		return custom_name;
	}

	@Override
	public String name()
	{
		return "fluid-storageinfo";
	}
	
	@Override
	public EMachineType type()
	{
		return EMachineType.FLUID;
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}

	@Override
	public <T extends TileEntity & IModuleListener> void addListener(T module)
	{
		if(!ListenerUtils.ListenerIsOnList(listeners, module))
		{
			listeners.add(module);
		}
	}

	@Override
	public <T extends TileEntity & IModuleListener> void removeListener(T module)
	{
		ListenerUtils.RemoveListenerOnList(listeners, module);
	}

	@Override
	public <T extends TileEntity & IModuleListener> NBTTagCompound forceGetInfo(T module)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		
		nbt.setString("cn", custom_name);
		if(tanks != null)
		{
			int id = 0;
			for(FluidTankInfo fti : tanks)
			{
				fti.forceUpdateInThisNBT(id, nbt);
				++id;
			}
		}
		nbt.setInteger("tp", type().ordinal());
		return nbt;
	}

	@Override
	public <T extends TileEntity & IModuleListener> void reciveInfoFromListener(T module, NBTTagCompound data)
	{
		if(data.hasKey("cn"))
		{
			custom_name = data.getString("cn");
		}
	}

	@Override
	public void updateListener()
	{
		ListenerUtils.UpdateListenerAccess(listeners);
	}

	@Override
	public IFluidTankInfo[] GetFluidTankInfo()
	{
		return tanks;
	}

}
