package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javapower.netman.block.BlockFluidValve;
import javapower.netman.core.NetworksManager;
import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineValve;
import javapower.netman.nww.IModuleListener;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.ListenerUtils;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.FluidUtils;
import javapower.netman.util.INeighborChange;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileEntityFluidValve extends TileEntitySynchronized implements IMachineValve, IModuleListenerAcceptor, INeighborChange
{
	String custom_name = "";
	boolean valvePos = false;
	
	boolean update = true;
	EnumFacing face = null;
	
	IFluidHandler blockfluid = null;
	
	boolean init = false;
	
	private List<IModuleListener> listeners = new ArrayList<IModuleListener>();
	
	@Override
	public void reciveDataFromClient(NBTTagCompound nbt, EntityPlayer player)
	{
		if(nbt.hasKey("vp"))
		{
			valvePos = nbt.getBoolean("vp");
			update = true;
		}
		
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
	}

	@Override
	public void onPlayerOpenGUISendData(NBTTagCompound nbt, EntityPlayer player)
	{
		nbt.setBoolean("vp", valvePos);
		nbt.setString("cn", custom_name);
	}

	@Override
	public NBTTagCompound updateData()
	{
		if(update)
		{
			update = false;
			NBTTagCompound tag = new NBTTagCompound();
			tag.setBoolean("vp", valvePos);
			tag.setString("cn", custom_name);
			return tag;
		}
		return null;
	}
	
	@Override
	public void update()
	{
		if(!init && world != null && face != null)
		{
			observedNeighborChange(null, pos.offset(face.getOpposite()));
			world.notifyNeighborsOfStateChange(pos, blockType, true);
			init = true;
		}
		
		if(face == null && world != null)
		{
			face = world.getBlockState(pos).getValue(BlockFluidValve.PROPERTY_DIR);
		}
		
		if(!players.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				for(EntityPlayerMP pl : players)
				{
					if(pl != null)
					NetworkUtils.sendToPlayerTheData(this, update, pl);
				}
				
				if(listeners != null && listeners.size() > 0)
				{
					for(IModuleListener l : listeners)
					{
						l.listenerReciveInfo(this, update);
					}
				}
			}
		}
		else if(!listeners.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				try
				{
					for(IModuleListener l : listeners)
					{
						if(l != null)
						l.listenerReciveInfo(this, update);
					}
				}
				catch (ConcurrentModificationException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)
						e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		if(nbt.hasKey("vp"))
		{
			valvePos = nbt.getBoolean("vp");
			update = true;
		}
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
	}
	
	@Override
	public NBTTagCompound write(NBTTagCompound compound)
	{
		compound.setBoolean("vp", valvePos);
		compound.setString("cn", custom_name);
		
		return compound;
	}
	
	@Override
	public TileEntity entity()
	{
		return this;
	}

	@Override
	public String customName()
	{
		return custom_name;
	}

	@Override
	public String name()
	{
		return "fluid-valve";
	}
	
	@Override
	public EMachineType type()
	{
		return EMachineType.FLUID;
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}

	@Override
	public void ValveOnOff(boolean onOff)
	{
		valvePos = onOff;
		update = true;
		markDirty();
	}

	@Override
	public boolean getValvePos()
	{
		return valvePos;
	}
	
	@Override
	public void observedNeighborChange(Block changedBlock, BlockPos changedBlockPos)
	{
		EnumFacing blockface = Tools.getFacingForm2Blocks(pos, changedBlockPos);
		Block newblock = world.getBlockState(changedBlockPos).getBlock();
		if(face != null && blockface == face.getOpposite())
		{
			TileEntity teblock = world.getTileEntity(changedBlockPos);
			if(teblock != null && teblock.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face))
			{
				blockfluid = teblock.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face);
			}
			else
			{
				blockfluid = null;
			}
		}
		
		markDirty();
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			if(facing == face)
				return (T) this_in;
			if(facing == face.getOpposite())
				return (T) this_out;
		}
		
		return super.getCapability(capability, facing);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(face == null)
			face = world.getBlockState(pos).getValue(BlockFluidValve.PROPERTY_DIR);
		
		return (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && (facing == face || facing == face.getOpposite()))
				|| super.hasCapability(capability, facing);
	}
	
	public FluidInput this_in = new FluidInput();
	class FluidInput implements IFluidHandler
	{

		@Override
		public IFluidTankProperties[] getTankProperties()
		{
			if(blockfluid != null)
				return blockfluid.getTankProperties();
			return null;
		}

		@Override
		public int fill(FluidStack resource, boolean doFill)
		{
			if(valvePos && blockfluid != null)
				return blockfluid.fill(resource, doFill);
			return 0;
		}

		@Override
		public FluidStack drain(FluidStack resource, boolean doDrain)
		{
			return null;
		}

		@Override
		public FluidStack drain(int maxDrain, boolean doDrain)
		{
			return null;
		}
		
	}
	
	public FluidOutput this_out = new FluidOutput();
	class FluidOutput implements IFluidHandler
	{

		@Override
		public IFluidTankProperties[] getTankProperties()
		{
			return FluidUtils.getTankPropertiesDrain();
		}

		@Override
		public int fill(FluidStack resource, boolean doFill)
		{
			return 0;
		}

		@Override
		public FluidStack drain(FluidStack resource, boolean doDrain)
		{
			return null;
		}

		@Override
		public FluidStack drain(int maxDrain, boolean doDrain)
		{
			return null;
		}
		
	}
	
	@Override
	public <T extends TileEntity & IModuleListener> void addListener(T module)
	{
		if(!ListenerUtils.ListenerIsOnList(listeners, module))
		{
			listeners.add(module);
		}
	}

	@Override
	public <T extends TileEntity & IModuleListener> void removeListener(T module)
	{
		ListenerUtils.RemoveListenerOnList(listeners, module);
	}

	@Override
	public <T extends TileEntity & IModuleListener> NBTTagCompound forceGetInfo(T module)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("vp", valvePos);
		nbt.setString("cn", custom_name);
		nbt.setInteger("tp", type().ordinal());
		return nbt;
	}

	@Override
	public <T extends TileEntity & IModuleListener> void reciveInfoFromListener(T module, NBTTagCompound data)
	{
		if(data == null)
			return;
		
		if(data.hasKey("vp"))
		{
			valvePos = data.getBoolean("vp");
			update = true;
		}
		
		if(data.hasKey("cn"))
		{
			custom_name = data.getString("cn");
			update = true;
		}
	}
	
	@Override
	public void updateListener()
	{
		ListenerUtils.UpdateListenerAccess(listeners);
	}

}
