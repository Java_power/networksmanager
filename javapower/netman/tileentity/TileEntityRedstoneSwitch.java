package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javapower.netman.core.NetworksManager;
import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineSwitch;
import javapower.netman.nww.IModuleListener;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.ListenerUtils;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.IRedstoneEvent;
import javapower.netman.util.NetworkUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;

public class TileEntityRedstoneSwitch extends TileEntitySynchronized implements IMachineSwitch, IModuleListenerAcceptor, IRedstoneEvent
{
	
	private List<IModuleListener> listeners = new ArrayList<IModuleListener>();
	public boolean update_rs = false;
	public boolean update = true;
	
	public String custom_name = "";
	public boolean redstone_signal = false;
	
	@Override
	public TileEntity entity()
	{
		return this;
	}

	@Override
	public String customName()
	{
		return custom_name;
	}

	@Override
	public EMachineType type()
	{
		return EMachineType.REDSTONE;
	}

	@Override
	public String name()
	{
		return "rsswitch";
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		if(nbt.hasKey("sw"))
		{
			redstone_signal = nbt.getBoolean("sw");
			update = true;
			update_rs = true;
		}
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
	}
	
	@Override
	public NBTTagCompound write(NBTTagCompound compound)
	{
		compound.setBoolean("sw", redstone_signal);
		compound.setString("cn", custom_name);
		return compound;
	}

	@Override
	public void reciveDataFromClient(NBTTagCompound nbt, EntityPlayer player)
	{
		if(nbt.hasKey("sw"))
		{
			redstone_signal = nbt.getBoolean("sw");
			update = true;
			update_rs = true;
		}
		
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
	}

	@Override
	public void onPlayerOpenGUISendData(NBTTagCompound nbt, EntityPlayer player)
	{
		nbt.setBoolean("sw", redstone_signal);
		nbt.setString("cn", custom_name);
	}

	@Override
	public NBTTagCompound updateData()
	{
		if(update)
		{
			update = false;
			markDirty();
			NBTTagCompound tag = new NBTTagCompound();
			tag.setBoolean("sw", redstone_signal);
			tag.setString("cn", custom_name);
			return tag;
		}
		return null;
	}
	
	@Override
	public void update()
	{
		if(update_rs)
		{
			world.notifyNeighborsOfStateChange(pos, blockType, false);
			update_rs = false;
		}
		
		if(!players.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				for(EntityPlayerMP pl : players)
				{
					if(pl != null)
					NetworkUtils.sendToPlayerTheData(this, update, pl);
				}
				
				if(listeners != null && listeners.size() > 0)
				{
					for(IModuleListener l : listeners)
					{
						l.listenerReciveInfo(this, update);
					}
				}
			}
		}
		else if(!listeners.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				try
				{
					for(IModuleListener l : listeners)
					{
						if(l != null)
						l.listenerReciveInfo(this, update);
					}
				}
				catch (ConcurrentModificationException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)
						e.printStackTrace();
				}
			}
		}
	}

	@Override
	public <T extends TileEntity & IModuleListener> void addListener(T module)
	{
		if(!ListenerUtils.ListenerIsOnList(listeners, module))
		{
			listeners.add(module);
		}
	}

	@Override
	public <T extends TileEntity & IModuleListener> void removeListener(T module)
	{
		ListenerUtils.RemoveListenerOnList(listeners, module);
	}

	@Override
	public <T extends TileEntity & IModuleListener> NBTTagCompound forceGetInfo(T module)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("sw", redstone_signal);
		nbt.setString("cn", custom_name);
		nbt.setInteger("tp", type().ordinal());
		return nbt;
	}

	@Override
	public <T extends TileEntity & IModuleListener> void reciveInfoFromListener(T module, NBTTagCompound data)
	{
		if(data.hasKey("sw"))
		{
			redstone_signal = data.getBoolean("sw");
			update = true;
			update_rs = true;
		}
		
		if(data.hasKey("cn"))
		{
			custom_name = data.getString("cn");
			update = true;
		}
	}
	
	@Override
	public void updateListener()
	{
		ListenerUtils.UpdateListenerAccess(listeners);
	}

	@Override
	public void switchOnOff(boolean onOff)
	{
		redstone_signal = onOff;
		update_rs = true;
	}

	@Override
	public boolean getSwitchPos()
	{
		return redstone_signal;
	}

	@Override
	public void redstoneEvent(int redstonePower, EnumFacing face){}

	@Override
	public int getRedstoneOutputSinal(EnumFacing face)
	{
		return redstone_signal ? 15 : 0;
	}

}
