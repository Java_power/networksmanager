package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javapower.netman.block.BlockRFCounter;
import javapower.netman.core.NetworksManager;
import javapower.netman.eventio.IEventOut;
import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineEnergyStorageInfo;
import javapower.netman.nww.IModuleListener;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.ListenerUtils;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.DifferentialEvent;
import javapower.netman.util.INeighborChange;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TileEntityRFEnergyInfo extends TileEntitySynchronized implements IMachineEnergyStorageInfo, IModuleListenerAcceptor, INeighborChange
{
	String custom_name = "";
	public EnumFacing face = null;
	
	IEnergyStorage blockenergy = null;
	
	boolean init = false;
	
	DifferentialEvent<Integer> energy = new DifferentialEvent<>(new IEventOut<Integer>()
	{
		@Override
		public void event(Integer out)
		{
			updateValue = true;
		}
	});
	
	DifferentialEvent<Integer> energyMax = new DifferentialEvent<>(new IEventOut<Integer>()
	{
		@Override
		public void event(Integer out)
		{
			updateMaxValue = true;
		}
	});
	
	//boolean updateWorld = false;
	
	boolean update = true;
	boolean updateValue = true;
	boolean updateMaxValue = true;
	
	private List<IModuleListener> listeners = new ArrayList<IModuleListener>();
	
	@Override
	public void update()
	{
		if(!init && world != null && face != null)
		{
			observedNeighborChange(null, pos.offset(face.getOpposite()));
			init = true;
		}
		
		if(face == null && world != null)
		{
			face = world.getBlockState(pos).getValue(BlockRFCounter.PROPERTY_DIR);
		}
		
		/*if(updateWorld && world != null)
		{
			if(!world.isRemote)
			{
				BlockPos poslook = pos.add(face.getOpposite().getDirectionVec());
				IBlockState statelook = world.getBlockState(poslook);
				observedNeighborChange(statelook.getBlock(), poslook);
			}
			
			updateWorld = false;
		}*/
		
		if(blockenergy != null)
		{
			energy.SetValue(blockenergy.getEnergyStored());
			energyMax.SetValue(blockenergy.getMaxEnergyStored());
		}
		
		//super.update();
		if(!players.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				for(EntityPlayerMP pl : players)
				{
					if(pl != null)
					NetworkUtils.sendToPlayerTheData(this, update, pl);
				}
				
				if(listeners != null && listeners.size() > 0)
				{
					for(IModuleListener l : listeners)
					{
						l.listenerReciveInfo(this, update);
					}
				}
			}
		}
		else if(!listeners.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				try
				{
					for(IModuleListener l : listeners)
					{
						if(l != null)
						l.listenerReciveInfo(this, update);
					}
				}
				catch (ConcurrentModificationException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)
						e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void observedNeighborChange(Block changedBlock, BlockPos changedBlockPos)
	{
		EnumFacing blockface = Tools.getFacingForm2Blocks(pos, changedBlockPos);
		Block newblock = world.getBlockState(changedBlockPos).getBlock();
		if(blockface == face.getOpposite())
		{
			TileEntity teblock = world.getTileEntity(changedBlockPos);
			if(teblock != null && teblock.hasCapability(CapabilityEnergy.ENERGY, face))
			{
				blockenergy = teblock.getCapability(CapabilityEnergy.ENERGY, face);
			}
			else
			{
				blockenergy = null;
			}
		}
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		//updateWorld = true;
		init = false;
		
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
	}
	
	@Override
	public NBTTagCompound write(NBTTagCompound compound)
	{
		compound.setString("cn", custom_name);
		return compound;
	}
	
	@Override
	public void reciveDataFromClient(NBTTagCompound nbt, EntityPlayer player)
	{
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
		}
	}

	@Override
	public void onPlayerOpenGUISendData(NBTTagCompound nbt, EntityPlayer player)
	{
		nbt.setString("cn", custom_name);
		if(energy != null && energy.GetValue() != null)
			nbt.setInteger("ev", energy.GetValue());
		if(energyMax != null && energyMax.GetValue() != null)
			nbt.setInteger("em", energyMax.GetValue());
	}

	@Override
	public NBTTagCompound updateData()
	{
		if(update)
		{
			update = false;
			updateValue = false;
			updateMaxValue = false;
			
			NBTTagCompound tag = new NBTTagCompound();
			tag.setString("cn", custom_name);
			if(energy != null && energy.GetValue() != null)
				tag.setInteger("ev", energy.GetValue());
			if(energyMax != null && energyMax.GetValue() != null)
				tag.setInteger("em", energyMax.GetValue());
			return tag;
		}
		
		if(updateValue)
		{
			updateValue = false;
			
			NBTTagCompound tag = new NBTTagCompound();
			if(energy != null && energy.GetValue() != null)
				tag.setInteger("ev", energy.GetValue());
			return tag;
		}
		
		if(updateMaxValue)
		{
			updateMaxValue = false;
			
			NBTTagCompound tag = new NBTTagCompound();
			if(energyMax != null && energyMax.GetValue() != null)
				tag.setInteger("em", energyMax.GetValue());
			return tag;
		}
		
		
		return null;
	}

	@Override
	public TileEntity entity()
	{
		return this;
	}

	@Override
	public String customName()
	{
		return custom_name;
	}

	@Override
	public String name()
	{
		return "rf-energyinfo";
	}
	
	@Override
	public EMachineType type()
	{
		return EMachineType.ENERGY_RF;
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}

	@Override
	public int GetEnergyStored()
	{
		if(energy != null && energy.GetValue() != null)
			return energy.GetValue();
		return 0;
	}

	@Override
	public int GetMaxEnergyStored()
	{
		if(energyMax != null && energyMax.GetValue() != null)
			return energyMax.GetValue();
		return 0;
	}

	@Override
	public <T extends TileEntity & IModuleListener> void addListener(T module)
	{
		if(!ListenerUtils.ListenerIsOnList(listeners, module))
		{
			listeners.add(module);
		}
	}

	@Override
	public <T extends TileEntity & IModuleListener> void removeListener(T module)
	{
		ListenerUtils.RemoveListenerOnList(listeners, module);
	}

	@Override
	public <T extends TileEntity & IModuleListener> NBTTagCompound forceGetInfo(T module)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		
		nbt.setString("cn", custom_name);
		if(energy != null && energy.GetValue() != null)
			nbt.setInteger("ev", energy.GetValue());
		if(energyMax != null && energyMax.GetValue() != null)
			nbt.setInteger("em", energyMax.GetValue());
		nbt.setInteger("tp", type().ordinal());
		return nbt;
	}

	@Override
	public <T extends TileEntity & IModuleListener> void reciveInfoFromListener(T module, NBTTagCompound data)
	{
		if(data.hasKey("cn"))
		{
			custom_name = data.getString("cn");
		}
	}

	@Override
	public void updateListener()
	{
		ListenerUtils.UpdateListenerAccess(listeners);
	}

}
