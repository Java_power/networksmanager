package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javapower.netman.block.BlockFluidValve;
import javapower.netman.block.BlockRFCounter;
import javapower.netman.core.NetworksManager;
import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineFluidFlowMeter;
import javapower.netman.nww.IModuleListener;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.ListenerUtils;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.FluidUtils;
import javapower.netman.util.INeighborChange;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileEntityFluidFlowMeter extends TileEntitySynchronized implements IMachineFluidFlowMeter, IModuleListenerAcceptor, INeighborChange
{
	String custom_name = "";
	
	int[] flow = null;
	int flowmoy = 0;
	int flowIndex = 0;
	int flowBufferSize = 8;
	int flowTimeOut = 0;
	
	IFluidHandler blockfluid = null;
	
	boolean init = false;
	
	EnumFacing face = null;
	boolean update = true;
	boolean updateF = true;
	
	private List<IModuleListener> listeners = new ArrayList<IModuleListener>();
	
	//---------- NBT & update ----------
	
	@Override
	public void reciveDataFromClient(NBTTagCompound nbt, EntityPlayer player)
	{
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
		
		if(nbt.hasKey("fs"))
		{
			flowBufferSize = nbt.getInteger("fs");
			
			flow = new int[flowBufferSize];
			
			update = true;
		}
	}

	@Override
	public void onPlayerOpenGUISendData(NBTTagCompound nbt, EntityPlayer player)
	{
		nbt.setInteger("fv", Tools.moyInteger(flow));
		nbt.setInteger("fs", flowBufferSize);
		
		nbt.setString("cn", custom_name);
	}

	@Override
	public NBTTagCompound updateData()
	{
		if(update)
		{
			update = false;
			updateF = false;
			NBTTagCompound tag = new NBTTagCompound();
			tag.setString("cn", custom_name);
			tag.setInteger("fv", Tools.moyInteger(flow));
			tag.setInteger("fs", flowBufferSize);
			return tag;
		}
		
		if(updateF)
		{
			updateF = false;
			
			NBTTagCompound tag = new NBTTagCompound();
			tag.setInteger("fv", Tools.moyInteger(flow));
			return tag;
		}
		return null;
	}
	
	@Override
	public NBTTagCompound write(NBTTagCompound compound)
	{
		compound.setInteger("fv", Tools.moyInteger(flow));
		compound.setInteger("fs", flowBufferSize);
		
		compound.setString("cn", custom_name);
		
		return super.write(compound);
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
		
		if(nbt.hasKey("fs"))
		{
			flowBufferSize = nbt.getInteger("fs");
			if(flowBufferSize <= 0)
			{
				flowBufferSize = 1;
				UpdateCkunk();
			}
			flow = new int[flowBufferSize];
			update = true;
		}
		
		if(nbt.hasKey("fv"))
		{
			int val = nbt.getInteger("fv");
			for(int i = 0; i < flow.length; ++i)
			{
				flow[i] = val;
			}
			
			update = true;
		}
		
		super.read(nbt);
	}
	
	// ---------- ----- ----- ----------
	
	@Override
	public TileEntity entity()
	{
		return this;
	}

	@Override
	public String customName()
	{
		return custom_name;
	}

	@Override
	public String name()
	{
		return "fluid-flowmeter";
	}
	
	@Override
	public EMachineType type()
	{
		return EMachineType.FLUID;
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}
	
	@Override
	public int GetFlow()
	{
		return Tools.moyInteger(flow);
	}
	
	@Override
	public void update()
	{
		if(!init && world != null && face != null)
		{
			observedNeighborChange(null, pos.offset(face.getOpposite()));
			world.notifyNeighborsOfStateChange(pos, blockType, true);
			init = true;
		}
		
		if(face == null && world != null)
		{
			face = world.getBlockState(pos).getValue(BlockRFCounter.PROPERTY_DIR);
			//world.notifyNeighborsOfStateChange(pos, blockType, true);
		}
		
		if(world != null && !world.isRemote)
		{
			if(flow != null && flowTimeOut > 10)
			{
				flowTimeOut -= 2;
				if(flow.length > flowIndex + 1)
				{
					++flowIndex;
					flow[flowIndex] = 0;
				}
				else
				{
					flowIndex = 0;
					flow[0] = 0;
				}
				UpdateCkunk();
				
				//updateF = true;
				int fluxlocalmoy = Tools.moyInteger(flow);
				updateF = flowmoy != fluxlocalmoy;
				flowmoy = fluxlocalmoy;
			}
			else
			{
				++flowTimeOut;
			}
		}
		
		//super.update();
		if(!players.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				for(EntityPlayerMP pl : players)
				{
					if(pl != null)
					NetworkUtils.sendToPlayerTheData(this, update, pl);
				}
				
				if(listeners != null && listeners.size() > 0)
				{
					for(IModuleListener l : listeners)
					{
						l.listenerReciveInfo(this, update);
					}
				}
			}
		}
		else if(!listeners.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				try
				{
					for(IModuleListener l : listeners)
					{
						if(l != null)
						l.listenerReciveInfo(this, update);
					}
				}
				catch (ConcurrentModificationException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)
						e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void observedNeighborChange(Block changedBlock, BlockPos changedBlockPos)
	{
		EnumFacing blockface = Tools.getFacingForm2Blocks(pos, changedBlockPos);
		Block newblock = world.getBlockState(changedBlockPos).getBlock();
		if(face != null && blockface == face.getOpposite())
		{
			TileEntity teblock = world.getTileEntity(changedBlockPos);
			if(teblock != null && teblock.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face))
			{
				blockfluid = teblock.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face);
			}
			else
			{
				blockfluid = null;
			}
		}
		
		markDirty();
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			if(facing == face)
				return (T) this_in;
			if(facing == face.getOpposite())
				return (T) this_out;
		}
		
		return super.getCapability(capability, facing);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(face == null)
			face = world.getBlockState(pos).getValue(BlockFluidValve.PROPERTY_DIR);
		return (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && (facing == face || facing == face.getOpposite()))
				|| super.hasCapability(capability, facing);
	}
	
	
	public FluidInput this_in = new FluidInput();
	class FluidInput implements IFluidHandler
	{

		@Override
		public IFluidTankProperties[] getTankProperties()
		{
			if(blockfluid != null)
				return blockfluid.getTankProperties();
			return null;
		}

		@Override
		public int fill(FluidStack resource, boolean doFill)
		{
			if(blockfluid != null)
			{
				int transfert = blockfluid.fill(resource, doFill);
				
				if(flow == null)
					flow = new int[flowBufferSize];
				
				if(doFill)
				{
					if(flow.length > flowIndex + 1)
					{
						++flowIndex;
						flow[flowIndex] = transfert;
					}
					else
					{
						flowIndex = 0;
						flow[0] = transfert;
					}
					flowTimeOut = 0;
					
					int fluxlocalmoy = Tools.moyInteger(flow);
					updateF = flowmoy != fluxlocalmoy;
					flowmoy = fluxlocalmoy;
					UpdateCkunk();
				}
				return transfert;
			}
			return 0;
		}

		@Override
		public FluidStack drain(FluidStack resource, boolean doDrain)
		{
			return null;
		}

		@Override
		public FluidStack drain(int maxDrain, boolean doDrain)
		{
			return null;
		}
		
	}
	
	public FluidOutput this_out = new FluidOutput();
	class FluidOutput implements IFluidHandler
	{

		@Override
		public IFluidTankProperties[] getTankProperties()
		{
			return FluidUtils.getTankPropertiesDrain();
		}

		@Override
		public int fill(FluidStack resource, boolean doFill)
		{
			return 0;
		}

		@Override
		public FluidStack drain(FluidStack resource, boolean doDrain)
		{
			return null;
		}

		@Override
		public FluidStack drain(int maxDrain, boolean doDrain)
		{
			return null;
		}
		
	}
	
	@Override
	public <T extends TileEntity & IModuleListener> void addListener(T module)
	{
		if(!ListenerUtils.ListenerIsOnList(listeners, module))
		{
			listeners.add(module);
		}
	}

	@Override
	public <T extends TileEntity & IModuleListener> void removeListener(T module)
	{
		ListenerUtils.RemoveListenerOnList(listeners, module);
	}

	@Override
	public <T extends TileEntity & IModuleListener> NBTTagCompound forceGetInfo(T module)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		
		nbt.setInteger("fv", Tools.moyInteger(flow));
		nbt.setInteger("fs", flowBufferSize);
		nbt.setString("cn", custom_name);
		nbt.setInteger("tp", type().ordinal());
		return nbt;
	}

	@Override
	public <T extends TileEntity & IModuleListener> void reciveInfoFromListener(T module, NBTTagCompound data)
	{
		if(data.hasKey("cn"))
		{
			custom_name = data.getString("cn");
			update = true;
		}
		
		if(data.hasKey("fs"))
		{
			flowBufferSize = data.getInteger("fs");
			
			flow = new int[flowBufferSize];
			
			update = true;
		}
	}

	@Override
	public void updateListener()
	{
		ListenerUtils.UpdateListenerAccess(listeners);
	}

}
