package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMEventUpdate;
import javapower.netman.nww.IMachineNetwork;
import javapower.netman.nww.IModuleListener;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.ListenerUtils;
import javapower.netman.nww.MachineUtil;
import javapower.netman.util.BlockPosDim;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityTerminal extends TileEntitySynchronized implements IMachineNetwork, IModuleListener, IMEventUpdate
{
	HashMap<IMachineNetwork, NBTTagCompound> infos = new HashMap<IMachineNetwork, NBTTagCompound>();
	List<IMachineNetwork> update_info = new ArrayList<IMachineNetwork>();
	
	List<IMachineNetwork> machines = null;
	boolean forceUpdateInfo = false;
	
	// ---------- Network ----------
	
	 @Override
	public void onEventListMachinesUpdate(List<IMachineNetwork> _machines)
	{
		 machines = _machines;
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}

	@Override
	public TileEntity entity()
	{
		return this;
	}

	@Override
	public String customName()
	{
		return "";
	}

	@Override
	public String name()
	{
		return "terminal";
	}
	
	@Override
	public EMachineType type()
	{
		return EMachineType.MACHINE;
	}

	@Override
	public void reciveDataFromClient(NBTTagCompound nbt, EntityPlayer player)
	{
		if(nbt.hasKey("fgi"))
		{
			forceUpdateInfo = true;
		}
		
		if(nbt.hasKey("uls"))
		{
			if(machines != null)
				for(IMachineNetwork m : machines)
					ListenerUtils.addThisMachine(this, m.entity());
		}
	}

	@Override
	public void onPlayerOpenGUISendData(NBTTagCompound nbt, EntityPlayer player)
	{
		if(machines != null)
			for(IMachineNetwork m : machines)
				ListenerUtils.addThisMachine(this, m.entity());
	}

	@Override
	public NBTTagCompound updateData()
	{
		if(forceUpdateInfo)
		{
			forceUpdateInfo = false;
			NBTTagCompound nbt_final = new NBTTagCompound();
			int id = 0;
			if(machines != null)
				for(IMachineNetwork m: machines)
				{
					if(m instanceof IModuleListenerAcceptor)
					{
						NBTTagCompound nbt_machine = ((IModuleListenerAcceptor)m).forceGetInfo(this);
						if(nbt_machine == null)
							nbt_machine = new NBTTagCompound();
						
						m.thispos().WriteToNBT(nbt_machine, "mwp");
						nbt_machine.setString("mnm", m.name());
						nbt_machine.setString("class", MachineUtil.getMachineClassName(m));
						
						nbt_final.setTag("mi"+id, nbt_machine);
						++id;
					}
				}
			
			return nbt_final;
		}
		
		if(!update_info.isEmpty())
		{
			NBTTagCompound nbt_final = new NBTTagCompound();
			
			int id = 0;
			for(IMachineNetwork m : update_info)
			{
				NBTTagCompound nbt_machine = infos.get(m);
				if(nbt_machine == null)
					nbt_machine = new NBTTagCompound();
				
					m.thispos().WriteToNBT(nbt_machine, "mwp");
				
				nbt_final.setTag("mi"+id, nbt_machine);
				++id;
			}
			
			update_info.clear();
			
			return nbt_final;
		}
		
		return null;
	}
	
	//server side
	@Override
	public void listenerReciveInfo(IMachineNetwork from, NBTTagCompound data)
	{
		if(players.isEmpty())
		{
			if(machines != null)
				for(IMachineNetwork m : machines)
					ListenerUtils.removeThisMachine(this, m.entity());
			
			infos.clear();
			update_info.clear();
		}
		else
		{
			if(from == null)
				return;
			
			if(data == null)
			{
				infos.remove(from);
			}
			else
			{
				infos.put(from, data);
			}
			
			for(IMachineNetwork m: update_info)
				if(m.equals(from))
					return;
			
			update_info.add(from);
		}
	}

}
