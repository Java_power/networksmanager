package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import javapower.netman.block.BlockRFCounter;
import javapower.netman.core.NetworksManager;
import javapower.netman.nww.EMachineType;
import javapower.netman.nww.IMachineCounter;
import javapower.netman.nww.IModuleListener;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.ListenerUtils;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.INeighborChange;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TileEntityRFCounter extends TileEntitySynchronized implements IMachineCounter, IModuleListenerAcceptor, INeighborChange
{
	String custom_name = "";
	long consomation = 0l;
	//boolean autoextract = false;
	
	boolean update = true;
	
	boolean updateCM = true;
	String updateCM_String = "";
	
	EnumFacing face = null;
	
	IEnergyStorage blockenergy = null;
	
	boolean init = false;
	
	private List<IModuleListener> listeners = new ArrayList<IModuleListener>();
	
	@Override
	public void reciveDataFromClient(NBTTagCompound nbt, EntityPlayer player)
	{
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
		
		if(nbt.hasKey("cm"))
		{
			consomation = nbt.getLong("cm");
			update = true;
		}
		
		/*if(nbt.hasKey("ae"))
		{
			autoextract = nbt.getBoolean("ae");
			update = true;
		}*/
		
		if(nbt.hasKey("z0"))
		{
			consomation = 0;
			update = true;
		}
	}

	@Override
	public void onPlayerOpenGUISendData(NBTTagCompound nbt, EntityPlayer player)
	{
		nbt.setString("cn", custom_name);
		nbt.setLong("cm", consomation);
		//nbt.setBoolean("ae", autoextract);
	}

	@Override
	public NBTTagCompound updateData()
	{
		if(update)
		{
			update = false;
			updateCM = false;
			NBTTagCompound tag = new NBTTagCompound();
			tag.setString("cn", custom_name);
			tag.setLong("cm", consomation);
			//tag.setBoolean("ae", autoextract);
			return tag;
		}
		
		if(updateCM)
		{
			updateCM = false;
			
			NBTTagCompound tag = new NBTTagCompound();
			tag.setLong("cm", consomation);
			return tag;
		}
		return null;
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		if(nbt.hasKey("cm"))
		{
			consomation = nbt.getLong("cm");
			update = true;
		}
		if(nbt.hasKey("cn"))
		{
			custom_name = nbt.getString("cn");
			update = true;
		}
		/*if(nbt.hasKey("ae"))
		{
			autoextract = nbt.getBoolean("ae");
			update = true;
		}*/
		/*if(nbt.hasKey("gy"))
		{
			energy = nbt.getInteger("gy");
		}*/
	}
	
	@Override
	public NBTTagCompound write(NBTTagCompound compound)
	{
		compound.setLong("cm", consomation);
		compound.setString("cn", custom_name);
		//compound.setInteger("gy", energy);
		//compound.setBoolean("ae", autoextract);
		return compound;
	}

	@Override
	public TileEntity entity()
	{
		return this;
	}

	@Override
	public String customName()
	{
		return custom_name;
	}

	@Override
	public String name()
	{
		return "rf-counter";
	}
	
	@Override
	public EMachineType type()
	{
		return EMachineType.ENERGY_RF;
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}

	@Override
	public long GetConsomation()
	{
		return consomation;
	}

	@Override
	public void Reset()
	{
		consomation = 0;
		UpdateCkunk();
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(face == null)
			face = world.getBlockState(pos).getValue(BlockRFCounter.PROPERTY_DIR);
		
		return (capability == CapabilityEnergy.ENERGY && (facing == face || facing == face.getOpposite()))
				|| super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
		{
			if(facing == face)
				return (T) this_in;
			if(facing == face.getOpposite())
				return (T) this_out;
		}
		
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void update()
	{
		if(!init && world != null && face != null)
		{
			observedNeighborChange(null, pos.offset(face.getOpposite()));
			world.notifyNeighborsOfStateChange(pos, blockType, true);
			init = true;
		}
		
		if(face == null && world != null)
		{
			face = world.getBlockState(pos).getValue(BlockRFCounter.PROPERTY_DIR);
		}
		
		/*if(autoextract)
		{
			EnergyUtils.emit(this, this_out, face.getOpposite());
		}*/
		
		//super.update();
		if(!players.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				for(EntityPlayerMP pl : players)
				{
					if(pl != null)
					NetworkUtils.sendToPlayerTheData(this, update, pl);
				}
				
				if(listeners != null && listeners.size() > 0)
				{
					for(IModuleListener l : listeners)
					{
						l.listenerReciveInfo(this, update);
					}
				}
			}
		}
		else if(!listeners.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				try
				{
					for(IModuleListener l : listeners)
					{
						if(l != null)
						l.listenerReciveInfo(this, update);
					}
				}
				catch (ConcurrentModificationException e)
				{
					if(NetworksManager.PrintStackTrace_TileEntity)
						e.printStackTrace();
				}
			}
		}
	}
	
	//int energy = 0;
	
	public RFInput this_in = new RFInput();
	class RFInput implements IEnergyStorage
	{

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate)
		{
			/*if(energy <= 0)
			{
				if(!simulate)
				{
					energy += maxReceive;
					UpdateCkunk();
				}
				return maxReceive;
			}*/
			if(blockenergy != null)
			{
				int transfert = blockenergy.receiveEnergy(maxReceive, simulate);
				
				if(!simulate && transfert > 0)
				{
					consomation += transfert;
					
					String conso = Tools.longFormatToString(consomation);
					updateCM = !updateCM_String.equals(conso);
					updateCM_String = conso;
					
					UpdateCkunk();
				}
				
				return transfert;
			}
			return 0;
		}

		@Override
		public int extractEnergy(int maxExtract, boolean simulate){return 0;}

		@Override
		public int getEnergyStored()
		{
			if(blockenergy != null)
				return blockenergy.getEnergyStored();
			return 0;
		}

		@Override
		public int getMaxEnergyStored()
		{
			if(blockenergy != null)
				return blockenergy.getMaxEnergyStored();
			return 0;
		}

		@Override
		public boolean canExtract()
		{
			return false;
		}

		@Override
		public boolean canReceive()
		{
			return true;
		}
		
	}
	
	public RFOutput this_out = new RFOutput();
	class RFOutput implements IEnergyStorage
	{

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate){return 0;}

		@Override
		public int extractEnergy(int maxExtract, boolean simulate)
		{
			/*if(energy > 0)
			{
				if(energy <= maxExtract)
				{
					int e = energy;
					if(!simulate)
						energy = 0;
					
					consomation += e;
					
					String conso = Tools.longFormatToString(consomation);
					updateCM = !updateCM_String.equals(conso);
					updateCM_String = conso;
					UpdateCkunk();
					
					return e;
				}
				else
				{
					if(!simulate)
						energy -= maxExtract;
					
					consomation += maxExtract;
					
					String conso = Tools.longFormatToString(consomation);
					updateCM = !updateCM_String.equals(conso);
					updateCM_String = conso;
					
					UpdateCkunk();
					
					return maxExtract;
				}
			}*/
			return 0;
		}

		@Override
		public int getEnergyStored()
		{
			return 0;
		}

		@Override
		public int getMaxEnergyStored()
		{
			return Integer.MAX_VALUE-1;
		}

		@Override
		public boolean canExtract()
		{
			return true;
		}

		@Override
		public boolean canReceive()
		{
			return false;
		}
		
	}
	
	@Override
	public <T extends TileEntity & IModuleListener> void addListener(T module)
	{
		if(!ListenerUtils.ListenerIsOnList(listeners, module))
		{
			listeners.add(module);
		}
	}

	@Override
	public <T extends TileEntity & IModuleListener> void removeListener(T module)
	{
		ListenerUtils.RemoveListenerOnList(listeners, module);
	}

	@Override
	public <T extends TileEntity & IModuleListener> NBTTagCompound forceGetInfo(T module)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		
		nbt.setString("cn", custom_name);
		nbt.setLong("cm", consomation);
		nbt.setInteger("tp", type().ordinal());
		//nbt.setBoolean("ae", autoextract);
		
		return nbt;
	}

	@Override
	public <T extends TileEntity & IModuleListener> void reciveInfoFromListener(T module, NBTTagCompound data)
	{
		if(data.hasKey("cn"))
		{
			custom_name = data.getString("cn");
			update = true;
		}
		
		if(data.hasKey("cm"))
		{
			consomation = data.getLong("cm");
			update = true;
		}
		
		/*if(data.hasKey("ae"))
		{
			autoextract = data.getBoolean("ae");
			update = true;
		}*/
		
		if(data.hasKey("z0"))
		{
			consomation = 0;
			update = true;
		}
	}

	@Override
	public void updateListener()
	{
		ListenerUtils.UpdateListenerAccess(listeners);
	}
	
	@Override
	public void observedNeighborChange(Block changedBlock, BlockPos changedBlockPos)
	{
		EnumFacing blockface = Tools.getFacingForm2Blocks(pos, changedBlockPos);
		Block newblock = world.getBlockState(changedBlockPos).getBlock();
		if(face != null && blockface == face.getOpposite())
		{
			TileEntity teblock = world.getTileEntity(changedBlockPos);
			if(teblock != null && teblock.hasCapability(CapabilityEnergy.ENERGY, face))
			{
				blockenergy = teblock.getCapability(CapabilityEnergy.ENERGY, face);
			}
			else
			{
				blockenergy = null;
			}
		}
		
		markDirty();
		//UpdateCkunk();
	}

}
