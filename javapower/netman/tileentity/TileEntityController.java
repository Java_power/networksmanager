package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.List;

import javapower.netman.nww.IMEventUpdate;
import javapower.netman.nww.IMachineController;
import javapower.netman.nww.IMachineNetwork;
import javapower.netman.nww.IModuleListenerAcceptor;
import javapower.netman.nww.MachineUtil;
import javapower.netman.util.BlockPosDim;
import javapower.netman.util.Tools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityController extends TileEntity implements IMachineController
{
	List<IMachineNetwork> machines = new ArrayList<IMachineNetwork>();
	@Override
	public void onLoad()
	{
		UpdateController();
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		if(Tools.isServer())
		{
			//NBT
		}
		super.readFromNBT(compound);
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		if(world != null && !world.isRemote)
		{
			//NBT
		}
		return super.writeToNBT(compound);
	}

	@Override
	public void UpdateController()
	{
		if(world != null && !world.isRemote)
		{
			List<IMachineNetwork> machines_after = machines;
			machines = MachineUtil.GetAllMachines(world, pos);
			if(machines != null)
			{
				for(IMachineNetwork machine: machines_after)
				{
					if(!machines.contains(machine))
					{
						if(machine instanceof IMEventUpdate)
						{
							((IMEventUpdate)machine).onEventListMachinesUpdate(null);
						}
						
						if(machine instanceof IModuleListenerAcceptor)
						{
							((IModuleListenerAcceptor)machine).updateListener();
						}
					}
				}
				
				for(IMachineNetwork machine: machines)
				{
					if(machine instanceof IMEventUpdate)
					{
						((IMEventUpdate)machine).onEventListMachinesUpdate(machines);
					}
					
					if(machine instanceof IModuleListenerAcceptor)
					{
						((IModuleListenerAcceptor)machine).updateListener();
					}
				}
			}
		}
	}

	@Override
	public BlockPosDim thispos()
	{
		return new BlockPosDim(pos, world.provider.getDimension());
	}
}
