package javapower.netman.tileentity;

import java.util.ArrayList;
import java.util.List;

import javapower.netman.util.ITileUpdate;
import javapower.netman.util.NetworkUtils;
import javapower.netman.util.Tools;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;

public abstract class TileEntitySynchronized extends TileEntityBase implements ITileUpdate, ITickable
{
	public List<EntityPlayerMP> players = new ArrayList<EntityPlayerMP>();

	@Override
	public void addOrRemovePlayer(EntityPlayerMP player, boolean isAdded)
	{
		if(isAdded)
		{
			if(players.isEmpty())
			{
				players.add(player);
			}
			else
			{
				if(!Tools.PlayerIsOnList(players, player))
				{
					players.add(player);
				}
			}
		}
		else
		{
			Tools.RemovePlayerOnList(players, player);
		}
	}

	@Override
	public void update()
	{
		if(!players.isEmpty())
		{
			NBTTagCompound update = updateData();
			if(update != null)
			{
				for(EntityPlayerMP pl : players)
				{
					if(pl != null)
					NetworkUtils.sendToPlayerTheData(this, update, pl);
				}
			}
		}
	}

}
