package javapower.netman.block;

import javapower.netman.core.NetworksManager;
import javapower.netman.nww.IBlockNetwork;
import javapower.netman.util.IBlockRegister;
import javapower.netman.util.IRenderItemRegister;
import javapower.netman.util.ItemRenderCast;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BBase extends Block implements IBlockRegister, IRenderItemRegister
{
	String _name;
	Item thisItem;
	
	public BBase(Material materialIn, String name)
	{
		super(materialIn);
		setRegistryName(name);
		setUnlocalizedName(name);
		_name = name;
		setCreativeTab(NetworksManager.creativeTab);
		setHardness(2.5F);
		setResistance(3F);
	}
	
	@Override
	public Item getItem()
	{
		if(thisItem == null)
		{
			thisItem = new ItemBlock(this);
			thisItem.setRegistryName(_name);
		}
		return thisItem;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ItemRenderCast[] getItemsRender()
	{
		return new ItemRenderCast[]
				{
						new ItemRenderCast(0, "inventory")
				};
	}

}
