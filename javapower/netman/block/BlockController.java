package javapower.netman.block;

import javapower.netman.nww.MachineUtil;
import javapower.netman.tileentity.TileEntityController;
import javapower.netman.util.TileNamed;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockController extends BBaseNetworkContainer
{

	public BlockController()
	{
		super(Material.IRON, "controller");
	}
	
	// ------- Register --------

	@Override
	public Block getBlock()
	{
		return this;
	}
	
	// ------- Block definition --------
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	// ------ Network ------

	@Override
	public boolean IsMachine()
	{
		return true;
	}
	
	// ------- TileEntity ------
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntityController();
	}
	
	@Override
	public TileNamed[] getTilesEntity()
	{
		return new TileNamed[]{new TileNamed(TileEntityController.class, "controller")};
	}
	
	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		if(MachineUtil.GetControllers(worldIn, pos).isEmpty())
			return super.canPlaceBlockAt(worldIn, pos);
		
		return false;
	}

}
