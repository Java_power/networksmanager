package javapower.netman.block;

import javapower.netman.container.ContainerVoid;
import javapower.netman.core.NetworksManager;
import javapower.netman.gui.GuiRFCounter;
import javapower.netman.tileentity.TileEntityRFCounter;
import javapower.netman.util.IGuiRegister;
import javapower.netman.util.TileNamed;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockRFCounter extends BBaseNetworkContainer implements IGuiRegister
{
	public static final PropertyDirection PROPERTY_DIR = PropertyDirection.create("dir");
	
	public BlockRFCounter()
	{
		super(Material.IRON, "rfcounter");
	}
	
	// ---------- Network ----------

	@Override
	public boolean IsMachine()
	{
		return true;
	}
	
	// ---------- Register ----------

	@Override
	public Block getBlock()
	{
		return this;
	}

	@Override
	public TileNamed[] getTilesEntity()
	{
		return new TileNamed[]{new TileNamed(TileEntityRFCounter.class, "rfcounter")};
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntityRFCounter();
	}
	
	// ---------- Block State ----------
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] {PROPERTY_DIR});
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(PROPERTY_DIR, EnumFacing.getFront(meta));
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(PROPERTY_DIR).getIndex();
	}
	
	// ---------- Render ----------
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	// ---------- Event ----------
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		if(!worldIn.isRemote)
		{
			worldIn.setBlockState(pos, state.withProperty(PROPERTY_DIR, EnumFacing.getDirectionFromEntityLiving(pos, placer)));
		}
		super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(!playerIn.isSneaking())
			playerIn.openGui(NetworksManager.INSTANCE, 0, worldIn, pos.getX(), pos.getY(), pos.getZ());
		
		return true;
	}
	
	// ---------- Player Inteface ----------

	@Override
	@SideOnly(Side.CLIENT)
	public GuiScreen getGui(TileEntity tile)
	{
		return new GuiRFCounter(new ContainerVoid(tile), (TileEntityRFCounter) tile);
	}

	@Override
	public Container getContainer(TileEntity tile)
	{
		return new ContainerVoid(tile);
	}

}
