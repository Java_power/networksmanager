package javapower.netman.block;

import javapower.netman.container.ContainerVoid;
import javapower.netman.core.NetworksManager;
import javapower.netman.gui.GuiRFEnergyInfo;
import javapower.netman.tileentity.TileEntityRFEnergyInfo;
import javapower.netman.util.IGuiRegister;
import javapower.netman.util.TileNamed;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockRFEnergyInfo extends BBaseNetworkContainer implements IGuiRegister
{
	public static final PropertyDirection PROPERTY_DIR = PropertyDirection.create("dir");
	
	public BlockRFEnergyInfo()
	{
		super(Material.IRON, "rfenergyinfo");
	}
	
	//---------- Network ----------

	@Override
	public boolean IsMachine()
	{
		return true;
	}
	
	//---------- Register ----------

	@Override
	public Block getBlock()
	{
		return this;
	}

	@Override
	public TileNamed[] getTilesEntity()
	{
		return new TileNamed[]{new TileNamed(TileEntityRFEnergyInfo.class, "rfenergyinfo")};
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntityRFEnergyInfo();
	}
	
	// ---------- Block State ----------
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] {PROPERTY_DIR});
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(PROPERTY_DIR, EnumFacing.getFront(meta));
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(PROPERTY_DIR).getIndex();
	}
	
	// ---------- Render ----------
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	// ---------- Event ----------
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		if(!worldIn.isRemote)
		{
			EnumFacing face = EnumFacing.getDirectionFromEntityLiving(pos, placer);
			worldIn.setBlockState(pos, state.withProperty(PROPERTY_DIR, face));
			
			TileEntity te = worldIn.getTileEntity(pos);
			if(te instanceof TileEntityRFEnergyInfo)
			{
				BlockPos poslook = pos.add(face.getOpposite().getDirectionVec());
				IBlockState statelook = worldIn.getBlockState(poslook);
				((TileEntityRFEnergyInfo)te).face = face;
				((TileEntityRFEnergyInfo)te).observedNeighborChange(statelook.getBlock(), poslook);
			}
		}
		super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(!playerIn.isSneaking())
			playerIn.openGui(NetworksManager.INSTANCE, 0, worldIn, pos.getX(), pos.getY(), pos.getZ());
		
		return true;
	}
	
	/*@Override
	public void observedNeighborChange(IBlockState observerState, World world, BlockPos observerPos, Block changedBlock, BlockPos changedBlockPos)
	{
		if(!world.isRemote)
		{
			TileEntity te = world.getTileEntity(observerPos);
			if(te instanceof TileEntityRFEnergyInfo)
			{
				((TileEntityRFEnergyInfo)te).observedNeighborChange(changedBlock, changedBlockPos);
			}
		}
		super.observedNeighborChange(observerState, world, observerPos, changedBlock, changedBlockPos);
	}*/
	
	//---------- Player Interface ----------

	@Override
	@SideOnly(Side.CLIENT)
	public GuiScreen getGui(TileEntity tile)
	{
		return new GuiRFEnergyInfo(new ContainerVoid(tile), (TileEntityRFEnergyInfo) tile);
	}

	@Override
	public Container getContainer(TileEntity tile)
	{
		return new ContainerVoid(tile);
	}

}
