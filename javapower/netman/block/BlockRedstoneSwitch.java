package javapower.netman.block;

import javapower.netman.container.ContainerVoid;
import javapower.netman.core.NetworksManager;
import javapower.netman.gui.GuiRedstoneSwitch;
import javapower.netman.tileentity.TileEntityRedstoneSwitch;
import javapower.netman.util.IGuiRegister;
import javapower.netman.util.RedstoneUtils;
import javapower.netman.util.TileNamed;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockRedstoneSwitch extends BBaseNetworkContainer implements IGuiRegister
{

	public BlockRedstoneSwitch()
	{
		super(Material.IRON, "redstoneswitch");
	}

	@Override
	public boolean IsMachine()
	{
		return true;
	}

	@Override
	public Block getBlock()
	{
		return this;
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}

	@Override
	public TileNamed[] getTilesEntity()
	{
		return new TileNamed[]{ new TileNamed(TileEntityRedstoneSwitch.class, "redstoneswitch") };
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntityRedstoneSwitch();
	}
	
	@Override
	public boolean canConnectRedstone(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side)
	{
		return true;
	}
	
	@Override
	public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		return blockState.getWeakPower(blockAccess, pos, side);
	}
	
	@Override
	public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		return RedstoneUtils.block_getWeakPower(blockState, blockAccess, pos, side);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(!playerIn.isSneaking())
			playerIn.openGui(NetworksManager.INSTANCE, 0, worldIn, pos.getX(), pos.getY(), pos.getZ());
		
		return true;
	}
	
	@Override
	public boolean shouldCheckWeakPower(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side)
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public GuiScreen getGui(TileEntity tile)
	{
		return new GuiRedstoneSwitch(new ContainerVoid(tile), (TileEntityRedstoneSwitch) tile);
	}

	@Override
	public Container getContainer(TileEntity tile)
	{
		return new ContainerVoid(tile);
	}

}
