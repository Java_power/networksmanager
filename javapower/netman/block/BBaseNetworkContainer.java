package javapower.netman.block;

import javapower.netman.core.NetworksManager;
import javapower.netman.nww.IBlockNetwork;
import javapower.netman.nww.MachineUtil;
import javapower.netman.util.IBlockRegister;
import javapower.netman.util.INeighborChange;
import javapower.netman.util.IRenderItemRegister;
import javapower.netman.util.ITileRegister;
import javapower.netman.util.ItemRenderCast;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BBaseNetworkContainer extends BlockContainer implements IBlockNetwork, IBlockRegister, IRenderItemRegister, ITileRegister
{
	String _name;
	Item thisItem;
	
	public BBaseNetworkContainer(Material materialIn, String name)
	{
		super(materialIn);
		setRegistryName(name);
		setUnlocalizedName(name);
		_name = name;
		setCreativeTab(NetworksManager.creativeTab);
		setHardness(2.5F);
		setResistance(3F);
	}
	
	@Override
	public Item getItem()
	{
		if(thisItem == null)
		{
			thisItem = new ItemBlock(this);
			thisItem.setRegistryName(_name);
		}
		return thisItem;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ItemRenderCast[] getItemsRender()
	{
		return new ItemRenderCast[]
				{
						new ItemRenderCast(0, "inventory")
				};
	}
	
	// ------ Event -------
	
		@Override
		public void observedNeighborChange(IBlockState observerState, World world, BlockPos observerPos, Block changedBlock, BlockPos changedBlockPos)
		{
			if(!world.isRemote)
			{
				TileEntity te = world.getTileEntity(observerPos);
				if(te instanceof INeighborChange)
				{
					((INeighborChange)te).observedNeighborChange(changedBlock, changedBlockPos);
				}
			}
			super.observedNeighborChange(observerState, world, observerPos, changedBlock, changedBlockPos);
		}
	
		@Override
		public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
		{
			super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
			MachineUtil.UpdateController(worldIn, pos);
		}
		
		@Override
		public void onBlockDestroyedByPlayer(World worldIn, BlockPos pos, IBlockState state)
		{
			super.onBlockDestroyedByPlayer(worldIn, pos, state);
			MachineUtil.UpdateController(worldIn, pos);
		}
		
		@Override
		public void onBlockDestroyedByExplosion(World worldIn, BlockPos pos, Explosion explosionIn)
		{
			super.onBlockDestroyedByExplosion(worldIn, pos, explosionIn);
			MachineUtil.UpdateController(worldIn, pos);
		}

}
