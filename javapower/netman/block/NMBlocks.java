package javapower.netman.block;

import net.minecraft.block.Block;

public class NMBlocks
{
	public static final Block block_cable = new BlockCable();
	public static final Block block_controller = new BlockController();
	public static final Block block_terminal = new BlockTerminal();
	
	public static final Block block_RFSwitch = new BlockRFSwitch();
	public static final Block block_RFCounter = new BlockRFCounter();
	public static final Block block_RFFluxMeter = new BlockRFFluxMeter();
	public static final Block block_RFEnergyInfo = new BlockRFEnergyInfo();
	
	public static final Block block_FluidValve = new BlockFluidValve();
	public static final Block block_FluidCounter = new BlockFluidCounter();
	public static final Block block_FluidFlowMeter = new BlockFluidFlowMeter();
	public static final Block block_FluidStorageInfo = new BlockFluidStorageInfo();
	
	public static final Block block_RedstoneSwitch = new BlockRedstoneSwitch();
	
	public static final Block block_machine = new BlockMachine();
}
