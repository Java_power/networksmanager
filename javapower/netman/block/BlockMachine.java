package javapower.netman.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockMachine extends BBase
{

	public BlockMachine()
	{
		super(Material.IRON, "blockmachine");
	}

	@Override
	public Block getBlock()
	{
		return this;
	}

}
