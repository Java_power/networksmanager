package javapower.netman.block;

import java.util.List;

import javapower.netman.nww.IBlockNetwork;
import javapower.netman.nww.MachineUtil;
import javapower.netman.util.Tools;
import net.minecraft.block.Block;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCable extends BBase implements IBlockNetwork
{
	public static final PropertyBool NORTH = PropertyBool.create("north");
    public static final PropertyBool EAST = PropertyBool.create("east");
    public static final PropertyBool SOUTH = PropertyBool.create("south");
    public static final PropertyBool WEST = PropertyBool.create("west");
    public static final PropertyBool UP = PropertyBool.create("up");
    public static final PropertyBool DOWN = PropertyBool.create("down");
    
    public static final AxisAlignedBB CORE_AABB = Tools.getBounds(6, 6, 6, 10, 10, 10);
    private static final AxisAlignedBB NORTH_AABB = Tools.getBounds(6, 6, 0, 10, 10, 6);
    private static final AxisAlignedBB EAST_AABB = Tools.getBounds(10, 6, 6, 16, 10, 10);
    private static final AxisAlignedBB SOUTH_AABB = Tools.getBounds(6, 6, 10, 10, 10, 16);
    private static final AxisAlignedBB WEST_AABB = Tools.getBounds(0, 6, 6, 6, 10, 10);
    private static final AxisAlignedBB UP_AABB = Tools.getBounds(6, 10, 6, 10, 16, 10);
    private static final AxisAlignedBB DOWN_AABB = Tools.getBounds(6, 0, 6, 10, 6, 10);
	
	public BlockCable()
	{
		super(Material.IRON, "cable");
	}
	
	// ------- Register --------
	
	@Override
	public Block getBlock()
	{
		return this;
	}
	
	// ------- Block definition --------
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public EnumPushReaction getMobilityFlag(IBlockState state)
	{
		return EnumPushReaction.BLOCK;
	}
	
	@Override
	public boolean causesSuffocation(IBlockState state)
	{
		return false;
	}
	
	@Override
    public BlockRenderLayer getBlockLayer()
	{
		return super.getBlockLayer();
    }
	
	// -------- BlockState --------
	@Override
	protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {NORTH, EAST, WEST, SOUTH, UP, DOWN});
    }
	
	public int getMetaFromState(IBlockState state)
    {
        return 0;
    }
	
	@Override
	public boolean canBeConnectedTo(IBlockAccess world, BlockPos pos, EnumFacing facing)
	{
		Block connector = world.getBlockState(pos.offset(facing)).getBlock();
		if(connector instanceof IBlockNetwork)
			return true;
		return false;
	}
	
	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
	{
		return state.withProperty(NORTH, canBeConnectedTo(worldIn, pos, EnumFacing.NORTH))
				.withProperty(EAST, canBeConnectedTo(worldIn, pos, EnumFacing.EAST))
				.withProperty(SOUTH, canBeConnectedTo(worldIn, pos, EnumFacing.SOUTH))
				.withProperty(WEST, canBeConnectedTo(worldIn, pos, EnumFacing.WEST))
				.withProperty(UP, canBeConnectedTo(worldIn, pos, EnumFacing.UP))
				.withProperty(DOWN, canBeConnectedTo(worldIn, pos, EnumFacing.DOWN));
				
	}
	
	// -------- Collison --------
	
	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean p_185477_7_)
	{
		if (!p_185477_7_)
        {
            state = state.getActualState(worldIn, pos);
        }
		addCollisionBoxToList(pos, entityBox, collidingBoxes, CORE_AABB);
		if(state.getValue(WEST))
			addCollisionBoxToList(pos, entityBox, collidingBoxes, WEST_AABB);
		if(state.getValue(DOWN))
			addCollisionBoxToList(pos, entityBox, collidingBoxes, DOWN_AABB);
		if(state.getValue(NORTH))
			addCollisionBoxToList(pos, entityBox, collidingBoxes, NORTH_AABB);
		if(state.getValue(EAST))
			addCollisionBoxToList(pos, entityBox, collidingBoxes, EAST_AABB);
		if(state.getValue(UP))
			addCollisionBoxToList(pos, entityBox, collidingBoxes, UP_AABB);
		if(state.getValue(SOUTH))
			addCollisionBoxToList(pos, entityBox, collidingBoxes, SOUTH_AABB);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		state = getActualState(state, source, pos);
		return new AxisAlignedBB(state.getValue(WEST) ? 0 : 0.375D, state.getValue(DOWN) ? 0 : 0.375D, state.getValue(NORTH) ? 0 : 0.375D, state.getValue(EAST) ? 1 : 0.625D, state.getValue(UP) ? 1 : 0.625D, state.getValue(SOUTH) ? 1 : 0.625D);
	}
	
	
	/*@Override
    public RayTraceResult collisionRayTrace(IBlockState state, World world, BlockPos pos, Vec3d start, Vec3d end)
	{
        Tools.AdvancedRayTraceResult result = Tools.collisionRayTrace(pos, start, end, getCollisionBoxes(this.getActualState(state, world, pos)));
        return result != null ? result.hit : null;
	}*/
	
	/*public List<AxisAlignedBB> getUnionizedCollisionBoxes(IBlockState state)
	{
		List<AxisAlignedBB> boxes = new ArrayList<>();
        boxes.add(CORE_AABB);
        
        if (state.getValue(NORTH))
        	boxes.add(NORTH_AABB);
        
        if (state.getValue(EAST))
        	boxes.add(EAST_AABB);

        if (state.getValue(SOUTH))
        	boxes.add(SOUTH_AABB);

        if (state.getValue(WEST))
            boxes.add(WEST_AABB);

        if (state.getValue(UP))
            boxes.add(UP_AABB);

        if (state.getValue(DOWN))
            boxes.add(DOWN_AABB);

        return boxes;
    }
	
	public List<AxisAlignedBB> getNonUnionizedCollisionBoxes(IBlockState state)
	{
        return Collections.emptyList();
    }
	
	public List<AxisAlignedBB> getCollisionBoxes(IBlockState state)
	{
        List<AxisAlignedBB> boxes = new ArrayList<>();

        boxes.addAll(getUnionizedCollisionBoxes(state));
        boxes.addAll(getNonUnionizedCollisionBoxes(state));

        return boxes;
    }*/
	
	// ------ Network -------

	@Override
	public boolean IsMachine()
	{
		return false;
	}
	
	// ------ Event -------
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
		MachineUtil.UpdateController(worldIn, pos);
	}
	
	@Override
	public void onBlockDestroyedByPlayer(World worldIn, BlockPos pos, IBlockState state)
	{
		super.onBlockDestroyedByPlayer(worldIn, pos, state);
		MachineUtil.UpdateController(worldIn, pos);
	}
	
	@Override
	public void onBlockDestroyedByExplosion(World worldIn, BlockPos pos, Explosion explosionIn)
	{
		super.onBlockDestroyedByExplosion(worldIn, pos, explosionIn);
		MachineUtil.UpdateController(worldIn, pos);
	}
	

}
