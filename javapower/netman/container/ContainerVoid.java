package javapower.netman.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;

public class ContainerVoid extends Container
{
	TileEntity tileEntity;
	public ContainerVoid(TileEntity te)
	{
		tileEntity = te;
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return true;
	}
	
	@Override
	public void onContainerClosed(EntityPlayer playerIn)
	{
		tileEntity.markDirty();
	}

}
