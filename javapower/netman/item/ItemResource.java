package javapower.netman.item;

import javapower.netman.core.NetworksManager;
import javapower.netman.util.IItemRegister;
import javapower.netman.util.IRenderItemRegister;
import javapower.netman.util.ItemRenderCast;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ItemResource extends Item implements IItemRegister, IRenderItemRegister
{
	public ItemResource()
	{
		setCreativeTab(NetworksManager.creativeTab);
		setRegistryName("resource");
		setUnlocalizedName("resource");
		setHasSubtypes(true);
	    setMaxDamage(0);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		if(stack != null)
		{
			switch (stack.getItemDamage())
			{
				case 0: return "item.netcard";
				case 1: return "item.cpucard";
				case 2: return "item.memorycard";
				case 3: return "item.swcard";
				case 4: return "item.fluxcard";
				case 5: return "item.electricvalve";
				case 6: return "item.flowmeter";
				default: return "item.resource";
			}
		}
		
		return super.getUnlocalizedName();
	}

	@Override
	public Item getItem()
	{
		return this;
	}
	
	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items)
	{
		if (this.isInCreativeTab(tab))
        {
			for(int i = 0; i <= 6; ++i)
				items.add(new ItemStack(this, 1, i));
		}
	}
	
	

	@Override
	public ItemRenderCast[] getItemsRender()
	{
		return new ItemRenderCast[]
				{
						new ItemRenderCast(0, "netcard"),
						new ItemRenderCast(1, "cpucard"),
						new ItemRenderCast(2, "memorycard"),
						new ItemRenderCast(3, "swcard"),
						new ItemRenderCast(4, "fluxcard"),
						new ItemRenderCast(5, "electricvalve"),
						new ItemRenderCast(6, "flowmeter")
				};
	}

}
