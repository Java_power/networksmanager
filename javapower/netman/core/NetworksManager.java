/***************************************************
*						 ^
* 						/ \
* 					   /   \
*                     /  |  \
*                    /   .   \
*                   ___________
*                   
*   			[ W A R N I N G ! ]
*
****************************************************
*
*	All class belonging to the mod Network Manager is developed by Cyril GENIN (Java_Power).
*	All rights reserved to Cyril GENIN.
*	it is strictly forbidden to copy or recopy!
*	These rules apply to all class, scripts, textures, configs and all file types of this project.
*
*		author: Cyril GENIN (Java_Power)
*		website: http://javapower.fr/
*		email: cyril@famille-genin.fr
*		creation date: 26/01/2016 (dd/mm/yyyy)
*		creat at: Montigny Le Bretonneux France
*		recreation date: 04/08/2017 (dd/mm/yyyy)
*		last modification: 30/04/2018 (dd/mm/yyyy)
*		comment: RAS
*		
***************************************************/
package javapower.netman.core;

import javapower.netman.block.NMBlocks;
import javapower.netman.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = NetworksManager.MODID, version = NetworksManager.VERSION, guiFactory = "javapower.netman.core.GuiFactoryNM")
public class NetworksManager
{
    public static final String MODID = "networksmanager";
    public static final String VERSION = "12.0.4";
    
    @Instance
	public static NetworksManager INSTANCE;
    
    @SidedProxy(clientSide = "javapower.netman.proxy.ClientProxy", serverSide = "javapower.netman.proxy.CommonProxy")
    public static CommonProxy proxy;
	public static CreativeTabs creativeTab = new CreativeTabs(MODID)
	{
		
		@Override
		public ItemStack getTabIconItem()
		{
			return new ItemStack(NMBlocks.block_terminal);
		}
	};
	
    @EventHandler
    public void preInit(FMLPreInitializationEvent e)
    {
    	Config.init(e);
        proxy.preInit(e);
    }

    @EventHandler
    public void init(FMLInitializationEvent e)
    {
    	proxy.init(e);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent e)
    {
    	proxy.postInit(e);
    }
    
    // ---------- for development ----------
    public static final boolean PrintStackTrace_TileEntity = false;
}
