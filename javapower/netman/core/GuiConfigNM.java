package javapower.netman.core;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;

public class GuiConfigNM extends GuiConfig
{

	public GuiConfigNM(GuiScreen parentScreen)
	{
		super(parentScreen, new ConfigElement(Config.conf_client.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), NetworksManager.MODID, false, false, "Networks Manager Config");
	}
	
	@Override
    public void initGui()
    {
        super.initGui();
    }
	
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void actionPerformed(GuiButton button)
    {
        super.actionPerformed(button);
        if(button.id == 2000)
    	{
    		Config.conf_client.save();
    		Config.onChangment();
    	}
    }

}
