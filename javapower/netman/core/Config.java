package javapower.netman.core;

import javapower.netman.util.Tools;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class Config
{
	public static Configuration conf_client;
	
	public static void init(FMLPreInitializationEvent event)
	{
		if(Tools.isClient())
		{
			conf_client = new Configuration(event.getSuggestedConfigurationFile());
			//conf.getInt("terminal gui scale", "general", 1, 1, 2, "");
			conf_client.load();
			
			//conf_client.get("general", "terminal gui scale", "x1", "", new String[] {"x1","x2"});
			//conf_client.get("general", "terminal background opacity", 255, "", 1, 255);
			
			onChangment();
			conf_client.save();
		}
	}
	
	public static int guiTerminal_scrale = 1;
	public static int guiTerminal_backgroudOpacity = 255;
	public static boolean guiTerminal_KeyEscEnable = false;
	
	public static void onChangment()
	{
		guiTerminal_scrale = conf_client.get("general", "terminal gui scale", "x1", "", new String[] {"x1","x2"}).getString().equals("x2")? 2 : 1;
		guiTerminal_backgroudOpacity = conf_client.get("general", "terminal background opacity", 255, "", 1, 255).getInt();
		guiTerminal_KeyEscEnable = conf_client.get("general", "terminal key ESC enable", false).getBoolean();
		
	}
	
}
